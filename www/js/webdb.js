/********************************
 * Web SQL Database(SQLite)
 * ※WebKitにて推奨。IE・Firefoxは非推奨
 * 
 * openDatabase(データベース名, バージョン, 表示名, 確保容量)
 * db.transaction(FUNC, ERROR, SUCCESS) //書込・読込
 * db.readTransaction(FUNC, ERROR, SUCCESS) //読込のみ
 * db.executeSql(SQL, VARUE, ERROR, SUCCESS)
 * db.changeVersion(OLD_VERSION, NEW_VERSION, FUNC, ERROR, SUCCESS);
 *
 * FUNC         function 
 * SQL		    SQL
 * ERROR		エラーコールバック
 * SUCCESS	    サクセスコールバック
 * OLD_VERSION  以前のデータベースバージョン
 * NEW_VERSION  新しいデータベースバージョン
 * 
 * エラーコード
 * try/catch(e) e.code
 * 1	NOT_FOUND_ERR
 * 2	SECURITY_ERR
 * 3	ABORT_ERR
 * 4	NOT_READABLE_ERR
 * 5	ENCODING_ERR
 * 6	NO_MODIFICATION_ALLOWED_ERR
 * 7	INVALID_STATE_ERR
 * 8	SYNTAX_ERR
 * 9	INVALID_MODIFICATION_ERR
 * 10	QUOTA_EXCEEDED_ERR
 * 11	TYPE_MISMATCH_ERR
 * 12   PATH_EXISTS_ERR
 * 
 * エラーコード
 * SQL Error
 * 0	UNKNOWN_ERR
 * 1	DATABASE_ERR
 * 2	VERSION_ERR
 * 3	TOO_LARGE_ERR
 * 4	QUOTA_ERR
 * 5	SYNTAX_ERR
 * 6	CONSTRAINT_ERR
 * 7	TIMEOUT_ERR
 * 
 * メモ(ファイル読み込み)
 * $.ajax() 同期処理可能
 * $.get()  非同期処理
 ********************************/

/************ 設定項目 ************/
var apVer = "1.0.0"; //アプリのバージョン
var dbName = "UPTODATE18-19"; //DB名
var webStrName = "UPTODATE18-19WEB"; //ブラウザで管理する際の名前
var dbSize = 1024 * 1024 * 50; //50MB
var createSql = ["db/CREATE_TABLE.sql", "db/INSERT_TBYOMEI.sql", "db/INSERT_TTAGLIST.sql","db/INSERT_TSYSSTATE.sql"];

/************ DB Object ************/
var webdb = {};
webdb.db = null; //初期化
var xmlList = []; // xmlファイル名のみを保持するリスト
var xmlData = []; // JSON形式(DBデータを全て保持するリスト)

/************ DB接続 ******************/
webdb.open = function() {
	try{
		//バージョンを指定(固定)して接続
		webdb.db = openDatabase(dbName, "1.0", webStrName, dbSize);
	}catch(e){
    webdb.db = openDatabase(dbName, "", webStrName, dbSize);
	}

	//バージョンチェック
	webdb.chkController( "open" );
};

/** ******************************
 * DBのチェックを行います
 * chkflg：制御フラグ・データ値
 ********************************/
webdb.chkController = function( chkflg ){
	//テーブル有無確認
  if( chkflg=="open" ){
    webdb.chkTable( webdb.chkController );
    return;
  }

	//最新バージョンとなっているか確認
	if( apVer > chkflg ){
    /** ～今後VerUP時の処理を記載～ **/
		return;
	}

  //メニューデータ取得
  webdb.getXMLList( 0, webdb.initData );
};

/********************************
 * テーブルの生成状態を確認 
 * func：戻り先（引数にver情報)
 ********************************/
webdb.chkTable = function( func ){
	webdb.db.transaction(function(tx){
    tx.executeSql(
			//TBYOMEIの有無を確認
			"select KBN_TXT from TSYSSTATE where KBN_CD=? and SUB_CD=?", 
			[ 1, 0 ], 
      function( tx, results ){
        //バージョンデータを呼び出し元に返す
        func(results.rows.item(0).KBN_TXT);
      },
      function(){
      //テーブルとデータを入れ直す
			  webdb.createTable();
      });
	});
};

/** テーブル・データを新規作成 **/
webdb.createTable = function(){
	webdb.db.transaction(function(tx){
		for( var i=0; i<createSql.length; i++ ){
			$.ajax({
				url: createSql[i],
				type:"GET",
				async: false,   //同期させ順にSQLを実行
				dataType:"text",
				error:err,
				success:function( txt, act ){
					if( "success"!=act || txt==null) err();
					var sqlList = txt.split(";");
					for( var j=0; j<(sqlList.length-1); j++ ){
						tx.executeSql(sqlList[j]);
					}
				}
			});
		}
	});
	webdb.getXMLList( 0, webdb.initData );
};

/********************************
 * 初期化
 ********************************/
webdb.initializeDB = function(){
	if( !confirm("初期化を実行しますか？") ) return;

	$.get("db/DROP_TABLE.sql", function( txt ){
		if( txt==null) err();
		var sqlList = txt.split(";");
		webdb.db.transaction(function(tx){
			for( var i=0; i<(sqlList.length-1); i++ ){
				tx.executeSql(sqlList[i]);
			}
		});
		setTimeout("webdb.createTable()", 100); //並列実行
		ons.notification.alert({
			title: "メッセージ",
			message: "初期化が完了しました。",
      callback: function(){}
		});
	});
};

/********************************
 * cat：[1～17]カテゴリー番号 [それ以外]全件
 * func：戻り先
 ********************************/
webdb.getXMLList = function( cat, func ){
	var sql = "select CAT_ID,BYO_ID,BYO_NM,BYO_XML from TBYOMEI";
	if( 1<=cat && cat<=17 ) sql +=" where CAT_ID='"+cat+"'";
	sql += " order by CAT_ID ASC, BYO_ID ASC"; //章節の昇順
	webdb.db.readTransaction(function(tx){
		tx.executeSql(sql, [], func, err );
	});
};

/********************************
 * 初回起動時にDBからデータを取得する
 * xmlData：JSON形式(DBデータを全て格納)
 * xmlList：xmlファイル名のみのリスト
********************************/
webdb.initData = function( tx, results ){
  if( 0<xmlData.length ) return;
	xmlData = results.rows;
	xmlList = new Array();
	for(var i=0; i<results.rows.length; i++){
		data = results.rows.item(i);
		xmlList.push(data.BYO_XML);
	}
  //利用規約の同意状況を確認
  chkTerms();
};

/********************************
 * お気に入り登録制御
 * addFlg：[1]登録 [1以外]登録解除
********************************/
webdb.updateTag = function( addFlg ){
	if( "1"!=String(addFlg) ) addFlg=0;
	var sql = "update TTAGLIST set TAG_FLG=? where CAT_ID=? and BYO_ID=?";	
	webdb.db.transaction(function(tx){
		tx.executeSql(sql,[addFlg, saveCID, saveBID]);
	});
};

/********************************
 * お気に入りチェック
 * catID：カテゴリーID
 * byoID：病名ID
 * func：戻り先
********************************/
webdb.chkTagFlg = function( catID, byoID, func ){
	var sql = "select TAG_FLG from TTAGLIST where CAT_ID=? and BYO_ID=?";	
	webdb.db.readTransaction(function(tx){
		tx.executeSql(sql, [catID,byoID], func, err);
	});
};

/********************************
 * お気に入り登録されているXMLを取得
********************************/
webdb.chkTagXML = function( func ){
	var sql = "select CAT_ID, BYO_ID from TTAGLIST where TAG_FLG=1";	
	webdb.db.readTransaction(function(tx){
		tx.executeSql(sql, [], function( tx, results ){
			//データがあるかチェック
			if( null!=results && 0<results.rows.length){
				sql = "select CAT_ID,BYO_ID,BYO_NM,BYO_XML from TBYOMEI where "; 
				for( var i=0; i<results.rows.length; i++ ){
				   sql+="(CAT_ID="+results.rows.item(i).CAT_ID+" and BYO_ID="+results.rows.item(i).BYO_ID+") ";
				   if( i+1<results.rows.length ) sql+="or ";
				}
				sql += "order by CAT_ID asc, BYO_ID asc"; //章節の昇順
				
				webdb.db.transaction(function(tx){
					tx.executeSql(sql, [], func, err );
				});
			}else{
				func( null, null );
			}
		}, err);
	});
};

/********************************
 * XMLファイル名からXML情報を取得します。
 * xmlName：xmlファイル名
 * func：戻り先(CAT_ID, BYO_ID, BYO_XML)
********************************/
webdb.xmlDetail = function( xmlName, func ){
	var sql = "select CAT_ID, BYO_ID from TBYOMEI where BYO_XML=?";
	webdb.db.readTransaction(function(tx){
		tx.executeSql(sql, [xmlName], function( tx, results ){
			//データがあるかチェック
			if( null!=results && 0<results.rows.length){
				var item = results.rows.item(0);
				func( item.CAT_ID, item.BYO_ID, xmlName );
			}
		}, err);
	});
};

/********************************
 * 参照履歴を追加します 
********************************/
webdb.upHistory = function( catID, byoID ) {   
	//参照履歴追加(ユニーク：ROWID(自動生成))
	var sql = "insert into THISTORY ( CAT_ID, BYO_ID, READ_DT, READ_TM ) values ( ?, ?, ?, ? )";
	webdb.db.transaction(function(tx){
		tx.executeSql(sql, [ catID, byoID, getDTTM("dt"), getDTTM("tm") ]);
	});
};

/********************************
 * 参照回数を更新します
********************************/
webdb.upCount = function( catID, byoID ) {
	//参照回数更新
	var sql = "select READ_COUNT from TREADCOUNT where CAT_ID="+catID+" and BYO_ID="+byoID;
	webdb.db.readTransaction(function(tx){
		tx.executeSql(sql, [], function( tx, results ){
			//データがあるかチェック
			if( null!=results && 0<results.rows.length){
				var item = results.rows.item(0);
				sql = "update TREADCOUNT set READ_COUNT="+(item.READ_COUNT+1)+" where CAT_ID="+catID+" and BYO_ID="+byoID;
			}else{
				//データ登録
				sql = "insert into TREADCOUNT ( CAT_ID, BYO_ID, READ_COUNT ) values ("+catID+","+byoID+",1)";
			}
			webdb.db.transaction(function(tx){
				tx.executeSql(sql);
			});
		}, err);
	});
};

/********************************
 * 参照履歴直近i日分とxmlデータを取得します。
 * getDays=取得日数
********************************/
webdb.historyData = function( getDays ){
	var sql = "select READ_DT from THISTORY group by READ_DT order by READ_DT desc, READ_TM desc limit "+getDays;
	webdb.db.readTransaction(function(tx){
		 tx.executeSql(sql, [], function( tx, results ){
			sql = "";
			//結果をSQLにセット
			for(var i=0; i<results.rows.length; i++ ){
				var item = results.rows.item(i);
				if((i+1)==results.rows.length)
					sql+="'"+item.READ_DT+"'";
				else
					sql+="'"+item.READ_DT+"',";
			}
			sql = 
				"select THISTORY.CAT_ID, THISTORY.BYO_ID, THISTORY.READ_DT, TBYOMEI.BYO_NM ,TBYOMEI.BYO_XML "+
				"from THISTORY inner join TBYOMEI "+
				"on THISTORY.CAT_ID=TBYOMEI.CAT_ID and THISTORY.BYO_ID=TBYOMEI.BYO_ID "+
				"where THISTORY.READ_DT IN("+sql+") "+
				"group by THISTORY.CAT_ID, THISTORY.BYO_ID "+
				"order by READ_DT desc";

			webdb.db.readTransaction(function(tx_){
				tx_.executeSql(sql, [], createHistoryDateMenu, err);
			});
		}, err);
	});
}

/********************************
 * 参照回数上位i件とxmlデータを取得します。
 * geti=取得項目数
********************************/
webdb.countData = function( geti ){
	var sql = 
		"select TREADCOUNT.CAT_ID, TREADCOUNT.BYO_ID, TREADCOUNT.READ_COUNT, TBYOMEI.BYO_NM ,TBYOMEI.BYO_XML "+
		"from TREADCOUNT inner join TBYOMEI "+
		"on TREADCOUNT.CAT_ID=TBYOMEI.CAT_ID and TREADCOUNT.BYO_ID=TBYOMEI.BYO_ID "+
		"order by READ_COUNT desc limit "+geti;
	webdb.db.readTransaction(function(tx){
		tx.executeSql(sql, [], createHistoryCountMenu, err);
	});
};

/********************************
 * システム設定データを取得
 * kbnCD：区分コード
 * subCD：サブコード
 * func：戻り先( KBN_TXT, UP_DT, UP_TM )
 *
 * kbnCD[1]subCD[0]：アプリバージョン(1.0.0など)
 * kbnCD[1]subCD[1]：利用規約同意[0]未同意/[0以外]同意済
********************************/
webdb.getSysData = function( kbnCD, subCD, func ){
    var sql = "select KBN_TXT, UP_DT, UP_TM from TSYSSTATE where KBN_CD=? and SUB_CD=?";
    webdb.db.readTransaction(function(tx){
		tx.executeSql(sql, [kbnCD, subCD], function( tx, results ){
			//データがあるかチェック
			if( null!=results && 0<results.rows.length){
				var item = results.rows.item(0);
				func( item.KBN_TXT, item.UP_DT, item.UP_TM );
			}else{
    	  func( null, "", "" );
			}
		}, err);
	});
};

/********************************
 * システム設定のデータ更新(基本UPDATE)
 * kbnCD：区分コード
 * subCD：サブコード
 * kbnTxt：登録データ
 * 
 * kbnCD[1]subCD[0]：アプリバージョン(1.0.0など)
 * kbnCD[1]subCD[1]：利用規約同意[0]未同意/[0以外]同意済
********************************/
webdb.setSysData = function( kbnCD, subCD, kbnTxt ){
    var sql =
        "update TSYSSTATE "+
        "set KBN_TXT=?, UP_DT=?, UP_TM=? "+
        "where KBN_CD=? and SUB_CD=?";
    webdb.db.transaction(function(tx){
	    tx.executeSql(sql, [kbnTxt, getDTTM("dt"), getDTTM("tm"), kbnCD, subCD]);
	});
};

