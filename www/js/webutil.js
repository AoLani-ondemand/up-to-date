/**
 * webutil.js
 * WebKitの機能を扱うためのjs
**/

/** Util Object **/
var webUtil = {};

/** Strage Key **/
var SEARCH_WORD = "UPTODATE18-19WEB_SEARCH_WORD";

/** 検索ワードを保存 **/
webUtil.setSearchWord = function( word ){
	window.localStorage.setItem(SEARCH_WORD , word);
};

/** 
 * 前回の検索ワードを保存
 * delFlg[1]ワード取得後に削除
**/
webUtil.getSearchWord = function( delFlg ){
	var str = window.localStorage.getItem(SEARCH_WORD);
	//nullは返さない
	if( null==str ) str = "";
	//データを削除
	if(1==delFlg) window.localStorage.removeItem(SEARCH_WORD);
	return str;
}

/**
 * 本文にハイライトを付ける
 * word: ハイライトするワード
 * color: カラーコード
 **/
webUtil.wordHighlight = function( word, color ){
	//初期化
	$("#xmlTemplate").removeHighlight();

	//ワード絞り込み(ヘッダは除く)
	$("#xmlTemplate").highlight(word);
	$("#section-header").removeHighlight();

	//背景色を設定	
	$(".highlight").css({ 
		"background-color":color, 
		"display":"initial", 
		"margin":"0", 
		"padding":"0"
	});
}

/**
 * 本文のハイライト位置まで移動する
 **/
webUtil.showHighlight = function(){
	try{
		//最初のワードまで移動
		$(".page__content .ons-page-inner").scrollTop( $(".highlight").eq(0).offset().top-100 );
	}catch(e){}
}