/********************************
 * index.html
 * 各html読み込み時のscript処理を記載
********************************/

//Onsen UI読み込み
ons.bootstrap();
ons.ready(function() {
	//ポップアップメニューの表示設定
	ons.createPopover("popover.html").then(function(popover) {
		menu = popover;
	});

	//画像拡大処理
	$(document).on('transform', 'img', function(event) {
		svgResize( event );
	});
});

//初期化(画面展開時に実行)
$(function(){
	//DB接続
	try{
		webdb.open();
	}catch(e){
		alert(e);
	}

	//トーストメッセージオプション
	toastr.options = {
		"closeButton": false, //閉じる[×]表示
		"debug": false, //デバッグ
		"newestOnTop": true, //最新メッセージを上位に表示
		"progressBar": false, //表示経過バー
		"positionClass": "toast-top-center", //表示位置
		"preventDuplicates": false, //複数表示 true:OFF
		"timeOut": "2000", //表示時間
		"showEasing": "swing", //表示エフェクト
		"hideEasing": "linear", //消滅エフェクト
		"showMethod": "fadeIn", //表示方法
		"hideMethod": "fadeOut" //消滅方法
	}

	//画面回転時のイベント制御
	$(window).on("orientationchange",function(){
		//マスクの画面幅を設定
		$("#mskView, #imgMSK").width($(window).width());
		$("#mskView, #imgMSK").height($(window).height());

		//OSを判定
		if(monaca.isAndroid){
			$("ons-sliding-menu").css("z-index", "-1");
			// slideMenu.openMenu();
			setTimeout(function(){ uiController( true ); }, 400);
		}else{
			// 画面制御
			uiController( true );
		}
	});

	//初期処理
	$(document).on("pageinit", "#init", function(){
		//マスクの画面幅を設定
		$("#mskView, #imgMSK").width($(window).width());
		$("#mskView, #imgMSK").height($(window).height());

		// フワイプでのメニュー展開を制御
		slideMenu.setSwipeable(false);

		//初期XML展開
		openXML( 1, 1, "01-01.xml" );

		// 即実行するとエラー
		setTimeout(function(){ uiController( false ); }, 400);

		// スプラッシュをクローズ
		setTimeout(function(){
			if( monaca.isAndroid || monaca.isIOS ) navigator.splashscreen.hide();
			if( 960>$(window).width() ) slideMenu.openMenu();
		}, 1000);
	});

	//メイン(XMLテンプレート)
	var startUpFlg=true;
	$(document).on("pageinit", "#main", function(){
		//フワイプでのメニュー展開を制御
		slideMenu.setSwipeable(false);

		//XML読み込み(テスト);
		if(""==saveXML) return;

		//お気に入り登録状態確認
		webdb.chkTagFlg( saveCID, saveBID, chkTagInit);

		//初回起動時は実行しない
		if( !startUpFlg ){
			//参照履歴・回数を更新
			webdb.upCount( saveCID, saveBID );
			webdb.upHistory( saveCID, saveBID );
		}

		//XML表示
		modal.show();
		xmlLoad(saveXML);

		//初期展開完了
		startUpFlg = false;
	});

  // 診療ガイドラインに関する基本知識(XMLテンプレート)
  $(document).on("pageinit", "#info_basicknowledge", function(){
		//フワイプでのメニュー展開を制御
		slideMenu.setSwipeable(false);

		//XML読み込み(テスト);
		if(""==saveXML) return;

		//初回起動時は実行しない
		if( !startUpFlg ){
			//参照履歴・回数を更新
			webdb.upCount( saveCID, saveBID );
			webdb.upHistory( saveCID, saveBID );
		}

		//XML表示
		modal.show();
		xmlLoad(saveXML);

		//初期展開完了
		startUpFlg = false;
	});

	//メニュー初期処理処理
	$(document).on("pageinit", "#menu", function(){});

	//メニュー(コンテンツ)初期展開処理
	$(document).on("pageinit", "#menu_contents", function(){
    webdb.getXMLList( 0, createMenu );
	});

	//メニュー(検索)初期展開処理
	$(document).on("pageinit", "#menu_search", function(){
		menuSearchInit();
	});

	//メニュー(タグ)初期展開処理
	$(document).on("pageinit", "#menu_tag", function(){});

	//メニュー(履歴)初期展開処理
	$(document).on("pageinit", "#menu_info", function(){});

	//メニュー(履歴)初期展開処理
	$(document).on("pageinit", "#pop", function(){});

	//その他 利用規約
	$(document).on("pageinit", "#terms_page", function(){
		$("#terms-txt").load("info_terms_txt.html");
	});

	// スプラッシュを５秒後に強制クローズ
	setTimeout(function(){
		if( monaca.isAndroid || monaca.isIOS ) navigator.splashscreen.hide();
	}, 5000);
});