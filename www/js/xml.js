/**
 * XML関連の処理を行います。
 * read：jquery
**/

//データを埋め込み
var mui={
  //タイトル部
	title:function( xml ){
    var ctitle=( $(xml).text().trim() ).replace(/ /g, "．");
    if (ctitle == '診療ガイドラインに関する基本知識') {
      $("#toolbar_title").html( "【総論】 " + ctitle ); //ツールバーに総論を追加
      $("#title").html( "【総論】 " + ctitle ); //タイトル部の総論を追加
      //$("#title-guidline").css('height','0pt');// CSSの不具合を修正
      $("#title-guidline").removeClass('height');
    } else {
      $("#toolbar_title").html( ctitle ); //ツールバー
      $("#title").html( ctitle ); //タイトル部
    }
	},

	//ガイドライン部
  guide:function( xml ){
    $(xml).find("ul").each( function(i, xml_){
	    $("#guidline").append($(xml_).html());
    });
	},

  //What's new
  wnew:function( xml ){
    if($(xml).find(".ガイドライン_リスト_●なし").length == 0) {
      $(xml).find("ul").each( function(i, xml_) {
        if(0<$(xml).find(".whatsnew_小見出し").length) {
          $("#wahtnew-ul").append("<div class='whatsnew_小見出し'>"+$(xml).find(".whatsnew_小見出し").eq(i).text()+"</div>");
        }
        $("#wahtnew-ul").append($(xml_).html());
      });
    } else {
      $("#wahtnew-ul").append($(xml).children("ul").children("li").prop("outerHTML"));
      $(xml).children("ul").children("ul").each( function(i, xml_) {
        $("#wahtnew-ul").append($(xml_).html());
      });
    }
  },

  // アルゴリズム
  // sasaki:全体がタブでくられていれば複数あってもそのまま出せる。
   algorism:function( xml ){
      //10章10節のみ別の処理
      if( $(xml).html().match(/10-10-A01.svg/) ){
        $(xml).find(".図表_注").each( function(i, xml_) {
          $(xml_).replaceWith("<div style='margin-left: 1em;'>"+$(xml_).html()+"</div>");
        });
      }
      $("#algorism").append($(xml).html());
  },

  // 総説
  general:function( xml ){
    // 小見出しの前後空白を除去
    $(xml).find(".見出し_小見出し").each( function(i, xml_) {
      $(xml_).replaceWith("<div class='見出し_小見出し'>"+$(xml_).text().trim()+"</div>");
    });

    // 専門医へのコンサルト
    $(xml).find(".本文_専門医へのコンサルト").each( function(i, xml_) {
      //丸文字確認
      if(chkMaruSuji($(xml_).text())){
        //丸文字のスタイルが当たるようにclass修正
        $(xml_).replaceWith("<p class='"+$(xml_).attr("class")+"_先頭丸数字'>"+$(xml_).html()+"</p>");
      }
    });

// 総説
  // sasaki:全体がタブでくられていれば複数あってもそのまま出せる。
     $(xml).find(".総説_タイトル").remove();
     $("#detail").append($(xml).html());
},

  // 最近の話題
  topic:function( xml ){
    if ((xml).find(".見出し_最近の話題").length) {
       $("#recent_content").remove();
    }
    $(xml).find(".見出し_最近の話題").remove();
    // ◯数字有無によりインデント変更
    $(xml).find(".本文_最近の話題").each( function(i_, xml_){
      if( chkMaruSuji( $(xml_).text() ) ){
        $(xml_).removeClass("本文_最近の話題");
        $(xml_).addClass("本文_最近の話題_先頭丸数字");
      } 
    });
    $("#none-recent").append($(xml).html());
  },

  // 具体的処方
  prescription:function( xml ){
    // 記載がある場合、なしのデータを削除してデータを挿入
    if($(xml).find(".具体的処方_表").length) {
      $("#section-prescription").find("table").remove();
    }
    // ◯数字有無によりインデント変更
    $(xml).find(".具体的処方_治療方針").each( function(i_, xml_){
      if( chkMaruSuji( $(xml_).text() ) ){
        $(xml_).removeClass("具体的処方_治療方針");
        $(xml_).addClass("具体的処方_治療方針_先頭丸数字");
      }
    });
    if( 0==$(xml).find("thead > tr > th[colspan='6']").length){
      $("#section-prescription").append($(xml).html());
      $("#section-prescription-6").hide();
    }else{
      //17章5節 colspan='6' 対応
      $("#section-prescription-6").append($(xml).html());
    }

  },

  // 最後の図表
  lastchart:function( xml ){
    $("#section-lastchart").append($(xml).html());
  },

  // 執筆者
  author:function ( xml ) {
    $("#section-authors").append($(xml).html());
  },
  // 診療ガイドラインに関する基本知識
  // 総論
  soron:function( xml ){ 
   // 大見出しの装飾のためにテキストを置き換える　2018-10-03 add sasaki
    // 総論①
    $("#section-soron").append($(xml).html());
    $("#section-soron").find(".総論①").find(".総論_大見出し").text('　　　診療ガイドラインとEBM');
    // 総論②
    $("#section-soron").find(".総論②").find(".総論_大見出し").before("<div class='number'>Ⅱ</div>");
    $("#section-soron").find(".総論②").find(".総論_大見出し").empty();
    $("#section-soron").find(".総論②").find(".総論_大見出し").append('　　　診療ガイドラインを正しく読むには：' + "<span class='br'>　　　作成法</span>" + 'とその評価');
    // 総論③
    $("#section-soron").find(".総論③").find(".総論_大見出し").text('　　　診療ガイドラインの適切な活用に向けて');

    // 基本知識ページのタイトルCSSを調整　2018-09-11　add sasaki
     var winHeight = $(window).height();
    //～iPhone 6s
    if( 320 >= winHeight ){
      $("#section-header").find("#title-guidline").css('height', '0pt');
    //iPhone 7～ (6・7Plus=414)
    }else if( 380 >= winHeight ){
      $("#section-header").find("#title-guidline").css('height', '0pt');
    }
  },

  // 文献
  literature:function( xml ){
   $("#literature").append($(xml).html());
  }
};

//xml読み込み
function xmlLoad( fileName ){
    $.ajax({
		url: "xml/"+fileName,
		type:'GET',
		dataType:'xml',
		timeout:5000,
		error:err,
		success:function( xml, act ){
			if( "success"!=act || xml==null) err();
			$(xml).find("body").each(parseXML);
		}
	});
}

//XML解析 [20170082] ALL CHG
function parseXML(){
	//▶は文字化けするのでタグで代用
	$(this).html( $(this).html().replace(/▶/g, "<span class='Mark' />") );

	//image変換処理
	$(this).find("img").each( function(i, xml){
		//パスがない場合は削除
		if( ""==$(xml).attr("src") ){
			$(xml).removeAttr("src");
			return;
		}
    var src_ = $(xml).attr("src");
    if( src_.match(/小見出し_矢印/) ){
      // >> を画像から文字列に変換
      $(xml).replaceWith("<Attention>&gt;&gt;</Attention>");
    }else{
      //svgパス指定
      //$(xml).attr("src", "image/"+$(xml).attr("src")); // 2018-09-04 delete sasaki
      $(xml).attr("src", src_); // 2018-09-04 add sasaki
      //onloadで画像サイズを調整
      $(xml).attr("onload", "imgResize(this)");
      //拡大イベント
      $(xml).attr("onclick", "imgView(this)");
      //image-headerを表上部に
      if( $(xml).find(".アルゴリズム_見出し").length ){
        $(xml).before( $(xml).find(".アルゴリズム_見出し") );
        $(xml).find(".アルゴリズム_見出し").remove();
      }
      //uppertitleをimage-headerとimgの間部に
      if( $(xml).find(".図表_注[outputclass='nohanging']").length ){
        $(xml).before( $(xml).find(".図表_注[outputclass='nohanging']") );
        $(xml).find(".図表_注[outputclass='nohanging']").remove();
      }
    }
	});

  //タイトル構成
  mui.title( $(this).children(".ブロック-ヘッドライン").children(".記事タイトル") );

  //ガイドライン
  mui.guide( $(this).children(".ブロック-ヘッドライン").children(".ガイドライン") );

  // What's new
  mui.wnew( $(this).children(".ブロック-whatsnew") );

  // アルゴリズム
  $(this).find(".ブロック-アルゴリズム"). children(".アルゴリズム_タイトル").remove();
  mui.algorism( $(this).find(".ブロック-アルゴリズム") );
 
  // 総説
  mui.general( $(this).find(".ブロック-総説") );

  // 最近の話題
  mui.topic( $(this).children(".ブロック-最近の話題") );

  // 具体的処方
  mui.prescription( $(this).find(".ブロック-具体的処方") );

  // 筆者
  mui.author( $(this).find(".ブロック-著者"));

  // 診療ガイドラインに関する基本知識
  // 総論
  mui.soron( $(this).find(".ブロック-総論") );

  // Ⅲ-12の独自対応　2018-10-11 add sasaki start
  // 最後の図表
  mui.lastchart( $(this).find(".ブロック-最後の図表") );
  // 2018-10-11 add sasaki end

	//表示処理
	$("#xmlTemplate").show();

	setTimeout(function(){
		//表示切り替え状態を取得
		if( undefined != $("#popover_recent").prop("checked") ){
			getPopver();
		}
	}, 300);

	setTimeout(function(){
		modal.hide();
		var searchWord = webUtil.getSearchWord(1);
		if( ""!=searchWord ){
			webUtil.wordHighlight( searchWord, "#F4FA58" );
		}
	}, 500);
}

/**
 * XML 用語検索
 * key_：検索ワード
 * xmlList_：検索対象XML
**/
function xmlSearch( key_, xmlList_ ){
	var resultList = [];

	//for < while 早い(無限ループに注意)
	//ループ判定中にlengthを使用しない 多少早い
	var i = 0;
	var mxlL = xmlList_.length;
	while(i < mxlL) {
		$.ajax({
			url: "xml/"+xmlList_[i],
			type:'GET',
			async: false,
			dataType:'xml',
			timeout:1000,
			error:err,
			success:function( xml, act ){
				if( "success"!=act || xml==null) err();
				//具体的処方項目は検索対象外
				$(xml).find("midashi_prescription").remove();
				//var pFlg = chkXMLKey( $(xml).find("Root"), key_ );
        var pFlg = chkXMLKey( $(xml).find("body"), key_ );
				if( pFlg ){
					resultList+=xmlList_[i]+"|";
				}
			}
		});
		i++;
	}

	if(resultList.length > 0){
		resultList=resultList.substring(0, resultList.length-1);
	}

	//結果を解析
	searchResultView( key_, resultList );

	//マスクを非表示化
	mskControl("");
}

/**
 * 検索に該当したXMLを検索結果に表示します。
 * src:menu_search_result.html
**/
function searchResultView( word, xmllist ){
	mskControl("lockMSK");
	//domの有無チェック
	if( 0==$("#chapter01").length) return;

	//前の状態を初期化
	var liDom = $("#search_result > ons-page > .list-content > .list-content-container > .index-list");
	$(liDom).find("ol").empty(); //前回の結果を削除
	$(liDom).hide(); //表示されているメニューを非表示
	$("#resultMsg").html("");
	$("#inputWord").val(word);

	//表示する際にタグエスケープ
	var wordEsc = word.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

	if(0<xmllist.length){
		//ハイライト処理がリロード時にしか効かないので初期化
		saveXML="";

		//今回の検索結果を反映
		var list = xmllist.split("|");
		$("#resultMsg").html("【"+wordEsc+"】の結果("+list.length+"件)");
		$("#search_result > ons-page > .list-content > .list-content-container").css("height", ($(window).height() - $("#resultMsg").height() - 200) ); //200はメニュー部

		//結果表示
		for( var i=0; i<list.length; i++ ){
			var openid=list[i].substring(0,2);
			$("#chapter"+openid).show();

			//xml読み込み
			$.ajax({
				url: "xml/"+list[i],
				type:'GET',
				async: false,
				dataType:'xml',
				timeout:3000,
				error:err,
				success:function( xml, act ){
          var title = $(xml).find("head > article-title").text(); // 2018-08-30 add sasaki
          // 2018-08-30 delete sasaki start
          //var tstr = ( $(xml).find("head > article-title").text() ).split("　");
					//var title = tstr[1];
          //var title = tstr;
          // 2018-08-30 delete sasaki end
					$("#chapter"+openid).find("ol").append("<li onclick='openXmlFile(\""+list[i]+"\", \""+word+"\"); chgBackColor(this)'><a class='midashi'>"+title+"</a></li>");
				}
			});
		}
	}else{
		$("#resultMsg").html("【"+wordEsc+"】の結果0件");
	}
	//マスクを非表示化
	mskControl("");
}

//タイトル部にデータを埋め込み
function makeTitle( xml ){
  var ctitle= $(xml).find("head >section").text();
  //節番号の先頭0を消す
  if(ctitle.slice(0,1)=="0") ctitle=ctitle.slice(1,2); 
  ctitle+="."+$(xml).find("head >article-title").text();
  //ツールバー
  $("#toolbar_title").html( ctitle );
  //タイトル
  $("#title").html( ctitle );
}

//XML文字列チェック
function chkXMLKey( xml_, key_ ){
	var result = ($(xml_).text().toLowerCase()).search(key_.toLowerCase());
	if( result > 0 ){
		return true;
	}else{
		return false;
	}
}

// 改訂版にて画像サイズが変わったので補正 2018-10-12 sasaki fix
function imgResize( dom ){
	var winWidth = $(window).height();
	//縦画面をベースにサイズ補正 0 or 180=縦
	var angle_ = Math.abs(window.orientation);
	if( 0 == angle_ || 180 == angle_ ) winWidth = $(window).width();
	//class=Width-900の画像最大幅を画面の75%とする。
	winWidth_900=Math.floor(winWidth*0.75);
   //class=Width-500の画像最大幅を画面の60%とする。
  winWidth_500=Math.floor(winWidth*0.6);
  //class=Width-400の画像最大幅を画面の50%とする。
  winWidth_400=Math.floor(winWidth*0.5);
	
	//設置されたmax-widthを取得
	//var imgWidth=$(dom).attr("max-width"); 2018-10-12 sasaki delete
	//imgWidth = imgWidth.replace(/px/g, ""); 2018-10-12 sasaki delete
    var imgWidth = $(dom).width();

	//画面幅より広い画像はリサイズ
  if ($(dom).attr("class") == "width-900" || $(dom).attr("class") == "width-600") {
	  if( winWidth_900 < imgWidth ) imgWidth=winWidth_900;
  }
  if ($(dom).attr("class") == "width-500") {
    if( winWidth_500 < imgWidth ) imgWidth=winWidth_500;
  }
  if ($(dom).attr("class") == "width-400" || $(dom).attr("class") == "width-300") {
    if( winWidth_400 < imgWidth ) imgWidth=winWidth_400;
  }

	//幅を設定
	$(dom).width(imgWidth+"px");

	//不要な属性を削除
	//$(dom).removeAttr("max-width"); 2018-10-12 sasaki delete
  $(dom).removeAttr("width-900");
  $(dom).removeAttr("width-400");
	$(dom).removeAttr("width");
}

/**
 * 先頭丸文字かチェック
 * true:丸文字 または 片カッコ文字 / false:その他
 */
function chkMaruSuji( str ){
  try{
    str_1 = str.substring(0,1);
    if(str_1.match("①|②|③|④|⑤|⑥|⑦|⑧|⑨|⑩|⑪|⑫|⑬|⑭|⑮|⑯|⑰|⑱|⑲|⑳")) return true;
    if(str_1.match("❶|❷|❸|❹|❺|❻|❼|❽|❾|❿|⓫|⓬|⓭|⓮|⓯|⓰|⓱|⓲|⓳|⓴")) return true;
    
    str_2 = str.substring(0,2);
    if(str_2.match("1）|2）|3）|4）|5）|6）|7）|8）|9）|10）")) return true;
  }catch(e){}
  return false;
}