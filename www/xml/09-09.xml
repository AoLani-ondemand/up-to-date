<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>09</chapter>
    <chapter-title>運動器系疾患</chapter-title>
    <section>09</section>
    <article-code>09-09</article-code>
    <article-title>大腿骨頸部/転子部骨折</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>9 大腿骨頸部/転子部骨折</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">大腿骨頚部/転子部骨折診療ガイドライン改訂第2版（2011）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">大腿骨頸部/転子部骨折患者は，2020年には約25万人，2030年には約30万人，2042年には約32万人発生すると推計される。わが国の発生率は北欧・米国に比べると約半分と低く，南欧・東南アジアとほぼ同様である。北欧・北米では発生率が減少し始めている。</li>
        <li class="whatsnew_リスト">内科的合併症で手術が遅れる場合を除き，できる限り早期の手術が推奨される。</li>
        <li class="whatsnew_リスト">「大腿骨頚部/転子部骨折診療ガイドライン改訂第2版」は2011年6月に改訂出版された<span class="上付き">1</span><span class="上付き">）</span>。分類，疫学から退院後の管理までの10章からなり，初版のClinical Question（CQ）が見直された。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">①大腿骨頸部/転子部骨折の診断フローチャート</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/09-09-A01.svg"/>
          <p class="図表_注">1）診断確定するまで安静を指示する</p>
          <p class="図表_注">2）4～24時間以上経過してから，STIR，T1，T2</p>
          <p class="図表_注">3）72時間以上経過してから撮影</p>
          <p class="図表_注">4）MRIによる骨折診断，T1 low，T2 high</p>
          <p class="図表_注">5）偽陽性がありover-diagnosisに注意</p>
          <p class="図表_引用">（「日本整形外科学会診療ガイドライン委員会，他 編：大腿骨頚部/転子部骨折診療ガイドライン改訂第2版，p71，2011，南江堂」より許諾を得て転載）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="2">②大腿骨頸部骨折の治療・初期治療の選択</div>
      <div class="アルゴリズム" order="2">
        <div class="図表" type="A" order="2">
          <img class="width-800" src="./image/09-09-A02.svg"/>
          <p class="図表_引用">（「日本整形外科学会診療ガイドライン委員会，他 編：大腿骨頚部/転子部骨折診療ガイドライン改訂第2版，2011，南江堂」より作成）</p>
          <p class="図表_注">・本ガイドラインの推奨グレードを<span class="本文_図表番号">表</span><span class="本文_図表番号">1</span>に示す。</p>
          <div class="図表" type="T" order="1">
            <p class="図表_タイトル" type="T" order="1" data-file="09-09-T01.svg">表1　推奨Grade</p>
            <img class="width-400" src="./image/09-09-T01.svg"/>
            <p class="図表_引用">（文献1）より抜粋）</p>
          </div>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶大腿骨頸部/転子部骨折の名称に関しては，これまで少なからず混乱を生じていた。そこで初版作成時に本骨折の名称が検討され，それまでわが国で用いられてきた「内側骨折」，「外側骨折」という名称をやめ，それぞれ頸部骨折（neck fracture），転子部骨折（trochanteric fracture）と呼称し，両者を合わせた骨折を「大腿骨近位部骨折」あるいは「大腿骨頸部/転子部骨折」と呼称することとした（<span class="本文_図表番号">図</span><span class="本文_図表番号">1</span>）<span class="上付き">2</span><span class="上付き">）</span><span class="上付き">3</span><span class="上付き">）</span>。本稿では以後，大腿骨頸部/転子部骨折とする。</p>
      <p class="本文_先頭丸数字">❷頸部骨折には<span class="本文_色文字太字">Garden</span><span class="本文_色文字太字">の</span><span class="本文_色文字太字">4</span><span class="本文_色文字太字">段階分類</span>が用いられるが，検者間での分類判定の一致率が低いため，stageⅠとⅡを非転位型，stageⅢとⅣを転位型として<br/> 2つに分類するのが主流である。</p>
      <p class="本文_先頭丸数字">❸転子部骨折の分類には主に<span class="本文_色文字太字">Evans</span><span class="本文_色文字太字">分類</span>が用いられ，Type1（group1～4）とType2に分けられる（group1，2が安定型，その他は不安定型）。この他，Jensen分類，AO分類も用いられる。</p>
      <div class="図表" type="F" order="1">
        <img class="width-400" src="./image/09-09-F01.svg"/>
        <p class="図表_タイトル" type="F" order="1" data-file="09-09-F01.svg">図1　大腿骨頸部/転子部骨折の分類</p>
        <p class="図表_注">矢印は関節包の付着部を示す。</p>
        <p class="図表_引用">（文献3））</p>
      </div>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶本骨折はX線像で容易に診断ができるように思われるが，超高齢者の増加とそれに伴う著しい骨脆弱化例が多いために，X線像では骨折線がわからない症例が増加している。そこで本ガイドラインではこのような<span class="本文_色文字太字">不顕性骨折（</span><span class="本文_色文字太字">occult</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">fracture</span><span class="本文_色文字太字">）</span>について，過去の臨床研究結果に基づいて診断の手順を示している（<span class="本文_図表番号">アルゴリズム①</span>）。</p>
      <p class="本文_先頭丸数字">❷多くの骨折はX線単純写真で診断できるが，骨折線が認められなくても骨折がないと断定はできない。X線単純写真による正診率は98.1％，96.7％である。臨床的に股関節周辺骨折を疑うが，X線単純写真で診断できない場合に選択する検査については，第一選択をMRI（Grade Ⅰa），MRIが実施できない場合や禁忌の場合には，骨シンチグラフィーを第二選択とし（Grade Ⅰb），日をおいて繰り返しX線単純写真を撮影することも有用である（Grade Ⅰb）（<span class="本文_図表番号">アルゴリズム②</span>）。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p>　治療は頸部骨折と転子部骨折とで異なる（<span class="本文_図表番号">図</span><span class="本文_図表番号">1</span>）。</p>
      <p class="本文_先頭丸数字">❶<span class="本文_太字">頸部骨折</span>：非転位型骨折の保存的治療では偽関節発生率が高いので，全身状態が手術に耐えうる症例には保存療法を行わないほうがよい（Grade Ⅰd）と推奨される。さらに外科的治療では非転位型には骨接合術が（Grade Ⅰb），高齢者の転位型は人工物置換術が（Grade A），それぞれ推奨される。ただし，対象患者の全身状態，年齢を考慮して，手術法が選択される。これは転位型では非転位型よりも骨癒合率が低く，<span class="本文_色文字太字">骨頭壊死</span>や<span class="本文_色文字太字">late</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">segmental</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">collapse</span><span class="本文_色文字太字">（</span><span class="本文_色文字太字">LSC</span><span class="本文_色文字太字">）</span>の頻度が高く，骨接合術のほうが短期間に再手術に至る確率が高いためである。また，人工物置換術は手術侵襲が大きく，長期的にみれば時間の経過とともに再置換率が高まると考えられるので，対象患者の全身状態が悪い場合や年齢が若い場合には手術法は慎重に選択される。人工物置換術を実施する場合，人工骨頭置換術と全人工股関節置換術（THA）のいずれかが選択される。活動性が高い症例にはTHAを推奨し，全身状態が悪い症例や高齢で活動性が低い症例には人工骨頭置換術が推奨される。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">転子部骨折</span>：①転位のある大腿骨転子部骨折は骨接合術が推奨される（Grade A）。②転位のない大腿骨転子部骨折は保存的治療も可能であるが，骨接合術が推奨される（Grade Ⅰb）。③転位のない大腿骨転子部のみの骨折は保存的治療が推奨される（Grade Ⅰb）。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">適切な手術時期</span>：本骨折ではできる限り早期の手術が推奨される（Grade B）。最近の報告では緊急で24時間以内に手術する必要はないものの，内科的合併症で手術が遅れる場合を除いて，できるだけ早期に手術を行うべきであるという報告が多くなっている。わが国でも早期手術の有効性が報告されているが，現在の医療体制では欧米並みの早期手術を行うことは実際には困難な場合が多く，重要な課題である。</p>
      <p class="本文_先頭丸数字">❹<span class="本文_太字">リハビリテーション</span></p>
      <p class="本文_先頭丸数字">①退院後のリハビリテーションの有効性が強調されている（Grade B）。さらに術後のリハビリテーションは，術後最低6ヵ月程度継続することが勧められる（Grade B）。</p>
      <p class="本文_先頭丸数字">②改訂第2版ではリハビリテーションにおけるクリニカルパスの意義についての推奨が追加となり，クリニカルパスが受傷前のADLが高い症例において，入院期間の短縮と術後合併症の防止に有効であることが示された（Grade B）。</p>
      <p class="本文_先頭丸数字">❺<span class="本文_太字">対側の骨折予防策</span>：本骨折を生じた患者は，対側の大腿骨頸部/転子部骨折のリスクが明らかに高いことから，骨粗鬆症治療や転倒予防対策を講じることが望ましい（Grade B）。</p>
      <h4 class="文献_見出し">文献</h4>
      <p class="文献_本文">1）日本整形外科学会診療ガイドライン委員会 編．大腿骨頚部/転子部骨折診療ガイドライン改訂第2版．南江堂；2011．</p>
      <p class="文献_本文">2）日本整形外科学会診療ガイドライン委員会 編．大腿骨頚部/転子部骨折診療ガイドライン．南江堂；2005．</p>
      <p class="文献_本文">3）Scottish Intercollegiate Guidelines Network. Management of hip fracture in older people. Edinburgh；2009.</p>
      <p class="文献_本文">4）Orimo H, et al. Osteoporos Int. 2016；<span class="本文_太字">27</span>：1777-84.</p>
      <p class="文献_本文">5）Wilson H. Best Pract Res Clin Rheumatol. 2013；27：717-30.</p>
      <p class="文献_本文">6）Marsh D, et al：Osteoporos Int. 2011；<span class="本文_太字">22</span>：2051-65.</p>
      <p class="文献_本文">7）Hagino H, et al. J Orthop Sci. 2017；<span class="本文_太字">22</span>：909-14.</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">①日本人の大腿骨頸部/転子部骨折の性・年齢階級別の発生率は経年的に上昇傾向にあることが報告されていた。しかしながら近年では年齢によっては低下傾向がみられている<span class="上付き">4</span><span class="上付き">）</span>。</p>
      <p class="本文_最近の話題">②大腿骨頸部/転子部骨折は高齢者，なかでも85歳以上の超高齢者が占めるため，多くの併存症を有する。したがって，その治療にあたっては単に骨折に対する治療のみではなく，併存症や老年症候群を考慮した対応が求められる。そこで老年病科医，理学療法士，作業療法士，看護師，栄養士，社会福祉士など多職種がチームとなって整形外科医とともに周術期治療に対応する集学的治療が推進され，その良好な臨床成績が報告されている<span class="上付き">5</span><span class="上付き">）</span>。</p>
      <p class="本文_最近の話題">③大腿骨頸部/転子部骨折を生じた例では，再びこの骨折を引き起こすリスクがきわめて高い。また，大腿骨頸部/転子部骨折例の約半数は脆弱性骨折を以前に起こしたことがある。そこで近年，<span class="本文_色文字太字">骨折リエゾンサービス（</span><span class="本文_色文字太字">FLS</span><span class="本文_色文字太字">）</span>と呼ばれる2次骨折への取り組みが世界各国で進められている。FLSは医師ではなく看護師をはじめとしたメディカルスタッフがリエゾン（連絡係）となって，すべての脆弱性骨折に対して2次骨折防止のための骨粗鬆症治療をマネージメントする仕組みである<span class="上付き">6</span><span class="上付き">）</span>。わが国ではリエゾンとなる骨粗鬆症マネージャーの認定制度が開始され，2017年3月までに1867名のマネージャーが誕生した。</p>
      <p class="本文_最近の話題">④日本整形外科学会の調査結果<span class="上付き">7</span><span class="上付き">）</span> によると，2014年のわが国の大腿骨頸部/転子部骨折例の術前待機期間は平均4.5日と長い。手術室が骨折手術に使用できない点が術前待機期間に3日以上を要する最も大きな要因であった（オッズ比2.82倍）。</p>
    </div>
    <!--最近の話題 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>鳥取大学保健学科<span class="著者名">　萩野　　浩</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
