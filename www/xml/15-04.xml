<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>15</chapter>
    <chapter-title>婦人科疾患</chapter-title>
    <section>04</section>
    <article-code>15-04</article-code>
    <article-title>卵巣癌・腹膜癌</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>4 卵巣癌・腹膜癌</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">卵巣がん治療ガイドライン2015年版（2015）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">これまでの総説形式から41項目のCQ形式に一新された。</li>
        <li class="whatsnew_リスト"><span class="本文_色文字太字">分子標的薬</span>を用いた治療に関する新事項が追加された。</li>
        <li class="whatsnew_リスト">遺伝性乳癌卵巣癌の検査基準と対応が記載された。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">卵巣癌の治療アルゴリズム</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/15-04-A01.svg"/>
          <p class="図表_引用">（日本婦人科腫瘍学会（編）．卵巣がん治療ガイドライン2015年版．金原出版；2015．より許諾を得て転載）</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶本ガイドラインでは，卵巣原発の悪性および境界悪性腫瘍，腹膜癌ならびに卵管癌を取り扱っている。</p>
      <p class="本文_先頭丸数字">❷表層上皮性・間質性悪性腫瘍を示す「卵巣癌」と，他の悪性卵巣腫瘍である胚細胞腫瘍，性索間質性腫瘍のすべてを包含し，「卵巣がん」の用語が用いられている。</p>
      <p class="本文_先頭丸数字">❸腹膜癌および卵管癌も治療法の選択において卵巣がんと密接に関連することから，本ガイドライン中に加えられている。</p>
      <p class="本文_先頭丸数字">❹本邦の卵巣がん罹患数，死亡数は増加傾向にあり，女性性器悪性腫瘍中，最も死亡数が多い疾患である。初期の段階での自覚症状に乏しいため，予後不良なⅢ/Ⅳ期症例が40～50％を占めており，進行症例における治療成績向上が重要な課題となっている。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶無症状のうちに進行している場合が多く，初発症状として腹部腫瘤，腹部膨満感，周囲臓器への圧迫症状，腹痛などを訴えて婦人科以外の診療科を受診することもあり注意を要する。視診，骨盤双合診に加えて超音波断層法などが診断に有用である。</p>
      <p class="本文_先頭丸数字">❷卵巣がんでは腫瘍マーカーとしてCA125，CA19-9，CEAなどが上昇していることが多い。</p>
      <p class="本文_先頭丸数字">❸超音波断層法，CTやMRIなどの画像診断では，囊胞の数や内容液の性状，充実部位の有無や不整な像，腹水貯留や播種の有無などに留意して良悪性の推定と進行度を判断する。</p>
      <p class="本文_先頭丸数字">❹組織学的な評価と進行期の決定のためには手術が必須であり，摘出組織の病理学的診断により確定診断が行われる。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p>　<span class="本文_色文字太字">手術療法と化学療法を組み合わせて行われる</span>。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/15-04-小見出し_矢印.png"/>手術療法</div>
      <p class="本文_先頭丸数字">❶<span class="本文_色文字太字">手術の目的は組織型の確定と</span><span class="本文_色文字太字">surgical</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">staging</span><span class="本文_色文字太字">を行うこと</span>であり，その際には<span class="本文_色文字太字">病巣の完全摘出を目指した最大限の腫瘍減量を行う</span>。</p>
      <p class="本文_先頭丸数字">❷推奨される手術術式は，卵巣に限局していると予想される症例では両側付属器摘出術＋子宮全摘出術＋大網切除術に加え，腹腔細胞診＋骨盤・傍大動脈リンパ節郭清（生検）＋腹腔内各所の生検（グレードB，C1），進行卵巣癌では肉眼的残存腫瘍がない状態を目指した最大限の腫瘍減量術が強く奨められる（場合によっては腸管部分切除・再建術／人工肛門造設術も考慮される）（グレードA）。最大残存腫瘍径1 cm未満（optimal surgery）で予後が改善するとされ，肉眼的残存腫瘍のない状態（complete surgery）ではさらに有意に予後が改善する。</p>
      <p class="本文_先頭丸数字">❸初回手術で最大残存腫瘍径が1 cm以上（suboptimal surgery）の症例では，初回化学療法中に可及的に最大限の腫瘍減量を行う手術（interval debulking surgery；IDS）が選択肢として考慮される（グレードC1）。また，初回手術でoptimal surgeryが不可能と予想される場合には術前化学療法（NAC）後にIDSを行うことが選択肢として考慮される（グレードB）。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/15-04-小見出し_矢印.png"/>化学療法</div>
      <p class="本文_先頭丸数字">❶初回化学療法としては，パクリタキセル＋カルボプラチン（TC）療法（グレードA）あるいはdose-dense（dd）TC療法（グレードB）が推奨される。末梢神経障害合併症が危惧される例やアルコール不耐例ではドセタキセル＋カルボプラチン（DC）療法が考慮される（グレードB）。</p>
      <p class="本文_先頭丸数字">❷化学療法を省略できる条件は，staging laparotomyにより確定したⅠa・Ⅰb期かつgrade 1の症例である（グレードB）。</p>
      <p class="本文_先頭丸数字">❸組織型によってTC療法以外の化学療法への変更についてのエビデンスはいまだなく，現時点では推奨されない（グレードC2）。</p>
      <p class="本文_先頭丸数字">❹分子標的薬としてベバシズマブが承認され，化学療法との併用またはその後の維持療法として用いられ，上乗せ効果が確認されている。重大な有害事象として消化管穿孔，血栓塞栓症，高血圧などが報告され，慎重な患者選択と適切な有害事象のモニターが必要である（グレードC1）。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/15-04-小見出し_矢印.png"/>その他</div>
      <p class="本文_先頭丸数字">❶<span class="本文_イタリック">BRCA1</span>あるいは<span class="本文_イタリック">BRCA2</span>遺伝子変異をもつ女性に対するrisk-reducing salpingo-oophorectomy（RRSO）については，遺伝カウンセリング体制ならびに病理医の協力体制が整っている施設において，倫理委員会による審査を受けたうえで，日本婦人科腫瘍学会婦人科腫瘍専門医が臨床遺伝専門医と連携してRRSOを行うことが推奨されている（グレードB）。また<span class="本文_イタリック">BRCA1</span><span class="本文_イタリック">/</span><span class="本文_イタリック">2</span>遺伝子検査をどのような女性に奨めるかについては，十分な問診と家族歴を聴取したうえで決定するべきである。</p>
      <p class="本文_先頭丸数字">❷無症状でCA125上昇のみが認められる症例に対する再発治療は予後を改善せず，QOLを有意に低下させたとの報告から，必ずしも推奨されない（グレードC2）。</p>
      <p class="本文_先頭丸数字">❸ホルモン補充療法（HRT）は再発のリスクを高めないと考えられ，エストロゲン投与によりQOL改善が期待される場合にはHRTの施行が考慮される（グレードC1）。</p>
      <p class="本文_先頭丸数字">❹本ガイドラインには卵巣癌の治療だけでなく，上皮性境界悪性腫瘍，再発卵巣癌，腹膜癌・卵管癌，胚細胞腫瘍，性索間質性腫瘍まで網羅されており，婦人科腫瘍に関わる医療関係者必携の一冊である。進行期分類・組織学的分類・手術術式・化学療法・緩和ケアの基本事項に関する解説が追加され，卵巣癌における実地診療についての情報が広く記載されている。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①IDSの必要性が予想される進行卵巣癌症例</p>
      <p class="本文_専門医へのコンサルト">②ベバシズマブ投与が考慮される症例</p>
      <p class="本文_専門医へのコンサルト">③臨床試験や治験への参加が見込まれる症例</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p>①近年，がん細胞による宿主免疫を回避する分子機構の阻害，いわゆる「免疫抑制の解除」によって，がん細胞の非自己性を免疫系に再度認識させることで抗腫瘍効果を発揮する薬剤である「免疫チェックポイント阻害薬」に注目が集まっている。特にT細胞の活性化後期に機能する免疫チェックポイント分子であるPD-1に対する，完全ヒト型抗PD-1抗体であるニボルマブは，根治切除不能な悪性黒色腫に対する新規治療薬として2014年に世界に先駆けて本邦で承認されて以来，さまざまな癌腫に対しても臨床試験が進行している。卵巣癌においては，2011年9月より本邦における医師主導型治験として「プラチナ抵抗性再発・進行卵巣癌に対する抗PD-1抗体を用いた免疫療法に関する第Ⅱ相試験」が行われ，有効性と安全性が示された（Hamanishi J, et al. J Clin Oncol. 2015；<span class="本文_太字">33</span>：4015-22.）。今後，卵巣癌に対する新たな治療薬としての期待がもたれている。</p>
      <p>②さらに近年，The Cancer Genome Atlas dataの解析によって，卵巣漿液性癌が組織の遺伝子発現プロファイルにより4つのサブタイプ（mesenchymal，immunoreactive，proliferative，differentiated）に分類でき，サブタイプごとに予後や化学療法感受性が違うことが報告された（Cancer Genome Atlas Research Network. Nature. 2011；<span class="本文_太字">474</span>：609-15., Verhaak RG, et al. J Clin Invest. 2013；<span class="本文_太字">123</span>：517-25.）。一方，Murakamiらは腫瘍間質所見を加味した新たな病理組織細分類〔mesenchymal transition（MT），immune reactive（IR），solid and proliferative（SP），papillo-glandular（PG）〕が上記の遺伝子発現サブタイプとよく相関し，MTタイプが最も予後が不良であるが，タキサン系の化学療法に感受性が高いことを報告した（Murakami R, et al. Am J Pathol. 2016；<span class="本文_太字">186</span>：1103-13.）。これらより，TC療法とddTC療法の効果についてのランダム化試験であるJGOG3016試験に登録された漿液性癌患者の病理組織型を4つに細分類し，細分類ごとの予後と化学療法の有効性についての検討が2017年の米国臨床腫瘍学会（ASCO）で報告された（Murakami R, et al. J Clin Oncol. 2017；<span class="本文_太字">35</span> Suppl：abstr 5510）。その結果，MTタイプではddTC療法がTC療法と比して有意に予後を改善する効果が認められた。この結果から，遺伝子検査をせずHE染色標本の結果で治療の個別化ができる可能性が示され，今後のさらなる解析が待たれる。</p>
      <p>③また，PARP阻害薬であるolaparibによる<span class="本文_イタリック">BRCA</span><span class="本文_イタリック">1</span><span class="本文_イタリック">/</span><span class="本文_イタリック">2</span>遺伝子変異を有するプラチナ感受性再発卵巣癌患者に対する維持効果（PFS：olaparib群19.1ヵ月vs.プラセボ群5.5ヵ月，ハザード比0.30，p＜0.0001）が報告された（Pujade-Lauraine E, et al. Lancet Oncol. 2017；<span class="太字">18</span>：1274-84.）。本邦においても2017年3月に厚生労働大臣より希少疾病用医薬品（オーファンドラッグ）の指定を受け，2018年内には<span class="本文_イタリック">BRCA</span>遺伝子変異陽性の卵巣癌に対して投与が可能になると見込まれている。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】手術療法とそれに続く化学療法が卵巣癌治療の基本である。手術療法では，病巣の完全摘出を目指した最大限の腫瘍減量を行う。術後，進行期に応じて化学療法を追加する。初回完全摘出が不可能な症例や重篤な合併症症例に対して，NACを数サイクル行った後に手術（IDS）を行うことも選択肢の1つである。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶卵巣癌</span>
              </p>
            </td>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅰ）TC療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">タキソール</span></p>
              <p class="具体的処方_処方例1W下げ">175～180 mg/m<span class="上付き">2</span>×1回，点滴静注（3時間），day1</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">パラプラチン</span></p>
              <p class="具体的処方_処方例1W下げ">AUC5～6×1回，点滴静注（1時間），day1</p>
              <p class="具体的処方_処方例">上記①＋②を3週毎，3～6サイクル</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">現時点でTC療法が卵巣癌の標準療法である。これまでにTC療法に対して生存期間を延長したレジメンは，腹腔内化学療法とパクリタキセルの毎週投与法（dose-dense TC療法）であるが，標準治療としては，まだ確立されていない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）DC療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">タキソテール</span></p>
              <p class="具体的処方_処方例1W下げ">70～75 mg/m<span class="上付き">2</span>×1回，点滴静注（1時間），day1</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">パラプラチン</span></p>
              <p class="具体的処方_処方例1W下げ">AUC5×1回，点滴静注（1時間），day1</p>
              <p class="具体的処方_処方例">上記①＋②を3週毎，6サイクル</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">DC療法は，SCOTROC1試験におけるTC療法との比較で，奏効率，無増悪生存期間（PFS）で差を認めなかったレジメンであり，末梢神経障害の合併症が危惧される症例，アルコール不耐例に関しては，DC療法が考慮される。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅲ）TC-Bev療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①TC療法（上記のとおり）</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">アバスチン</span></p>
              <p class="具体的処方_処方例1W下げ">15 mg/kg×1回，点滴静注（初回90分），day1</p>
              <p class="具体的処方_処方例">上記①＋②を4週毎，6サイクル（7～21サイクルは<span class="具体的処方_色文字">アバスチン</span>単剤のみ投与）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">GOG218試験およびICON7試験において，卵巣癌初回化学療法における②の有用性が示されている。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶再発卵巣癌</span>
              </p>
            </td>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅰ）PLD-C療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ドキシル</span></p>
              <p class="具体的処方_処方例1W下げ">30 mg/m<span class="上付き">2</span>×1回，点滴静注（1時間），day1</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">パラプラチン</span></p>
              <p class="具体的処方_処方例1W下げ">AUC5×1回，点滴静注（1時間），day1</p>
              <p class="具体的処方_処方例">上記①＋②を4週毎，6サイクル</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">CALYPSO試験により，プラチナ感受性癌に対し，PLD-C療法のTC療法に対するPFS・全生存期間（OS）での非劣性が証明されている。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）GC療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ジェムザール</span></p>
              <p class="具体的処方_処方例1W下げ">1000 mg/m<span class="上付き">2</span>×1回，点滴静注（30分），day1, 8</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">パラプラチン</span></p>
              <p class="具体的処方_処方例1W下げ">AUC4×1回，点滴静注（1時間），day1</p>
              <p class="具体的処方_処方例">上記①＋②を3週毎，6サイクル</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">OVAR2.5試験により，プラチナ感受性癌に対し，プラチナ単剤療法に比し，GC療法が有効であることが証明されている。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>熊本大学大学院産科婦人科学<span class="著者名">　宮原　　陽・片渕　秀隆</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
