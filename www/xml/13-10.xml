<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>13</chapter>
    <chapter-title>耳鼻咽喉科疾患</chapter-title>
    <section>10</section>
    <article-code>13-10</article-code>
    <article-title>顔面神経麻痺</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>10 顔面神経麻痺</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">顔面神経麻痺診療の手引─Bell麻痺とHunt症候群─（2011）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">塩基性線維芽細胞成長因子を用いた顔面神経減荷術の神経再生効果が臨床検討されている。</li>
        <li class="whatsnew_リスト">人工神経を用いた再建手術が神経断裂による顔面神経麻痺に対して用いられるようになってきた。</li>
        <li class="whatsnew_リスト">2年以上経過しても回復のない陳旧性麻痺に対して，筋肉移行術や筋肉移植術などの動的再建が施行される。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">顔面神経麻痺の診断アルゴリズム</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/13-10-A01.svg"/>
          <p class="図表_注">HSV-1：herpes simplex virus type 1，VZV：varicella zoster virus</p>
          <p class="図表_引用">（筆者作成）</p>
          <p class="図表_注">・各種検査を施行しつつ，原因を検索していく。</p>
          <p class="図表_注">・主な原因はBell麻痺とHunt症候群である。</p>
          <p class="図表_注">・原因，重症度，治療開始時期により治療が異なる（総説参照）。</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶顔面表情筋運動を支配する第Ⅶ脳神経（顔面神経）が，大脳皮質から最終効果器（顔面表情筋）に至るまでの過程で，何らかの原因により機能障害に陥って惹起される病態。</p>
      <p class="本文_先頭丸数字">❷中枢性麻痺と末梢性麻痺がある<span class="上付き">1</span><span class="上付き">）</span>。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶顔面神経麻痺の原因は多岐にわたる（<span class="本文_図表番号">表</span><span class="本文_図表番号">1</span>）。特に末梢性顔面神経麻痺の原因は多様である。</p>
      <div class="図表" type="T" order="1">
        <p class="図表_タイトル" type="T" order="1" data-file="13-10-T01.svg">表1　顔面神経麻痺の原因疾患</p>
        <img class="width-800" src="./image/13-10-T01.svg"/>
        <p class="図表_引用">（文献1）より一部改変）</p>
      </div>
      <p class="本文_先頭丸数字">❷顔面神経の全神経走行のいずれの部分の障害でも顔面神経麻痺は出現しうる。原因疾患として頻度の高いものは，<span class="本文_色文字太字">特発性顔面神経麻痺（</span><span class="本文_色文字太字">Bell</span><span class="本文_色文字太字">麻痺）であり，全体の約</span><span class="本文_色文字太字">6</span><span class="本文_色文字太字">割前後を占める</span>。これは，膝神経節における単純ヘルペスウイルス1型（HSV-1）の先行感染の再活性化がその主因と考えられている。</p>
      <p class="本文_先頭丸数字">❸これと酷似する臨床像を呈するもののなかに，帯状疱疹ウイルス（VZV）の関わるHunt症候群の不全型が紛れ込んでいて<span class="本文_色文字太字">無疱疹性帯状疱疹（</span><span class="本文_色文字太字">ZSH</span><span class="本文_色文字太字">）</span>といわれる。血清抗体価を調べれば推定可能である。Bell麻痺より麻痺の程度が一般に強く出ることが多いが臨床的に早期にZSHをBell麻痺と鑑別することは難しい。</p>
      <p class="本文_先頭丸数字">❹VZVの感染ないし再活性化による顔面麻痺（Hunt症候群）は一般に重症であり，ときに，耳介・外耳道・鼓膜に痛みを伴う皮疹（小水泡）とともに，さまざまな程度の難聴やめまいを合併する場合がある。</p>
      <p class="本文_先頭丸数字">❺その他のウイルス性ないし細菌性の麻痺は，病歴にみられる特徴やウイルス感染・細菌感染に関する血液・生化学的諸検査を施行して鑑別を進める。</p>
      <p class="本文_先頭丸数字">❻外傷性麻痺は，病歴で判断できる。側頭骨骨折を伴う麻痺の場合，受傷時点ですでに麻痺が出現している<span class="本文_色文字太字">即発性麻痺</span>と少し遅れて麻痺が出現する<span class="本文_色文字太字">晩発性麻痺</span>とがある。前者は神経断裂の可能性があるので，早期の外科処置を考えなければならない。後者の場合には，予後を慎重に見極めて対応する必要がある。</p>
      <p class="本文_先頭丸数字">❼手術損傷の場合も麻痺の程度，障害部位，回復の進展度を見極めながら必要に応じて次の処置（一般には外科処置）をすることになる。</p>
      <p class="本文_先頭丸数字">❽腫瘍性麻痺は，顔面神経鞘腫による（何回か麻痺が再発する経過を示す場合がある）ものと，顔面神経以外の組織の腫瘍性病変による圧迫や浸潤が関与しているものとがある。特に，見落としてならないのは，<span class="本文_色文字太字">耳下腺癌</span>である。とりわけ耳下腺深葉由来の腫瘍で，副咽頭間隙方向に進んでいる場合，耳下腺部の腫脹の視診・触診での確認が困難なことが少なくない。このような場合，麻痺の障害部位診断を行えば，涙分泌，アブミ骨反射，味覚がすべて正常に出てくる顔面神経麻痺であることから，顔面神経垂直部の鼓索神経分枝部よりさらに末梢に障害があることを示し，CTないしMRIを施行すれば，正診につながる。</p>
      <p class="本文_先頭丸数字">❾腫瘍が関わった麻痺の場合，麻痺が長く続いても後遺症が伴わないことが多く，治りの悪いBell麻痺とは多少とも経過が異なることにも注目すべきであろう。後者であれば，多少とも後遺症を併発している。</p>
      <p class="本文_先頭丸数字">❿<span class="本文_色文字太字">全身性疾患の一部分症状</span>として顔面麻痺が出現することもあり，神経系疾患では，多発性硬化症やギラン・バレー症候群などに注意しなければならない。</p>
      <p class="本文_先頭丸数字">⓫中枢性病変による場合は，血管障害によることが多く，まれに腫瘍によることもある。その他，先天性のものがある。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_級下げ">　原因として最も多い，Bell麻痺とHunt症候群を中心に述べる。</p>
      <p class="本文_先頭丸数字">❶麻痺発症の病態は，顔面神経膝神経節に主にウイルス感染ないし，この先行感染の再活性化による神経障害である。このため，神経は膨化（浮腫）する。しかし，顔面神経は，内耳道から側頭骨内に入って骨内（顔面神経管）を走行し茎状突起の付け根で側頭骨外に出て耳下腺の浅葉と深葉の間を貫いて表情筋に至る。側頭骨の中で膨化した神経は，骨内（顔面神経管）管腔は変わらず一定であるために，相対的に絞扼を受け，血流障害（虚血）を起こす。神経周膜による相対的絞扼も考慮しなければならない。このことは，さらに浮腫を悪化させるという<span class="本文_色文字太字">悪性サイクル</span>に陥っている。</p>
      <p class="本文_先頭丸数字">❷したがって，この悪性サイクルから解放させる治療手段を講じなければならない。使用薬剤は，<span class="本文_色文字太字">ステロイド，抗ウイルス薬</span>とともに<span class="本文_色文字太字">循環改善薬，ビタミン</span><span class="本文_色文字太字">B</span><span class="下付き">12</span><span class="本文_色文字太字">，</span><span class="本文_色文字太字">ATP</span><span class="本文_色文字太字">製剤</span>である。</p>
      <p class="本文_先頭丸数字">❸具体的な治療メニューは，神経障害の程度（神経変性の程度）を各種検査で把握し，病態を把握したうえで，その予後を見極めて立案しなければならない。予後判断をする方法には<span class="本文_色文字太字">40</span><span class="本文_色文字太字">点法</span>（<span class="本文_色文字太字">柳原法</span>）（<span class="本文_図表番号">図</span><span class="本文_図表番号">1</span>），electroneuronography（<span class="本文_色文字太字">ENoG</span>），神経興奮性検査（<span class="本文_色文字太字">NET</span>），最大刺激検査（<span class="本文_色文字太字">MST</span>）がある（<span class="本文_図表番号">表</span><span class="本文_図表番号">2</span>）。NETとMSTは，正確性においてはENoGより劣るため，ENoGが最も信頼度が高い。参考までに40点法と<span class="本文_色文字太字">Hous</span><span class="本文_色文字太字">e</span><span class="本文_色文字太字">-</span><span class="本文_色文字太字">B</span><span class="本文_色文字太字">rackmann</span>法との比較も示した（<span class="本文_図表番号">表</span><span class="本文_図表番号">3</span>）。</p>
      <div class="図表" type="F" order="1">
        <img class="width-800" src="./image/13-10-F01.svg"/>
        <p class="図表_タイトル" type="F" order="1" data-file="13-10-F01.svg">図1　40点法（柳原法）</p>
      </div>
      <div class="図表" type="T" order="2">
        <p class="図表_タイトル" type="T" order="2" data-file="13-10-T02.svg">表2　顔面麻痺の予後について</p>
        <img class="width-400" src="./image/13-10-T02.svg"/>
      </div>
      <div class="図表" type="T" order="3">
        <p class="図表_タイトル" type="T" order="3" data-file="13-10-T03.svg">表3　House-Brackmann法と40点法との比較</p>
        <img class="width-400" src="./image/13-10-T03.svg"/>
      </div>
      <p class="本文_先頭丸数字">❹<span class="本文_色文字太字">病態の重症度に応じて用量を変えるのは，ステロイドと抗ウイルス薬を使用する時である</span>。また，ステロイドを使用する時には，健胃薬を併用することが望ましい。</p>
      <p class="本文_先頭丸数字">❺<span class="本文_太字">後遺症の発症を最小限にする</span><span class="本文_色文字太字">リハビリテーション</span><span class="本文_太字">：</span>顔面神経麻痺に陥った患者が，病的共同運動（後遺症）を残さぬような回復を進めて行くうえで，リハビリテーションは重要な位置を占める。ENoG値が40％以上を示して，麻痺の病態として脱髄のみであるならば後遺症を残さない回復が期待されるが，麻痺がより重症で軸索断裂や神経断裂を伴う場合には，既述の薬物治療に加えて，適正なリハビリテーションが重要である。特に急性期には，過誤再生を助長する表情筋の過剰な顔面運動を回避し，表情筋のマッサージをすることが必要である。このとき，強く大きい表情筋運動を避け，表情筋の伸張マッサージ（ストレッチング）を頻繁に行い，上眼瞼挙筋を使った伸張を行うことが重要となる<span class="上付き">3</span><span class="上付き">）</span>。</p>
      <p><img class="" src="./image/13-10-2.png"/>末梢性顔面神経麻痺に対する減荷術の効果が再評価されてきている。</p>
      <h4 class="文献_見出し">文献</h4>
      <p class="文献_本文">1）石川和夫，他．医事新報．1997；<span class="本文_太字">3808</span>：16-23．</p>
      <p class="文献_本文">2）日本顔面神経研究会（編）．顔面神経麻痺診療の手引─Bell麻痺とHunt症候群─．金原出版；2011．</p>
      <p class="文献_本文">3）栢森良二．顔面神経麻痺が起きたらすぐに読む本．A・M・S；2011．</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①検査機器の制限により，正確な予後判断のできない中等度以上の麻痺症例</p>
      <p class="本文_専門医へのコンサルト">②治療開始後に，回復が遅れENoG値（最低値）の悪い症例。ENoG値は発症早期で変性が進まないうちは良い値が出ることに注意。</p>
      <p class="本文_専門医へのコンサルト">③外科処置（外傷や腫瘍など）が必要と判断する症例</p>
      <p class="本文_専門医へのコンサルト">④中枢性麻痺の場合</p>
      <p class="本文_専門医へのコンサルト">⑤後遺症や残存陳旧性麻痺への対応が求められるとき</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">　帯状疱疹ウイルスによるHunt症候群かHSV-1が関与するBell麻痺であるかは，ペア血清によるウイルス抗体検査でおよその判断ができるが，検査センターや病院の状況，検査回数など（初診時の血清検査で判定できた場合や，複数回の比較での判定など）により4日～3週間と時間を要し，結果的にHunt症候群としての治療が後手に回ることが懸念される。最近は発症早期でも，唾液（咽頭ぬぐい液）を用いたreal time PCR法による診断で，ZSHの検索が可能になったが，保険適用外で全国どこでも検査できるものではないため，今後の対応が待たれるところである。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】麻痺発症から治療開始までの日数と重症度との関係で調整。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">Bell</span>
                <span class="具体的処方_色文字">麻痺</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）麻痺発症7日以内に治療開始の場合</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">ⅰ）ⅱ）ともに基本薬剤として<span class="具体的処方_色文字">ビタミン</span><span class="具体的処方_色文字">B</span><span class="下付き">12</span>，<span class="具体的処方_色文字">ATP</span><span class="具体的処方_色文字">製剤</span>，<span class="具体的処方_色文字">循環改善薬</span>を用いる</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ａ）20点以上（軽度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プレドニゾロン</span></p>
              <p class="具体的処方_処方例1W下げ">30 mg/日，10日で漸減</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">ステロイド治療時は健胃薬を併用することが望ましい。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ｂ）18～10点（中等度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プレドニゾロン</span></p>
              <p class="具体的処方_処方例1W下げ">60 mg/日，10日で漸減</p>
              <p class="具体的処方_処方例">＋<span class="具体的処方_色文字">バルトレックス</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×2回/日，5日間</p>
              <p class="具体的処方_処方例">　または<span class="具体的処方_色文字">ファムビル</span></p>
              <p class="具体的処方_処方例1W下げ">250 mg×3回/日，5日間</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ｃ）8点以下（高度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">水溶性プレドニン</span></p>
              <p class="具体的処方_処方例1W下げ">120～200 mg/日，10日で漸減，点滴静注</p>
              <p class="具体的処方_処方例">＋<span class="具体的処方_色文字">バルトレックス</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×2回/日，5日間</p>
              <p class="具体的処方_処方例">　または<span class="具体的処方_色文字">ファムビル</span></p>
              <p class="具体的処方_処方例1W下げ">250 mg×3回/日，5日間</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">高度麻痺例（8点以下）で，発症後7～10日前後でENoG値が10％以下（NETが無反応）のものはなるべく早期（2週間以内が望ましい）に，患者の希望を確認したうえで顔面神経減荷術を施行するのが望ましい。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">ｄ）ZSHの疑い例</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">バルトレックス</span></p>
              <p class="具体的処方_処方例1W下げ">1000 mg×3回/日，5日間に変更する</p>
              <p class="具体的処方_処方例">　または<span class="具体的処方_色文字">ファムビル</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×3回/日，5日間</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）麻痺発症8～14日に治療開始の場合</p>
              <p class="具体的処方_病型分類">ａ）20点以上（軽度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">　</p>
              <p class="具体的処方_処方例">①基本薬剤のみ</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ｂ）18～10点（中等度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プレドニゾロン</span></p>
              <p class="具体的処方_処方例1W下げ">60 mg/日，10日で漸減</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ｃ）8点以下（高度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">水溶性プレドニン</span></p>
              <p class="具体的処方_処方例1W下げ">120～200 mg/日，10日で漸減，点滴静注</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">Hunt</span>
                <span class="具体的処方_色文字">症候群</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）麻痺発症7日以内に治療開始の場合</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">ⅰ）ⅱ）ともに基本薬剤として<span class="具体的処方_色文字">ビタミン</span><span class="具体的処方_色文字">B</span><span class="下付き">12</span>，<span class="具体的処方_色文字">ATP</span><span class="具体的処方_色文字">製剤</span>，<span class="具体的処方_色文字">循環改善薬</span>を用いる</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ａ）20点以上（軽度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プレドニゾロン</span></p>
              <p class="具体的処方_処方例1W下げ">30 mg/日，10日で漸減</p>
              <p class="具体的処方_処方例">＋<span class="具体的処方_色文字">バルトレックス</span></p>
              <p class="具体的処方_処方例1W下げ">1000 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ファムビル</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×3回/日，7日間</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ｂ）18～10点（中等度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プレドニゾロン</span></p>
              <p class="具体的処方_処方例1W下げ">60 mg/日，10日で漸減</p>
              <p class="具体的処方_処方例">＋<span class="具体的処方_色文字">バルトレックス</span></p>
              <p class="具体的処方_処方例1W下げ">1000 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ファムビル</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">アメナリーフ</span></p>
              <p class="具体的処方_処方例1W下げ">400 mg×1回/日，7日間</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ｃ）8点以下（高度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">水溶性プレドニン</span></p>
              <p class="具体的処方_処方例1W下げ">120～200 mg/日，10日で漸減，点滴静注</p>
              <p class="具体的処方_処方例">＋<span class="具体的処方_色文字">バルトレックス</span></p>
              <p class="具体的処方_処方例1W下げ">1000 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ファムビル</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">アメナリーフ</span></p>
              <p class="具体的処方_処方例1W下げ">400 mg×1回/日，7日間</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">高度麻痺例（8点以下）で，発症後7～10日前後でENoG値が10％以下（NETが無反応）のものはなるべく早期（2週間以内が望ましい）に，患者の希望を確認したうえで顔面神経減荷術を施行するのが望ましい。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）麻痺発症8～14日に治療開始の場合</p>
              <p class="具体的処方_病型分類">ａ）20点以上（軽度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">　</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">バルトレックス</span></p>
              <p class="具体的処方_処方例1W下げ">1000 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ファムビル</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">アメナリーフ</span></p>
              <p class="具体的処方_処方例1W下げ">400 mg×1回/日，7日間</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ｂ）18～10点（中等度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プレドニゾロン</span></p>
              <p class="具体的処方_処方例1W下げ">60 mg/日，10日で漸減</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">バルトレックス</span></p>
              <p class="具体的処方_処方例1W下げ">1000 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ファムビル</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">アメナリーフ</span></p>
              <p class="具体的処方_処方例1W下げ">400 mg×1回/日，7日間</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ｃ）8点以下（高度麻痺）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">水溶性プレドニン</span></p>
              <p class="具体的処方_処方例1W下げ">120～200 mg/日，10日で漸減，点滴静注</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">バルトレックス</span></p>
              <p class="具体的処方_処方例1W下げ">1000 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ファムビル</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×3回/日，7日間</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">アメナリーフ</span></p>
              <p class="具体的処方_処方例1W下げ">400 mg×1回/日，7日間</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">高度麻痺例（8点以下）で，発症後7～10日前後でENoG値が10％以下（NETが無反応）のものはなるべく早期（2週間以内が望ましい）に，患者の希望を確認したうえで顔面神経減荷術を施行するのが望ましい。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表">
      <p/>
      <p/>
      <p class="具体的処方_処方例">後遺症を予防するためのリハビリテーション</p>
      <p class="具体的処方_処方例">・麻痺出現時からの用手的ストレッチ，マッサージ</p>
      <p class="具体的処方_処方例">・ENoG値が40％未満の場合は，遅くとも発症3ヵ月までにミラーバイオフィードバックを1年間は施行する。</p>
    </div>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>秋田大学大学院耳鼻咽喉科・頭頸部外科学／秋田赤十字病院耳鼻咽喉科・めまいセンター<span class="上付き">＊</span>　<span class="著者名">佐藤　輝幸・石川　和</span><span class="著者名">夫</span><span class="上付き">＊</span><span class="著者名">】</span></p>
      <p class="著者"/>
      <p class="著者"/>
    </div>
    <!--著者 add end-->
  </body>
</doc>
