<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>04</chapter>
    <chapter-title>消化管疾患</chapter-title>
    <section>02</section>
    <article-code>04-02</article-code>
    <article-title>胃食道逆流症</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>2 胃食道逆流症</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">胃食道逆流症（GERD）診療ガイドライン2015（改訂第2版）（2015）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">PPI抵抗性GERDに対する治療戦略が提示された。</li>
        <li class="whatsnew_リスト">長期治療戦略として，重症びらん性GERD，軽症びらん性GERD，非びらん性GERDに分けて提案された。</li>
        <li class="whatsnew_リスト">Barrett食道が独立した項目として記載された。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">胃食道逆流症（GERD）の診断と治療フローチャート</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/04-02-A01.svg"/>
          <p class="図表_注">PPI：プロトンポンプ阻害薬，H<span class="下付き">2</span>RA：ヒスタミンH<span class="下付き">2</span> 受容体拮抗薬</p>
          <p class="図表_引用">（「日本消化器病学会 編：胃食道逆流症（GERD）診療ガイドライン2015（改訂第2版），<span class="本文_イタリック">xvii</span>，2015，南江堂」より許諾を得て転載） </p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶胃内容物の食道への逆流によりさまざまな症状や症候を呈する疾患。</p>
      <p class="本文_先頭丸数字">❷内視鏡的にびらんを認める「<span class="本文_色文字太字">びらん性</span><span class="本文_色文字太字">GERD</span>」と，びらんを認めず胸やけなど逆流症状を呈する「<span class="本文_色文字太字">非びらん性</span><span class="本文_色文字太字">GERD</span>」に分類される。</p>
      <div class="見出し_小見出し">診断</div>
      <p>　診断は，症状診断，内視鏡診断，機能診断，治療的診断に分けられる。</p>
      <p class="本文_先頭丸数字">❶胸やけ症状は患者に正しく理解されているとは限らないため，詳細な問診が重要である。ReQuestやFSSG（Fスケール）などの<span class="本文_色文字太字">自己記入式問診票</span>が有用である。また，食道粘膜傷害の内視鏡的重症度は自覚症状と必ずしも相関しないことに注意が必要である。</p>
      <p class="本文_先頭丸数字">❷胸痛，咳嗽，喘息，咽頭痛，睡眠障害など<span class="本文_色文字太字">食道外症状</span>のみを呈する患者がいることに留意する。</p>
      <p class="本文_先頭丸数字">❸びらん性GERDの内視鏡的重症度分類に用いられる<span class="本文_色文字太字">ロサンゼルス分類</span>の客観性は高く，有用である。しかし，本邦で用いられているminimal change（Grade M）は客観的診断，臨床的意義に関しては検討が十分でない。</p>
      <p class="本文_先頭丸数字">❹機能的逆流の評価には24時間食道pHモニタリング，24時間食道インピーダンス・pHモニタリングが有用。</p>
      <p class="本文_先頭丸数字">❺PPI投与による症状の改善を判断する<span class="本文_色文字太字">PPI</span><span class="本文_色文字太字">テスト</span>は診断に有用である。</p>
      <div class="見出し_小見出し">治療方針</div>
      <div class="見出し_孫見出し"><img class="" src="./image/04-02-小見出し_矢印.png"/>治療目標</div>
      <p class="本文_先頭丸数字">❶GERD患者の長期管理の主要目的は，症状のコントロールとQOLの改善に加え，合併症の予防である。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/04-02-小見出し_矢印.png"/>治療方法</div>
      <p class="本文_先頭丸数字">❶生活習慣のなかには酸の胃食道逆流を引き起こすものがあり，その変更や中止はPPI療法下に行えば有効であり，生活指導を行うよう提案する。ただし，生活習慣の改善単独では症状改善につながるというエビデンスは少ない。</p>
      <p class="本文_先頭丸数字">❷びらん性GERDの治癒速度および症状消失の速さは，薬剤の酸分泌抑制力に依存するため，その治療には強力な酸分泌抑制薬を使用することを推奨する。また非びらん性GERDの治療にも酸分泌抑制が有効であり，酸分泌抑制薬を使用することを推奨する。</p>
      <p class="本文_先頭丸数字">❸GERDの初期治療においてPPIは他剤と比較して優れた症状改善と食道粘膜傷害の治癒をもたらし，費用対効果にも優れており，GERDの第一選択薬として使用することを推奨する。</p>
      <p class="本文_先頭丸数字">❹消化管運動機能改善薬，漢方薬などは単独療法の有用性を指示するエビデンスはないが，PPIとの併用により症状改善効果を得られることがあり，使用することを提案する。</p>
      <p class="本文_先頭丸数字">❺アルギン酸ナトリウム，制酸薬はGERDの一時的症状改善に効果があり，使用することを提案する。</p>
      <p class="本文_先頭丸数字">❻常用量のPPIの1日1回投与にもかかわらず食道粘膜傷害が治癒しない，もしくは強い症状を訴える場合は，PPI抵抗性GERDとしてPPIの倍量・1日2回投与を推奨する。もしくはPPIの種類の変更，モサプリドの追加投与，六君子湯の追加投与，就寝時のヒスタミンH<span class="下付き">2</span> 受容体拮抗薬（H<span class="下付き">2</span>RA）追加投与を行うことを提案する。</p>
      <p class="本文_先頭丸数字">❼PPI抵抗性GERDや長期的なPPIの維持投与を要するびらん性GERDに対しては外科的治療が検討される。開腹手術に比べ腹腔鏡手術は有用である。</p>
      <p class="本文_先頭丸数字">❽PPIによる維持療法は最も効果が高く，費用対効果に優れており，GERDの維持療法にはPPIの投与を推奨する。</p>
      <p class="本文_先頭丸数字">❾重症のびらん性GERDでは積極的に維持療法を行うことを提案する。粘膜傷害が軽度のびらん性GERDの一部には，オンデマンド療法で症状コントロール可能な場合がある。</p>
      <p class="本文_先頭丸数字">❿GERDの長期管理について，患者の視点から，効果（症状の寛解），安全性，費用，剤形（服用しやすさ），服用回数なども考慮することを提案する。</p>
      <p class="本文_先頭丸数字">⓫PPIによる維持療法の安全性は高いが，長期投与に際しては注意深い観察が必要である。適切な適応症例においては，投与期間について明確な制限は存在しないが，必要に応じた最小限の用量で使用することを提案する。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/04-02-小見出し_矢印.png"/>合併症：Barrett食道</div>
      <p class="本文_先頭丸数字">❶日本では，Barrett粘膜（胃から連続性に食道に伸びる円柱上皮で，腸上皮化生の有無を問わない）の存在する食道と定義されている。</p>
      <p class="本文_先頭丸数字">❷Barrett食道の発生は酸または胆汁の胃内容物排泄速度（GER）が関係する。長さ3 cm以上のBarrett食道（LSBE）の頻度は1％以下とする報告が多く，3 cm未満のBarrett食道（SSBE）に比べて非常に少ない。</p>
      <p class="本文_先頭丸数字">❸米国ではLSBEの発癌頻度は年間0.4％といわれているが，日本人のBarrett食道からの発癌頻度に関するデータは少なく現時点では不明である。</p>
      <p class="本文_先頭丸数字">❹本邦ではBarrett食道から発生した腺癌の報告はあるが，その頻度はきわめて低く，現時点でBarrett食道全例に内視鏡による経過観察が必要かは不明である。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合，専門医への紹介が必要である。</p>
      <p class="本文_専門医へのコンサルト">①大きなヘルニアを合併する症例</p>
      <p class="本文_専門医へのコンサルト">②短食道を合併する症例</p>
      <p class="本文_専門医へのコンサルト">③誤嚥性肺炎を繰り返す症例</p>
      <p class="本文_専門医へのコンサルト">④PPI投与でもGrade C以上のびらん性GERD</p>
      <p class="本文_専門医へのコンサルト">⑤さまざま治療に対し抵抗を示す症例</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">　2015年新たなPPIとしてカリウムイオン競合型アシッドブロッカー（P-CAB）が発売された。ボノプラザンは，壁細胞分泌細管に集積し，酸に安定であることから，高い胃酸分泌抑制作用を有し，その効果発現も速いとされている。承認時の成績によれば，びらん性GERDの治癒率はランソプラゾールとの非劣性が確認され，特にGrade CやGrade Dのような重症びらん性GERDでは投与2週後の治癒率がランソプラゾール63.9％に対して88.0％と非常に高いが，ガイドラインにはその位置づけは記載されていない。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】PPIは初期治療・維持療法ともに第一選択薬である。PPI治療に反応しない症例では，他のPPIへのスイッチやPPI増量あるいはP-CABを選択する。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶びらん性</span>
                <span class="具体的処方_色文字">GERD</span>
                <span class="具体的処方_色文字">初期治療</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">パリエット</span></p>
              <p class="具体的処方_処方例1W下げ">10～20 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①～③PPIは食前30～60分に内服したほうが胃酸分泌抑制効果を十分に発揮できる。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ネキシウム</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">タケプロン</span></p>
              <p class="具体的処方_処方例1W下げ">30 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">タケキャブ</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×1回/日，朝食後</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④P-CABは酸分泌抑制効果が強く，食事の影響を受けにくいとされている。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶びらん性</span>
                <span class="具体的処方_色文字">GERD</span>
                <span class="具体的処方_色文字">維持療法</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ネキシウム</span></p>
              <p class="具体的処方_処方例1W下げ">10～20 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">パリエット</span></p>
              <p class="具体的処方_処方例1W下げ">10 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">タケプロン</span></p>
              <p class="具体的処方_処方例1W下げ">15 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">タケキャブ</span></p>
              <p class="具体的処方_処方例1W下げ">10～20 mg×1回/日，朝食後</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶非びらん性</span>
                <span class="具体的処方_色文字">GERD</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">タケプロン</span></p>
              <p class="具体的処方_処方例1W下げ">15 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ネキシウム</span></p>
              <p class="具体的処方_処方例1W下げ">10 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">パリエット</span></p>
              <p class="具体的処方_処方例1W下げ">10 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">PPI</span>
                <span class="具体的処方_色文字">抵抗性</span>
                <span class="具体的処方_色文字">GERD</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">パリエット</span></p>
              <p class="具体的処方_処方例1W下げ">10～20 mg×2回/日，朝夕食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">タケキャブ</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×1回/日，朝食後</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">ガスモチン</span></p>
              <p class="具体的処方_処方例1W下げ">【適応外処方】</p>
              <p class="具体的処方_処方例1W下げ">15 mg×3回/日，毎食後</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③～⑤は，単独では十分な効果は得られないので，PPIに併用して投与する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">六君子湯</span></p>
              <p class="具体的処方_処方例1W下げ">【適応外処方】</p>
              <p class="具体的処方_処方例1W下げ">2.5 g×3回/日，毎食前</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">⑤<span class="具体的処方_色文字">アルロイド</span><span class="具体的処方_色文字">G</span></p>
              <p class="具体的処方_処方例1W下げ">10 mL×3回/日，毎食間</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>大阪市立大学大学院消化器内科学<span class="著者名">　藤原　靖弘</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
