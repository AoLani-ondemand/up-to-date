<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>06</chapter>
    <chapter-title>内分泌・代謝系疾患</chapter-title>
    <section>06</section>
    <article-code>06-06</article-code>
    <article-title>副腎疾患</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>6 副腎疾患</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">
            <span>2017</span>
            <span>年時点で厳密な意味での診療ガイドラインは存在しないが，「副腎腫瘍取扱い規約」（</span>
            <span>2015</span>
            <span>年</span>
            <span>3</span>
            <span>月 第</span>
            <span>3</span>
            <span>版）のなかで取り上げられている。</span>
          </li>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  副腎性サブクリニカルクッシング症候群 新診断基準（2017）</li>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  わが国の原発性アルドステロン症の診療に関するコンセンサス・ステートメント（2016）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">全国予後調査の結果，<span class="本文_色文字太字">非機能性副腎腫瘍</span>から機能性副腎腫瘍や<span class="本文_色文字太字">副腎癌</span>への診断の変更があるため，慎重な経過観察が必要。</li>
        <li class="whatsnew_リスト">腫瘍径の大きいものでは手術を考慮する。</li>
        <li class="whatsnew_リスト"><span class="本文_色文字太字">サブクリニカルクッシング症候群（</span><span class="本文_色文字太字">SCS</span><span class="本文_色文字太字">）</span>では肥満，耐糖能異常などの生活習慣病の二次的な背景要因となっている場合がある。</li>
        <li class="whatsnew_リスト">高血圧を伴う症例では，<span class="本文_色文字太字">原発性アルドステロン症</span>の可能性を除外する。</li>
        <li class="whatsnew_リスト"><span class="本文_色文字太字">褐色細胞腫</span>の35％は無症候性であり，副腎偶発腫瘍においても10％弱，存在する。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">副腎偶発腫瘍診断アルゴリズム</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/06-06-A01.svg"/>
          <p class="図表_注">1）ARR：アルドステロン/レニン活性比，単位：アルドステロン（pg/mL），血漿レニン活性（ng/mL/時）</p>
          <p class="図表_注">2）2017年時点のコルチゾールカットオフ値は，本邦診断基準で1.8 μg/dL，米国クリニカルガイドラインで1.8 μg/dLとなっている。</p>
          <p class="図表_注">3）褐色細胞腫は腫瘍径によらず，臨床的悪性（転移巣の存在）についての局在診断を行う。</p>
          <p class="図表_注">4）臨床症状などを勘案し，性ホルモン産生腫瘍その他を除外診断する。</p>
          <p class="図表_注">5）その他，副腎皮質癌を疑う画像所見として，腫瘍内石灰化（神経節腫でも認めることがある）・CT値高値・MRI T2強調像にて高信号・低脂肪含有（非機能性褐色細胞腫，骨髄脂肪腫，嚢胞，神経節腫でも認めることがある）などがあげられる。副腎腫瘍発見のきっかけとなったCTから得られる脂肪成分に関する質的情報やそれ以前の画像所見があれば診断に利用する。</p>
          <p class="図表_注">MN：metanephrine，NMN：normetanephrine，DST：dexamethasone suppression test，AVS：adrenal venous sampling</p>
          <p class="図表_引用">（日本泌尿器科学会・日本病理学会・日本医学放射線学会・日本内分泌学会・日本内分泌外科学会（編）．副腎腫瘍取扱い規約 2015年3月 第3版．金原出版，2015©日本泌尿器科学会より許諾を得て転載し，2）についてはSCSの新診断基準に基づき修正）</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶健康診断や種々の疾患の精査過程で，画像上，たまたま発見される副腎腫瘍を副腎偶発腫瘍と総称。</p>
      <p class="本文_先頭丸数字">❷腹部CT施行例の0.3～0.8％に認められる。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶わが国の副腎偶発腫瘍の全国調査（3678例）の集計では，多くは無症状で発見されるため，非機能性副腎腺腫が50.8％と最多，コルチゾール産生腺腫10.5％，褐色細胞腫8.5％，アルドステロン産生腺腫5.1％，副腎癌は1.4％であった。その他，転移性悪性腫瘍，骨髄脂肪腫，神経節神経腫，囊胞，悪性リンパ腫など。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">SCS</span>：クッシング兆候を認めないもののコルチゾールの自律性分泌を呈する症例が存在し，SCSと呼ばれる。高血圧，糖尿病，メタボリックシンドローム（MetS）などの合併率が高い。 1 mgデキサメタゾン抑制試験（DST）で血中コルチゾール値は5 μg/dL以上では無条件にSCSと診断し，1.8～3 μg/dL，もしくは3 μg/dL以上では種々の条件を満たした場合にSCSとする新診断基準が2017年に提唱されている（<span class="本文_図表番号">表</span><span class="本文_図表番号">1</span>）。</p>
      <div class="図表" type="T" order="1">
        <p class="図表_タイトル" type="T" order="1" data-file="06-06-T01.svg">表1　副腎性SCS新診断基準</p>
        <img class="width-800" src="./image/06-06-T01.svg"/>
        <p class="図表_注">
          <span>注</span>
          <span>1</span>
          <span>：身体徴候としての高血圧，全身性肥満や病態としての耐糖能異常，骨密度低下，脂質異常症は</span>
          <span>Cushing</span>
          <span>症候群に特徴的所見とはみなさない。</span>
        </p>
        <p class="図表_注">
          <span>注</span>
          <span>2</span>
          <span>：安静，絶食の条件下で早朝に</span>
          <span>2</span>
          <span>回以上の測定が望ましく，常に高値の例は本症とみなさない。正常値については，各測定キットの設定に従う。</span>
        </p>
        <p class="図表_注">
          <span>注</span>
          <span>3</span>
          <span>：</span>
          <span>overnight</span>
          <span> </span>
          <span>1</span>
          <span> </span>
          <span>mgDST</span>
          <span>を施行する。スクリーニング検査を含め，</span>
          <span>1</span>
          <span> </span>
          <span>mgDST</span>
          <span>後の血中コルチゾール値</span>
          <span>1</span>
          <span>.</span>
          <span>8</span>
          <span> </span>
          <span>μ</span>
          <span>g</span>
          <span>/</span>
          <span>d</span>
          <span>L</span>
          <span>以上の場合，非健常と考えられ，何らかの臨床的意義を有する機能性副腎腫瘍あるいは非機能性副腎腫瘍の可能性を考慮する。</span>
        </p>
        <p class="図表_注">
          <span>注</span>
          <span>4</span>
          <span>：確定診断のための高用量（</span>
          <span>4</span>
          <span>～</span>
          <span>8</span>
          <span> </span>
          <span>mg</span>
          <span>）</span>
          <span>DST</span>
          <span>は必ずしも必要としないが，病型診断のために必要な場合には行う。</span>
        </p>
        <p class="図表_注">
          <span>注</span>
          <span>5</span>
          <span>：</span>
          <span>低濃度域の血中コルチゾール値は</span>
          <span>10</span>
          <span>％前後の測定のばらつき（</span>
          <span>3</span>
          <span> </span>
          <span>μ</span>
          <span>g</span>
          <span>/</span>
          <span>d</span>
          <span>L</span>
          <span>前後の血中コルチゾール値は，</span>
          <span>0</span>
          <span>.</span>
          <span>3</span>
          <span> </span>
          <span>μ</span>
          <span>g</span>
          <span>/</span>
          <span>d</span>
          <span>L</span>
          <span>程度のばらつき）が生じうることを考慮し，陽性所見の項目数も勘案して，総合的に診断を行う。</span>
        </p>
        <p class="図表_注">
          <span>注</span>
          <span>6</span>
          <span>：早朝の血中</span>
          <span>ACTH</span>
          <span>基礎値が</span>
          <span>10</span>
          <span> </span>
          <span>p</span>
          <span>g</span>
          <span>/</span>
          <span>m</span>
          <span>L</span>
          <span>未満（</span>
          <span>2</span>
          <span>回以上の測定が望ましい）あるいは</span>
          <span>ACTH</span>
          <span>分泌刺激試験の低反応（基礎値の</span>
          <span>1</span>
          <span>.</span>
          <span>5</span>
          <span>倍未満）。なお，</span>
          <span>ACTH</span>
          <span>分泌不全症でも生物活性の低い大分子型</span>
          <span>ACTH</span>
          <span>が分泌されている場合には，測定キットによって必ずしも血中</span>
          <span>ACTH</span>
          <span>が低値とならない場合があり，注意を要する。</span>
        </p>
        <p class="図表_注">
          <span>注</span>
          <span>7</span>
          <span>：</span>
          <span>21</span>
          <span>～</span>
          <span>24</span>
          <span>時の血中コルチゾール値</span>
          <span>5</span>
          <span> </span>
          <span>μ</span>
          <span>g</span>
          <span>/</span>
          <span>d</span>
          <span>L</span>
          <span>以上</span>
        </p>
        <p class="図表_注">
          <span>注</span>
          <span>8</span>
          <span>：健側の集積抑制がコルチゾール産生能と相関するため，定量的評価が望ましい。</span>
        </p>
        <p class="図表_注">
          <span>注</span>
          <span>9</span>
          <span>：年齢および性別を考慮した基準値以下の場合，低値と判断する。</span>
        </p>
        <p class="図表_注">
          <span>注</span>
          <span>10</span>
          <span>：手術施行に際しては，非機能性腫瘍である可能性を含めて十分な説明と同意を必要とする。</span>
        </p>
        <p class="図表_注">［副腎性SCS新取り扱いめやす案］</p>
        <p class="図表_注">本症と診断され，診断①の場合（1 mgDST後の血中コルチゾール値が5 μg/dL以上），治療抵抗性の合併症（高血圧，全身性肥満，耐糖能異常，骨密度低下，脂質異常症など）を有する例は副腎腫瘍の摘出を考慮する。その他の場合も，陽性項目数や合併症の有無を参考に手術もしくは慎重なる経過観察を行う。</p>
        <p class="図表_注">
          <span>付帯事項</span>
        </p>
        <p class="図表_注">
          <span>1</span>
          <span>）腫瘍径が</span>
          <span>3</span>
          <span> </span>
          <span>cm</span>
          <span>以上の場合や</span>
          <span>3</span>
          <span> </span>
          <span>cm</span>
          <span>未満でも増大傾向のあるものは，画像所見も参考に副腎癌の可能性が否定できない場合には副腎摘出術を行う。</span>
        </p>
        <p class="図表_注">
          <span>2</span>
          <span>）</span>
          <span>SCS</span>
          <span>の副腎腫瘍摘出後，糖質コルチコイド補充を必要とする例があるので注意を要する。</span>
        </p>
        <p class="図表_引用">（柳瀬敏彦，他．日内分泌会誌．2017；93 Suppl. September. 1-18.）</p>
      </div>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">原発性アルドステロン症（</span><span class="本文_太字">PA</span><span class="本文_太字">）</span></p>
      <p class="本文_級下げ">①定型的には高血圧と低K血症を呈するが，多くは正カリウム性。全高血圧の5～10％程度に存在。スクリーニングは血漿アルドステロン（PAC）（pg/mL）と血漿レニン活性（PRA）（ng/mL/時）の比，すなわちARR（PAC/PRA比）で行われる。副腎偶発腫瘍で高血圧を伴う場合は精査が望まれる。日本内分泌学会より診療指針が提示されている（<span class="本文_図表番号">図</span><span class="本文_図表番号">1</span>）。</p>
      <p class="本文_級下げ">②常塩食摂取下で測定したARR≧200かつPAC≧120 pg/mLであれば二次精査へ進む。なお，SCS合併PAでは，ACTHの抑制のために，PACがそれほど高値でない症例も存在する。</p>
      <p class="本文_級下げ">③<span class="下線">二次精査</span>：以下の検査のうち，2種類以上陽性であればPAと診断される。1）フロセミド立位負荷試験：40 mgフロセミド静注後2時間立位でPRA＜2.0 ng/mL/時を陽性とする。2）カプトプリル負荷試験：カプトプリル50 mgの服用後，60（90）分後のARR≧200（またはPAC＞120 pg/mL）を陽性とし，外来でも施行可能。3）生理食塩水（生食）負荷試験：生食2 Lを4時間かけて点滴静注し，4時間後の安静臥位でのPAC＞60 pg/mLを陽性とする。</p>
      <p class="本文_級下げ">④PAの腺腫は2 cm以下の小さな腫瘍が多く，CTの検出限界である5～6 mm以下の小さな腫瘍では検出されない。CTで片側副腎腫瘍が存在しても，反対側に微小腺腫が存在する可能性があり，最終的局在診断は，選択的副腎静脈サンプリング（AVS）による。AVSの施行手順やPAの局在診断基準は，施設によって異なるが，アルドステロンの過剰分泌の判定は，ACTH負荷後のPACが14000 pg/mL以上で，左右差判定はPAC/コルチゾール比の左右差（LR）が2.6～3以上の場合，左右差ありと判定されてきた。なお，「わが国の原発性アルドステロン症の診療に関するコンセンサスステートメント」では，LR＞4かつCR（反対側副腎PAC/コルチゾールと下大静脈PAC/コルチゾールの比率）＜1をカットオフとして手術適応とすることが推奨され，LR 2～4の場合もその他の条件を勘案して手術適応を決定するとしている。</p>
      <p class="本文_級下げ">⑤<span class="上付き">131</span>I-アドステロールシンチグラフィーによるPAの局在診断の精度は低く，その診断的意義は薄れつつあるが，コルチゾール産生腺腫の合併の診断には有用性がある。</p>
      <div class="図表" type="F" order="1">
        <img class="width-800" src="./image/06-06-F01.svg"/>
        <p class="図表_タイトル" type="F" order="1" data-file="06-06-F01.svg">図1　原発性アルドステロン症診断の手引き（専門医療機関向け：スクリーニングと確定診断）</p>
        <p class="図表_引用">（日本内分泌学会刊行委員会，他．日内分泌会誌．2010；<span class="太字">86</span> Suppl.）</p>
      </div>
      <p class="本文_先頭丸数字">❹<span class="本文_太字">褐色細胞腫</span>：高血圧病型は持続型，発作型，正常型に大別されるが，血圧正常の無症候型の場合は往々にして副腎偶発腫瘍として発見される。褐色細胞腫の35％は無症候性とされる。画像上，褐色細胞腫では，出血壊死のために囊胞化を来しやすい。褐色細胞腫の診断基準は<span class="本文_図表番号">表</span><span class="本文_図表番号">2</span>に示す。</p>
      <div class="図表" type="T" order="2">
        <p class="図表_タイトル" type="T" order="2" data-file="06-06-T02.svg">表2　褐色細胞腫・パラガングリオーマの診断基準（案）</p>
        <img class="width-400" src="./image/06-06-T02.svg"/>
        <p class="図表_注">注1：現在，過去の時期を問わない。副腎髄質由来を褐色細胞腫，傍神経節組織由来をパラガングリオーマと称する。</p>
        <p class="図表_注">注2：腫瘍細胞の大部分がクロモグラニンA陽性であること。</p>
        <p class="図表_注">注3：基準値上限の3倍以上を陽性とする。偽陽性や偽陰性があるため，反復測定が推奨される。</p>
        <p class="図表_注">注4：ノルアドレナリン高値例のみ。負荷後に前値の1/2以上あるいは500 pg/mL以上の場合を陽性とする。</p>
        <p class="図表_注">注5：<span class="上付き">123</span>I-MIBGあるいは<span class="上付き">131</span>I-MIBGシンチグラフィ。</p>
        <p class="図表_引用">（厚生労働省難治性疾患克服研究事業「褐色細胞腫の実態調査と診断指針作成」研究班．褐色細胞腫診療指針．2012．）</p>
      </div>
      <p class="本文_先頭丸数字">❺<span class="本文_太字">両側副腎病変</span>：両側過形成症例では先天性副腎過形成，クッシング病，特発性アルドステロン症など，両側副腎病変の場合には，ACTH非依存性大結節性過形成，両側副腎腺腫，両側褐色細胞腫，悪性リンパ腫，癌転移などの可能性を鑑別する。</p>
      <div class="見出し_小見出し">治療指針</div>
      <p class="本文_先頭丸数字">❶腫瘍径が大きいほど，副腎癌の確率が高くなるため，径が3.5～4 cm以上の腫瘍では，無症候でも手術を積極的に考慮する。</p>
      <p class="本文_先頭丸数字">❷副腎癌が強く疑われる場合には，術中播種を避けるために腹腔鏡下内視鏡的手術ではなく開腹手術が勧められる。</p>
      <p class="本文_先頭丸数字">❸副腎癌の術後アジュバント療法として，recurrence free survivalがミトタンの投薬で改善することが明らかになっている。</p>
      <p class="本文_先頭丸数字">❹非機能性副腎腺腫で，腫瘍径が3 cm未満で半年～1年ごとのフォローアップでも増大傾向がない場合は経過観察も可能である。腫瘍径の増大率と悪性の関係について明確な見解はないが，0.8～1 cm以上の増大で悪性を示唆する報告を認める。</p>
      <p class="本文_先頭丸数字">❺厚生労働省の疫学調査によれば，SCS合併症としての肥満，糖代謝異常，高血圧，脂質異常症は手術によって不変もしくは改善のどちらかであり，悪化例はほとんど認めない。SCSの手術適応基準は現時点で未確立であるが，合併症管理が薬剤で困難な場合や1 mg DSTの血中コルチゾール値5 μg/dL以上では，合併症管理の観点から，積極的に手術を考慮してもよいと思われる。</p>
      <p class="本文_先頭丸数字">❻PA治療でアルドステロン過剰分泌の原因が片側性と診断された場合は，腹腔鏡下内視鏡的副腎摘出術を行う。片側性でも手術不能症例や患者が手術を望まない場合，あるいは両側副腎が原因の場合，薬物療法を行う。</p>
      <p class="本文_先頭丸数字">❼褐色細胞腫は開腹または腹腔鏡による腫瘍摘出手術が治療方針の原則。悪性が疑われる場合には開腹で行う。術前に循環血液量が低下している症例では，補液によりその是正を図る。また多発内分泌腫瘍症（MEN）Ⅱ型では，甲状腺髄様癌の手術の前に，褐色細胞腫の手術を行う。薬物療法は，血圧管理や発作予防など術前管理を目的として行われると同時に，悪性症例や全身状態不良のため手術困難な症例が対象となる。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には積極的に専門医へ紹介する。</p>
      <p class="本文_専門医へのコンサルト">①副腎偶発腫瘍の腫瘍径が大きい（＞3 cm）場合</p>
      <p class="本文_専門医へのコンサルト">②糖尿病や高血圧を合併し，機能性腫瘍の可能性が否定できない場合</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">　非機能性副腎腫瘍は，従来は内分泌学的に有意のホルモン産生を認めない腫瘍として悪性が疑われない限り経過観察の対象とされてきた。しかしながら最近，非機能性副腎腫瘍であっても摘出手術により血圧，耐糖能，心血管病リスク因子などの改善を認めるとの報告が散見される。明確な結論は得られておらず今後の検討を要するが，軽微なホルモン産生異常が生活習慣病の背景因子となっている可能性は考えられる。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">原発性アルドステロン症</span>
              </p>
            </td>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅰ）高血圧症例</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">アダラート</span><span class="具体的処方_色文字">CR</span></p>
              <p class="具体的処方_処方例1W下げ">10～40 mg×1回/日，朝食後</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①ジヒドロピリジン系Ca拮抗薬には，ミネラルコルチコイド受容体（MR）拮抗作用が報告されており，ニフェジピンはその作用が相対的に強い。低K血症のある症例では，カリウム製剤の補充が必要な場合がある。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">セララ</span></p>
              <p class="具体的処方_処方例1W下げ">50～100 mg×1回/日，朝食後</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②選択的MR拮抗薬。低K血症の治療にもなる。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">アルダクトン</span><span class="具体的処方_色文字">A</span></p>
              <p class="具体的処方_処方例1W下げ">50～100 mg×1回/日，朝食後</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③MR拮抗薬。低K血症の治療にもなる。男性では女性化乳房の副作用に注意が必要。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶褐色細胞腫</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）術前処置，手術不能時</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ミニプレス</span></p>
              <p class="具体的処方_処方例1W下げ">1～2 mg×3回/日</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">デタントール</span></p>
              <p class="具体的処方_処方例1W下げ">1～4 mg×3回/日</p>
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">カルデナリン</span></p>
              <p class="具体的処方_処方例1W下げ">0.5 mg×1回/日より開始，最高8 mg/日まで</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①②③α<span class="下付き">1</span> 遮断薬。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）不整脈，頻脈時</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">インデラル</span></p>
              <p class="具体的処方_処方例1W下げ">10～40 mg×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①②β遮断薬。α遮断薬と併用し，単独投与はしない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">セロケン</span></p>
              <p class="具体的処方_処方例1W下げ">20～80 mg×3回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">トランデート</span></p>
              <p class="具体的処方_処方例1W下げ">50～150 mg×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③αβ遮断薬。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>福岡大学内分泌・糖尿病内科<span class="著者名">　柳瀬　敏彦</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
