<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>02</chapter>
    <chapter-title>呼吸器疾患</chapter-title>
    <section>04</section>
    <article-code>02-04</article-code>
    <article-title>慢性閉塞性肺疾患（COPD）</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>4 慢性閉塞性肺疾患（COPD）</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">COPD（慢性閉塞性肺疾患）診断と治療のためのガイドライン第4版（2013）<br/><span>（改訂版近日発行予定）</span></li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">現在，本ガイドラインの改訂作業が行われている。</li>
        <li class="whatsnew_リスト">薬物療法のメインである長時間作用性抗コリン薬（LAMA）と長時間作用性 β<span class="下付き">2</span> 刺激薬（LABA）およびその配合薬について使用方法が新たに策定される予定である。</li>
        <li class="whatsnew_リスト">COPDと診断される以前の早期の病態における臨床管理方法の指針が示される予定である。</li>
        <li class="whatsnew_リスト">COPD管理における身体活動性の重要性がますます高まった。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">安定期COPD管理のアルゴリズム</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/02-04-A01.svg"/>
          <p class="図表_注">LAMA：長時間作用性抗コリン薬，LABA：長時間作用性 β<span class="下付き">2</span> 刺激薬，＋：加えて行う治療</p>
          <p class="図表_引用">（日本呼吸器学会COPDガイドライン第4版作成委員会（編）．COPD（慢性閉塞性肺疾患）診断と治療のためのガイドライン第4版．メディカルレビュー社；2013．より許諾を得て転載）</p>
          <p class="図表_注">・薬物療法は気管支拡張薬が基本。単剤で十分な効果がなければ併用。喘息の合併があればICS併用。</p>
          <p class="図表_注">・薬物療法と同時に呼吸リハビリテーションなどの非薬物療法を怠らない。</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶タバコ煙を主とする有害物質を長期に吸入曝露することなどにより生ずる肺疾患であり，<span class="本文_色文字太字">呼吸機能検査で気流閉塞</span>を示す（改訂版ガイドラインでは若干の変更が予定されている）。</p>
      <p class="本文_先頭丸数字">❷気流閉塞は末梢気道病変と気腫性病変がさまざまな割合で複合的に関与し起こる。</p>
      <p class="本文_先頭丸数字">❸臨床的には徐々に進行する労作時の呼吸困難や慢性の咳・痰を示すが，これらの症状に乏しいこともある。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶<span class="本文_色文字太字">気管支拡張薬吸入後のスパイロメトリーで</span><span class="本文_色文字太字">1</span><span class="本文_色文字太字">秒率（</span><span class="本文_色文字太字">FE</span><span class="本文_色文字太字">V<span class="下付き">1</span></span><span class="本文_色文字太字">/</span><span class="本文_色文字太字">F</span><span class="本文_色文字太字">VC</span><span class="本文_色文字太字">）が</span><span class="本文_色文字太字">70</span><span class="本文_色文字太字">％未満</span>であれば，COPDと診断する。</p>
      <p class="本文_先頭丸数字">❷診断確定には，<span class="本文_色文字太字">気流閉塞を来す他の閉塞性疾患を除外</span>する必要がある。</p>
      <p class="本文_先頭丸数字">❸COPDを疑わせる参考所見としては，長期の喫煙歴または受動喫煙歴，慢性の咳および喀痰，労作時の息切れなどがある。</p>
      <p class="本文_先頭丸数字">❹COPDを疑わせる画像所見は，胸部単純X線写真における肺過膨張，肺野の透過性亢進，滴状心，ブラなどがある。また胸部CT所見ではブラなどの気腫性病変，あるいは全肺野に対する低吸収領域（LAA）の割合増加がみられる。</p>
      <p class="本文_先頭丸数字">❺気道可逆性や気道過敏性の存在をもって喘息の合併を疑うことは問題ないが，COPDを否定することはできない。</p>
      <div class="見出し_小見出し">治療方針</div>
      <div class="見出し_孫見出し"><img class="" src="./image/02-04-小見出し_矢印.png"/>COPDの管理目標<span class="本文_通常文字">（改訂版ガイドラインでは下記のように変更が予定されている）</span></div>
      <p class="本文_先頭丸数字">❶<span class="本文_太字">現状の改善</span>：<span class="本文_色文字太字">1</span><span class="本文_色文字太字">）症状および</span><span class="本文_色文字太字">QOL</span><span class="本文_色文字太字">の改善</span>，<span class="本文_色文字太字">2</span><span class="本文_色文字太字">）運動耐容能と身体活動性の向上および維持</span>。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">将来のリスクの低減</span>：<span class="本文_色文字太字">1</span><span class="本文_色文字太字">）増悪の予防</span>，<span class="本文_色文字太字">2</span><span class="本文_色文字太字">）全身併存症および肺合併症の予防・診断・治療</span>。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/02-04-小見出し_矢印.png"/>治療方法</div>
      <p class="本文_先頭丸数字">❶禁煙を徹底させる。受動喫煙を避けさせること。</p>
      <p class="本文_先頭丸数字">❷感染管理。手洗い励行，ワクチン接種（インフルエンザ，肺炎球菌など）。</p>
      <p class="本文_先頭丸数字">❸生活指導（栄養，感染管理，身体活動，服薬，その他）を行う。</p>
      <p class="本文_先頭丸数字">❹薬物治療と非薬物治療を十分に行う。</p>
      <p class="本文_先頭丸数字">❺COPDの薬物治療は吸入薬を基本とするが，吸入手技が適切でないと薬剤の効果が十分に発揮できないため，適切な吸入指導が行われることが必要である。</p>
      <p class="本文_先頭丸数字">❻薬物選択は，患者の合併症・併存症，薬剤および吸入デバイスの作用特性を考えて，<span class="本文_色文字太字">LAMA</span>か<span class="本文_色文字太字">LABA</span>を単剤で投与する。気管支拡張効果が十分でないかあるいは増悪の頻度が多い場合には，LAMA/LABAの併用または配合薬で投与する。喘息合併の際にはICSを併用する。以前に使われた増悪予防の意味のICS併用には慎重を要する。</p>
      <p class="本文_先頭丸数字">❼非薬物療法として<span class="本文_色文字太字">呼吸リハビリテーション</span>を並行して行う。導入期で運動療法を行い，運動の維持を指導するか，あるいは身体活動性を自己管理で維持することを指導する。</p>
      <p class="本文_先頭丸数字">❽合併症および併存症に対して適切に臨床的な管理を行う。また，心血管疾患や肺癌などはCOPD患者で罹患リスクが高く，定期的な監視が重要である。</p>
      <p>❾<img class="" src="./image/02-04-2.png"/>身体活動性は生命予後やQOLの向上に関係が深く，今後もますます重視される方向にある。介入として，単に薬物療法や運動処方を与えるだけでなく，日常的な指導のなかで活動的なライフスタイルへのモチベーションをもたせるように工夫していくことが重要である。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">　以下の場合には専門医への紹介が望ましい。プライマリケアとの役割分担が重要である。</p>
      <p class="本文_専門医へのコンサルト">①COPDの診断と病期判定，合併症・併存症の評価，治療方針策定</p>
      <p class="本文_専門医へのコンサルト">②増悪への対応</p>
      <p class="本文_専門医へのコンサルト">③重症期ないし終末期の治療，プライマリケアとの連携</p>
      <p class="本文_専門医へのコンサルト">④呼吸器科で行っていない場合には，呼吸リハビリテーションはリハビリテーション科など対応可能な医療機関に紹介</p>
      <p class="本文_専門医へのコンサルト">　以下はプライマリケアにおいて役割が望まれている。</p>
      <p class="本文_専門医へのコンサルト">①安定期の治療管理（薬物療法の継続，身体活動性など）</p>
      <p class="本文_専門医へのコンサルト">②禁煙指導および啓発</p>
      <p class="本文_専門医へのコンサルト">③一般住民および他疾患患者のなかのCOPDの早期発見</p>
      <p class="本文_専門医へのコンサルト">④増悪の早期診断，専門医紹介の必要性の判断</p>
      <p class="本文_専門医へのコンサルト">⑤専門医と連携した終末期管理</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">①世界の呼吸器の専門家が集まって作るCOPDの治療に関する報告であるGOLD（Global Initiative for Chronic Obstructive Lung Disease）の2017年版が発表されて，従来のABCD分類に変更があった。縦軸を増悪の頻度のみで評価する。ICSの使用は喘息合併病態が主となり，増悪に対してはLAMA単独あるいはLABAの併用となった。</p>
      <p class="本文_最近の話題">②FEV<span class="下付き">1</span>/FVC＞70％で呼吸機能は保たれているものの，実際には後にCOPDに進行する潜在的なCOPDの存在は，臨床的には無視されていた。しかし，そのような患者のなかでも，症状や増悪頻度が無視できない一群が報告されている。COPDは潜在的に進行するものであるが，該当患者を捕捉できれば，禁煙などによって予防可能となる。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】労作時の息切れに対しては，気管支拡張薬の処方を主とする。気管支拡張薬はLAMAかLABAを基本とする。喘息の合併がある場合にはICSを併用する。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶運動耐容能の低下，労作時の息切れ</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）LAMA</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">スピリーバレスピマット</span></p>
              <p class="具体的処方_処方例1W下げ">2吸入（5 μg）×1回/日，吸入</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">シーブリ</span></p>
              <p class="具体的処方_処方例1W下げ">1カプセル（50 μg）×1回/日，吸入</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①～④持続時間や吸入のタイミング，デバイスの特徴などを勘案して選択する。閉塞隅角緑内障には禁忌。前立腺肥大症で症状が出る場合には泌尿器科に相談。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">エクリラジェヌエア</span></p>
              <p class="具体的処方_処方例1W下げ">1吸入（400 μg）×2回/日，吸入</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">エンクラッセエリプタ</span></p>
              <p class="具体的処方_処方例1W下げ">1吸入（62.5 μg）×1回/日，吸入</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）LABA</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">オンブレス</span></p>
              <p class="具体的処方_処方例1W下げ">1吸入（150 μg）×1回/日，吸入</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">オーキシス</span></p>
              <p class="具体的処方_処方例1W下げ">1吸入（9 μg）×2回/日，吸入</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①～③それぞれの特徴を勘案して選択する。心疾患などに注意して使用。β遮断薬使用患者は必ずしも禁忌ではない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">セレベント</span></p>
              <p class="具体的処方_処方例1W下げ">1吸入（50 μg）×2回/日，吸入</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶単剤（</span>
                <span class="具体的処方_色文字">LABA</span>
                <span class="具体的処方_色文字">または</span>
                <span class="具体的処方_色文字">LAMA</span>
                <span class="具体的処方_色文字">）で効果が不十分な場合</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ウルティブロ</span></p>
              <p class="具体的処方_処方例1W下げ">1吸入×1回/日，吸入</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">スピオルトレスピマット</span></p>
              <p class="具体的処方_処方例1W下げ">2吸入×1回/日，吸入</p>
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">アノーロエリプタ</span></p>
              <p class="具体的処方_処方例1W下げ">1吸入×1回/日，吸入</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">単剤で効果が不十分の場合，単剤を併用するか①～③の配合薬（LAMA/LABA）を使用する。配合薬はデバイスが1つで済む利点あり。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶喘息の合併がある場合</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">シムビコートタービュヘイラー</span></p>
              <p class="具体的処方_処方例1W下げ">1～4吸入×2回/日，吸入</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">アドエア</span><span class="具体的処方_色文字">250</span><span class="具体的処方_色文字">ディスカス</span></p>
              <p class="具体的処方_処方例1W下げ">1吸入×2回/日，吸入</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">喘息の合併がある場合，それまで使用していた気管支拡張薬にICS単剤を加えるか，あるいは①～③のICS/LABA配合薬を使用。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">レルベア</span><span class="具体的処方_色文字">100</span><span class="具体的処方_色文字">エリプタ</span></p>
              <p class="具体的処方_処方例1W下げ">1吸入×1回/日，吸入</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶軽労作での息切れのみ</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">メプチンエアー</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">メプチンスイング</span><span class="具体的処方_色文字">へラー</span></p>
              <p class="具体的処方_処方例1W下げ">2吸入/回，頓用</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">軽労作のみの場合，短時間作用型吸入β<span class="下付き">2</span> 刺激薬（SABA）を頓用で使用。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶吸入が難しい場合</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ホクナリンテープ</span></p>
              <p class="具体的処方_処方例1W下げ">2 mg/回，1枚/日，貼付</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">吸入が困難な場合には，貼付剤も使用可能。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>東北大学大学院産業医学<span class="著者名">　黒澤　　一</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
