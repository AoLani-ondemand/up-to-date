<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>04</chapter>
    <chapter-title>消化管疾患</chapter-title>
    <section>12</section>
    <article-code>04-12</article-code>
    <article-title>大腸癌</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>12 大腸癌</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  大腸癌治療ガイドライン医師用2016年版（2016）</li>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  遺伝性大腸癌診療ガイドライン2016年版（2016）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <div class="whatsnew_小見出し">【大腸癌治療ガイドライン】</div>
      <ul>
        <li class="whatsnew_リスト">切除不能再発大腸癌に対する化学療法の選択に関し，近年の大規模臨床試験を含む重要な試験結果をふまえてアルゴリズムが改訂された。</li>
        <li class="whatsnew_リスト">大腸癌に対する適応が認められている抗癌剤として，経口薬：トリフルリジン・チピラシル塩酸塩（TAS-102），注射薬：ラムシルマブが追加された。</li>
        <li class="whatsnew_リスト">抗EGFR抗体薬は<span class="本文_イタリック">KRA</span><span class="本文_イタリック">S</span>/<span class="本文_イタリック">N</span><span class="本文_イタリック">RAS</span>野生型が適応であると記載が修正された。</li>
      </ul>
      <div class="whatsnew_小見出し">【遺伝性大腸癌診療ガイドライン】</div>
      <ul>
        <li class="whatsnew_リスト">家族性大腸腺腫症について，本邦のデータが加えられたとともに，大腸全摘術時の一時的人工肛門造設と腹腔鏡手術に関するCQが追加された。</li>
        <li class="whatsnew_リスト">リンチ症候群のスクリーニングから遺伝子診断に至る流れについて，大幅な改訂が行われた。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">①リンチ症候群の診断手順</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/04-12-A01.svg"/>
          <p class="図表_注">MSI：マイクロサテライト不安定性，MSI-H：高頻度MSI</p>
          <p class="図表_引用">（大腸癌研究会（編）．遺伝性大腸癌診療ガイドライン2016年版．金原出版；2016．より許諾を得て転載）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="2">②cTis（M）癌またはcT1（SM）癌の治療方針</div>
      <div class="アルゴリズム" order="2">
        <div class="図表" type="A" order="2">
          <img class="width-800" src="./image/04-12-A02.svg"/>
          <p class="図表_注">c：臨床所見，Tis（M）：上皮内腫瘍または粘膜固有層に浸潤，T1（SM）：粘膜下層</p>
          <p class="図表_引用">（大腸癌研究会（編）．大腸癌治療ガイドライン医師用2016年版．金原出版；2016．より許諾を得て転載）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="3">③内視鏡的摘除後のpT1（SM）癌の治療方針</div>
      <div class="アルゴリズム" order="3">
        <div class="図表" type="A" order="3">
          <img class="width-800" src="./image/04-12-A03.svg"/>
          <p class="図表_注">p：病理所見</p>
          <p class="図表_引用">（大腸癌研究会（編）．大腸癌治療ガイドライン医師用2016年版．金原出版；2016．より許諾を得て転載）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="4">④cStage 0～cStage Ⅲ大腸癌の手術治療方針</div>
      <div class="アルゴリズム" order="4">
        <div class="図表" type="A" order="4">
          <img class="width-800" src="./image/04-12-A04.svg"/>
          <p class="図表_注">N：所属リンパ節，T2（MP）：固有筋層，T3（SS）：漿膜下層，T3（A）：腹膜被覆のない結腸あるいは直腸の周囲組織，T4a（SE）：臓側腹膜を貫通，T4b（SI，AI）：隣接臓器に浸潤，D0，1，2，3：リンパ節郭清度</p>
          <p class="図表_引用">（大腸癌研究会（編）．大腸癌治療ガイドライン医師用2016年版．金原出版；2016．より許諾を得て転載）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="5">⑤Stage Ⅳ大腸癌の治療方針</div>
      <div class="アルゴリズム" order="5">
        <div class="図表" type="A" order="5">
          <img class="width-800" src="./image/04-12-A05.svg"/>
          <p class="図表_引用">（大腸癌研究会（編）．大腸癌治療ガイドライン医師用2016年版．金原出版；2016．より許諾を得て転載）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="6">⑥切除不能進行再発大腸癌に対する化学療法のアルゴリズム</div>
      <div class="アルゴリズム" order="6">
        <div class="図表" type="A" order="6">
          <img class="width-800" src="./image/04-12-A06.svg"/>
          <p class="図表_注">＊1：Bmab，Rmab，Cmab，Pmabなどの分子標的治療薬の併用が推奨されるが，適応とならない場合は化学療法単独を行う。</p>
          <p class="図表_注">＊2：<span class="本文_イタリック">RAS</span>（<span class="本文_イタリック">KRA</span><span class="本文_イタリック">S</span>/<span class="本文_イタリック">N</span><span class="本文_イタリック">RAS</span>）野生型のみに適応。</p>
          <p class="図表_注">＊3：Infusional 5-FU＋<span class="本文_イタリック">l</span>-LV</p>
          <p class="図表_注">＊4：IRI不耐でなければ併用するのが望ましい。</p>
          <p class="図表_注">＊5：PS2以上に適応される。</p>
          <p class="図表_注">注：“/（スラッシュ）”は列記したレジメンのいずれかを選択するという意味である。</p>
          <p class="図表_注">FOLFOX：5-FU＋LV＋オキサリプラチン，CapeOX：カペシタビン＋オキサリプラチン，SOX：S-1＋オキサリプラチン，Bmab：ベバシズマブ，FOLFIRI：5-FU＋LV＋イリノテカン，Cmab：セツキシマブ，Pmab：パニツムマブ，FOLFOXIRI：5-FU＋LV＋イリノテカン＋オキサリプラチン，FL/Cape：5-FU持続静注＋LV＋カペシタビン，UFT：テガフール・ウラシル，LV：レボホリナート，IRIS：イリノテカン＋S-1，IRI：イリノテカン，Rmab：ラムシルマブ，TAS-102：トリフルリジン・チピラシル塩酸塩</p>
          <p class="図表_引用">（大腸癌研究会（編）．大腸癌治療ガイドライン医師用2016年版．金原出版；2016．より許諾を得て転載）</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶「大腸癌取扱い規約（第8版）」（2013年）では，結腸（盲腸，上行結腸，横行結腸，下行結腸，S状結腸）あるいは直腸（直腸S状部，上部直腸，下部直腸）に原発性に発生した癌腫を大腸癌と定義しており，虫垂癌，肛門管癌は上記の定義に該当する大腸癌とは別に集計すべきであることが記載されている。</p>
      <p class="本文_先頭丸数字">❷遺伝性大腸癌は，生殖細胞系列に原因遺伝子の病的変異を認め，一般集団より明らかに大腸癌のリスクが高い疾患の総称である。典型的な症例でも必ずしも病的変異が同定されないこともある。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶生検組織（hot biopsyを含む）の場合は，大腸癌生検組織分類でGroup 5と判定された場合，確定診断とする。内視鏡的摘除材料（ポリペクトミー，内視鏡的粘膜切除，内視鏡的粘膜下層剝離），外科的切除材料の場合には，ホルマリン固定後の組織学的検索により確定診断される。</p>
      <p class="本文_先頭丸数字">❷WHOではnon-invasive neoplasia（carcinoma <span class="本文_イタリック">in</span><span class="本文_イタリック"> </span><span class="本文_イタリック">situ</span>）をintraepithelial neoplasia（dysplasia）とし，invasive neoplasia（carcinoma）と区別している（Sclemper RJ, et al. Gut. 2000； <span class="本文_太字">47</span>：251-5.）。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_色文字太字">遺伝性大腸癌のなかで，最も頻度が高いのがリンチ症候群で，全大腸癌の</span><span class="本文_色文字太字">2</span><span class="本文_色文字太字">～</span><span class="本文_色文字太字">4</span><span class="本文_色文字太字">％を占める</span>。リンチ症候群の診断は，臨床病理学的所見（既往歴や家族歴を含む）からリンチ症候群を疑い，腫瘍のマイクロサテライト不安定性検査あるいはミスマッチ修復蛋白の免疫染色などによるスクリーニングを経て，遺伝学的検査を行う患者を絞り込む。ミスマッチ修復遺伝子（<span class="本文_イタリック">MLH</span><span class="本文_イタリック">1</span>，<span class="本文_イタリック">MSH</span><span class="本文_イタリック">2</span>，<span class="本文_イタリック">MSH</span><span class="本文_イタリック">6</span>，<span class="本文_イタリック">PMS</span><span class="本文_イタリック">2</span>のいずれか）の病的変異を同定することで確定診断される（<span class="本文_図表番号">アルゴリズム①</span>）。家族性大腸腺腫症では，通常大腸全域に100個以上の腺腫を認めれば，臨床的に診断可能で，原因遺伝子<span class="本文_イタリック">APC</span>の病的変異の同定は必ずしも必要ない（例外は存在する）。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶<span class="本文_太字">内視鏡的治療</span></p>
      <p class="本文_先頭丸数字">①<span class="本文_色文字太字">cTis</span><span class="本文_色文字太字">（</span><span class="本文_色文字太字">M</span><span class="本文_色文字太字">）癌あるいは</span><span class="本文_色文字太字">cT</span><span class="本文_色文字太字">1</span><span class="本文_色文字太字">（</span><span class="本文_色文字太字">SM</span><span class="本文_色文字太字">）軽度浸潤癌で，内視鏡的一括摘除が可能な腫瘍は，内視鏡的治療の適応となる</span>（<span class="本文_図表番号">アルゴリズム②</span>）。</p>
      <p class="本文_先頭丸数字">②内視鏡的摘除後のpT1（SM）癌については，<span class="本文_図表番号">アルゴリズム③</span>に従って，リンパ節郭清を伴う腸切除を考慮する。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">外科治療</span></p>
      <p class="本文_先頭丸数字">①cTis（M）癌あるいはcT1（SM）軽度浸潤癌であっても，内視鏡的一括摘除が不可能な場合や，cT1（SM）高度浸潤癌の場合，外科治療の適応となる（<span class="本文_図表番号">アルゴリズム②</span>）。</p>
      <p class="本文_先頭丸数字">②内視鏡的摘除後の組織学的検索で，垂直断端陽性あるいはリンパ節転移の危険因子を有する場合，リンパ節郭清を伴う腸切除を考慮する（<span class="本文_図表番号">アルゴリズム③</span>）。</p>
      <p class="本文_先頭丸数字">③リンパ節の郭清度は，術前の臨床所見および術中所見によるリンパ節転移の有無と腫瘍の壁深達度の総合判断から決定される（<span class="本文_図表番号">アルゴリズム④</span>）。<span class="本文_色文字太字">手術の適応となる大多数の大腸癌では，</span><span class="本文_色文字太字">D</span><span class="本文_色文字太字">3</span><span class="本文_色文字太字">レベルのリンパ節郭清が行われる</span>。</p>
      <p class="本文_先頭丸数字">④直腸癌に対する外科治療の原則は直腸間膜切除（全切除あるいは部分切除）を伴う腸管切除で，腫瘍下縁が腹膜反転部より肛門側にあり，かつ固有筋層を越えて浸潤している場合には，側方（骨盤壁）リンパ節郭清の適応がある。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">Stage</span><span class="本文_太字"> </span>Ⅳ<span class="本文_太字">大腸癌の治療</span>：診断時に遠隔転移を伴う場合は，遠隔転移と原発巣の各々の切除の可否を検討して治療方針を決定する。原発巣が切除可能でも，遠隔転移が切除不能な場合，化学療法を確実に行うために原発巣切除の要否を決定する（<span class="本文_図表番号">アルゴリズム⑤</span>）。</p>
      <p class="本文_先頭丸数字">❹<span class="本文_太字">再発大腸癌の治療</span>：根治切除可能であれば再発巣（主に肝転移，肺転移など）の切除を考慮する。再発巣が根治切除不能もしくはボーダーラインであっても，全身化学療法が奏効した後に，根治切除可能になった場合（conversion therapy），生存期間の延長や治癒が期待できる。</p>
      <p class="本文_先頭丸数字">❺<span class="本文_太字">化学療法</span></p>
      <p class="本文_先頭丸数字">①<span class="下線">術後補助化学療法</span></p>
      <p class="本文_級下げ">1）<span class="本文_色文字太字">根治切除（</span><span class="本文_色文字太字">R</span><span class="本文_色文字太字">0</span><span class="本文_色文字太字">切除）が行われた</span><span class="本文_色文字太字">Stage</span><span class="本文_色文字太字"> </span>Ⅲ<span class="本文_色文字太字">大</span><span class="本文_色文字太字">腸癌が適応であるが，再発リスクが高い</span><span class="本文_色文字太字">Stage</span><span class="本文_色文字太字"> </span>Ⅱ<span class="本文_色文字太字">大腸癌にも適切なインフォームド・コンセントのもとに，適応を考慮する</span>。直腸癌に対する補助化学療法のエビデンスは少ないが，術後補助化学療法のエビデンスが多い結腸癌に準じて行う。</p>
      <p class="本文_級下げ">2）主要臓器機能が保たれ，performance status（PS）が0～1などが導入の基準となる。術後4～8週頃までに開始することが望ましく，投与期間の原則は6ヵ月である。</p>
      <p class="本文_級下げ">3）推奨されるレジメンは5-FU＋LV，UFT＋LV，カペシタビン，FOLFOX4/mFOLFOX6，CapeOX，S-1（テガフール・ギメラシル・オテラシルカリウム）である。</p>
      <p class="本文_先頭丸数字">②<span class="下線">切除不能進行再発大腸癌に対する化学療法</span></p>
      <p class="本文_級下げ">1）<span class="本文_色文字太字">腫瘍増大を遅延させて延命と症状コントロールを行うことが目的であるが，化学療法が奏効して切除可能になることがある</span>。</p>
      <p class="本文_級下げ">2）強力な治療が適応となる患者と強力な治療が適応とならない患者に分けて治療方針を選択することが望ましい（<span class="本文_図表番号">アルゴリズム⑥</span>）。</p>
      <p class="本文_級下げ">3）抗EGFR抗体薬は<span class="本文_イタリック">RAS</span>（<span class="本文_イタリック">KRA</span><span class="本文_イタリック">S</span>/<span class="本文_イタリック">N</span><span class="本文_イタリック">RAS</span>）野生型のみが適応である。</p>
      <p class="本文_級下げ">4）一次・二次治療において，分子標的薬が適応となる場合には化学療法薬（基軸レジメン）との併用が推奨される。ただし，重篤な併存疾患の存在や腫瘍の状態により分子標的薬の適応がない場合には化学療法単独療法を選択する。</p>
      <p class="本文_級下げ">5）主要臓器機能が保たれ，PS0～2などが適応の原則である。</p>
      <p class="本文_先頭丸数字">❻<span class="本文_太字">放射線療法</span></p>
      <p class="本文_先頭丸数字">①<span class="下線">補助放射線療法</span>：直腸癌に対する補助放射線療法には術前照射，術中照射，術後照射がある。目的は術後の局所制御率の向上である。欧米の報告では，術前照射では肛門括約筋温存率と切除率の向上が示されているが，生存率向上に関するエビデンスは乏しい。術前照射では化学療法を併用することが多い。</p>
      <p class="本文_先頭丸数字">②<span class="下線">緩和的放射線療法</span>：骨盤内病変による疼痛，出血，便通障害などの症状緩和を目的として行われる。症状緩和率は疼痛89～93％，出血79～100％，神経性症状52％，腫瘍の圧排による症状71～88％などで，症状緩和持続期間は3～10ヵ月である。骨盤外病変に対しても行われ，骨転移に対しては疼痛の軽減，病的骨折の予防，脊髄麻痺の予防と治療を目的とする。</p>
      <p class="本文_先頭丸数字">❼<span class="本文_太字">遺伝性大腸癌に対する治療</span></p>
      <p class="本文_先頭丸数字">①家族性大腸腺腫症では，進行大腸癌が発生する前（20歳代後半まで）に大腸全摘・回腸囊肛門吻合術あるいは結腸全摘・回腸直腸吻合術を行うことが推奨される。進行大腸癌を合併した場合には，大腸癌の部位，Stageのほかに，家族性大腸腺腫症の病態などを総合的に考慮して術式を決定する。直腸を温存する術式が選択された場合には，残存長にかかわらず生涯にわたる残存直腸のサーベイランスが必要である。大腸全摘術・回腸囊肛門吻合術における一時的回腸人工肛門造設の要否については，個別に対応する。また，腹腔鏡手術の安全性は担保されているものの，施設の習熟度に応じ，十分なインフォームド・コンセントのもとで適応を決定する。</p>
      <p class="本文_先頭丸数字">②リンチ症候群に発生した大腸癌に対する術式については，散発性大腸癌と同様か，異時性多発癌の発生リスクを考慮して拡大手術を行うかについて，コンセンサスは得られていない。リンチ症候群では，1年に1回の大腸内視鏡検査によるサーベイランスと腺腫の摘除は大腸癌の死亡を減少させる。</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">①<span class="本文_色文字太字">切除不能再発大腸癌に対するアフリべルセプト療法</span>：アフリベルセプトはVEGF-A/B，PlGF（placental growth factor）に結合し，血管新生を阻害する。オキサリプラチンを含むレジメンに不応となった患者の二次治療としてFOLFIRI療法への上乗せ効果を検証した第Ⅲ相試験であるVELOUR試験において，主要評価項目である全生存期間の延長効果が示された。このことからオキサリプラチンを含むレジメン治療後の二次治療として，FOLFIRIとの併用でアフリベルセプトをFDAが承認した。アフリベルセプトは2017年5月に本邦でも薬価収載された。</p>
      <p class="本文_最近の話題">②<span class="本文_色文字太字">ミスマッチ修復欠損大腸癌に対する抗</span><span class="本文_色文字太字">P</span><span class="本文_色文字太字">D</span><span class="本文_色文字太字">-</span><span class="本文_色文字太字">1</span><span class="本文_色文字太字">抗体薬</span>：最近ミスマッチ修復欠損（あるいはマイクロサテライト不安定性陽性）大腸癌に対する抗PD-1抗体であるペムブロリズマブの効果が注目されている。ミスマッチ修復欠損大腸癌13例（リンチ症候群11例）の全例，大腸癌以外のミスマッチ修復欠損腫瘍10例中9例できわめて優れた効果が確認され（Le DT, et al. N Engl J Med. 2015；<span class="本文_太字">372</span>：2509-20.），2017年5月にFDAではミスマッチ修復欠損を有する固形腫瘍に対し，ペムブロリズマブを承認した。本邦での承認も期待されている。</p>
      <p class="本文_最近の話題">③<span class="本文_色文字太字">大腸癌原発部位による予後と治療効果の違い</span>：近年，原発巣の部位による大腸癌の予後・治療成績の差が話題になっている。2016年の米国臨床腫瘍学会では切除不能再発大腸癌に対する分子標的薬を併用した化学療法の効果について，原発巣の部位による違いがCALGB/SWOG80405試験の解析結果から報告された。左側結腸癌・直腸癌ではセツキシマブ併用群はベバシズマブ併用群より全生存期間が有意に良好であったのに対し，右側結腸癌（横行結腸は除く）ではセツキシマブ併用群はべバシズマブ併用群より全生存期間が不良な傾向がみられた（Venook AP, et al. J Clin Oncol. 2016；<span class="本文_太字">34</span> Suppl：abstr 3504.）。また3つの臨床試験（FIRE-3，PEAK，CALGB/SWOG80405）のメタ解析により，全生存期間において<span class="本文_イタリック">RAS</span>野生型の左側結腸・直腸癌では抗EGFR抗体薬が有意に良好な一方，右側結腸癌ではべバシズマブが良好な傾向が認められた（Holch JW, et al. Eur J Cancer. 2017；<span class="本文_太字">70</span>：87-98.）。さらに2017年の米国臨床腫瘍学会ではCALGB80405試験の原発巣別の解析結果が報告され，交絡因子の影響を除外した多変量解析の結果，原発巣の左右は独立予後因子であることが示された（Venook AP, et al. J Clin Oncol. 2017；<span class="本文_太字">35</span> Suppl：abstr 3503.）。本邦のガイドラインでは，原発巣の部位による<span class="本文_イタリック">RAS</span>野生型大腸癌に対する一次治療の選択に関する記載はない。</p>
      <p class="本文_最近の話題">④<span class="本文_色文字太字">閉塞性大腸癌に対するステント留置術</span>：本邦では2011年から大腸用ステントが薬事承認されている。大腸癌術後の吻合部再発，転移再発，狭窄を伴う切除不能大腸癌の狭窄解除（緩和治療）と，閉塞症状を呈する大腸癌に対する緊急手術回避（BTS）が適応である。2014年に欧州消化器内視鏡学会から，臨床症状を伴う閉塞性左側大腸癌への標準治療としてのBTSは長期予後の点から推奨できないとの見解が示されている。一方，本邦で行われた多施設共同前向き試験に登録された513例において，ステント留置の技術的成功率97.9％，臨床的成功率95.5％，穿孔率2％と報告され（Matsuzawa T, et al. Gastrointest Endosc. 2015；<span class="本文_太字">82</span>：697-707.），実地臨床でも広く普及している。今後は長期予後に関する安全性が検討されていくと考えられる。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】大腸癌に対する化学療法を大別すると，原発巣の治癒切除後の再発抑制を目的とした術後補助化学療法と，切除不能進行再発大腸癌に対する主に延命・症状緩和を目的とした化学療法がある。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">術後補助化学療法</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①UFT/LV療法</p>
              <p class="具体的処方_処方例1W下げ">
                <span class="具体的処方_色文字">ユーエフティ</span>
              </p>
              <p class="具体的処方_処方例1W下げ">100 mg/m<span class="上付き">2</span>×3回/日</p>
              <p class="具体的処方_処方例">＋<span class="具体的処方_色文字">ユーゼル</span></p>
              <p class="具体的処方_処方例1W下げ">25 mg/body×3回/日</p>
              <p class="具体的処方_処方例1W下げ">28日間連続投与後，7日間休薬，5サイクル施行する</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①<span class="本文_イタリック">ユーエフティ</span>と<span class="本文_イタリック">ユーゼル</span>は食事の前後1時間を避け，約8時間間隔で内服（たとえば6時，14時，22時）。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②カペシタビン療法</p>
              <p class="具体的処方_処方例1W下げ">
                <span class="具体的処方_色文字">ゼローダ</span>
              </p>
              <p class="具体的処方_処方例1W下げ">1250 mg/m<span class="上付き">2</span>×2回/日</p>
              <p class="具体的処方_処方例1W下げ">14日間連続投与後，7日間休薬，8サイクル施行する</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②手足症候群の出現に注意が必要。予防には保湿剤を塗布する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③CapeOX療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③<span class="本文_イタリック">エルプラット</span>は蓄積性の末梢神経障害に注意が必要。Grade 3の末梢神経障害が出現する前に投与を中止する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="2" rowspan="">
              <p class="具体的処方_処方例"><img class="width-600" src="./image/04-12-P01.svg"/><br/>３週間を１サイクルとして８サイクル繰り返す</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">切除不能進行再発大腸癌に対する化学療法</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①CapeOX（またはSOX）＋ベバシズマブ療法</p>
              <p class="具体的処方_処方例1W下げ">
                <span class="具体的処方_色文字">アバスチン</span>
              </p>
              <p class="具体的処方_処方例1W下げ">7.5 mg/kg/回，点滴静注（30～90分）</p>
              <p class="具体的処方_処方例1W下げ">1日目＋Cape OX（またはSOX）療法</p>
              <p class="具体的処方_処方例1W下げ">3週毎に繰り返す</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①<span class="本文_イタリック">アバスチン</span>の投与は血栓症の既往がある場合は避ける。蛋白尿，消化管穿孔にも注意が必要。大手術後では4週間以上経過してから投与を開始する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②FOLFIRI＋セツキシマブ（またはパニツムマブ）療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②<span class="本文_イタリック">アービタックス</span>や<span class="本文_イタリック">ベクティビックス</span>では皮膚障害に注意し，予防には保湿剤を塗布し，<span class="本文_イタリック">ミノマイシン</span>を内服する。皮疹の出現時にはステロイド外用剤を用いる。間質性肺炎の出現にも注意。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="2" rowspan="">
              <p class="具体的処方_処方例"><img class="width-600" src="./image/04-12-P02.svg"/><br/>２週毎に繰り返す<br/>アービタックス，べクティビックスともに<span class="本文_イタリック">KRAS╱NRAS</span>野生型が適応<br/>アービタックスは週１回投与。初回は400 mg/m<span class="上付き">2</span> を２時間かけて，２回目以降は250 mg/m<span class="上付き">2</span> を１時間かけて投与<br/>ベクティビックスは２週毎に6 mg/kg を１時間かけて投与</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③レゴラフェニブ療法</p>
              <p class="具体的処方_処方例1W下げ">
                <span class="具体的処方_色文字">スチバーガ</span>
              </p>
              <p class="具体的処方_処方例1W下げ">160 mg/body×1回/日，食後，21日間連続投与，7日間休薬を繰り返す</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③手足症候群，下痢，高血圧などの有害事象の頻度が高い。手足症候群には保湿剤やステロイドを塗布する。2サイクルまでは週1回の肝機能検査を行うことが望ましい。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="2" rowspan="">
              <p class="具体的処方_処方例">
                <img class="width-600" src="./image/04-12-P03.svg"/>
              </p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④TAS-102療法</p>
              <p class="具体的処方_処方例1W下げ">
                <span class="具体的処方_色文字">ロンサーフ</span>
              </p>
              <p class="具体的処方_処方例1W下げ">35 mg/m<span class="上付き">2</span>×2回/日，朝夕食後，5日間連続内服後，2日間休薬を2回繰り返した後，14日休薬。これを1コースとして繰り返す</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④骨髄抑制の頻度が高い。下痢にも注意が必要。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="2" rowspan="">
              <p class="具体的処方_処方例">
                <img class="width-600" src="./image/04-12-P04.svg"/>
              </p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>埼玉医科大学総合医療センター消化管・一般外科<span class="著者名">　石田　秀行</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
