<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>14</chapter>
    <chapter-title>腎・泌尿器疾患</chapter-title>
    <section>04</section>
    <article-code>14-04</article-code>
    <article-title>前立腺癌</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>4 前立腺癌</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  前立腺癌診療ガイドライン2016年版（2016）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">ISUP2014の改訂に伴いGleasonスコアから新しくグレードグループ分類が記載され，悪性度評価法の見直しが示された。</li>
        <li class="whatsnew_リスト">ロボット手術の普及や放射線照射法や機器の進歩による根治療法の解説が大幅に追加され，また局所療法についても言及された。</li>
        <li class="whatsnew_リスト">去勢抵抗性前立腺癌（CRPC）に対する新規ホルモン薬，化学療法薬についての記載の充実と骨転移治療に関する方針が記載された。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">①前立腺癌診療アルゴリズム</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/14-04-A01.svg"/>
          <p class="図表_引用">（日本泌尿器科学会（編）．前立腺癌診療ガイドライン2016年版．メディカルレビュー社；2016©日本泌尿器科学会．より許諾を得て改変）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="2">②住民検診・人間ドックにおける受診対象年齢と泌尿器科専門医紹介までの前立腺がん検診アルゴリズム</div>
      <div class="アルゴリズム" order="2">
        <div class="図表" type="A" order="2">
          <img class="width-800" src="./image/14-04-A02.svg"/>
          <p class="図表_注">＊：高齢者におけるPSA検診継続の判断をするための余命を予測する正確なモデルは現時点ではないが，将来の方向性として健康状態評価手段（G8 geriatric screening tool）等を検診受診推奨判定に用いることは，方策の1つである。</p>
          <p class="図表_引用">（日本泌尿器科学会（編）．前立腺癌診療ガイドライン2016年版．メディカルレビュー社；2016©日本泌尿器科学会．より許諾を得て改変）</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p>　「前立腺癌診療ガイドライン2016年版」が対象とする前立腺癌は，腺房由来の腺癌が前提とされている。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶<span class="本文_太字">PSA</span><span class="本文_太字">検査</span>：前立腺特異抗原（PSA）による住民検診で死亡率低下効果は明らかになっているが，一方で過剰診療，過剰治療の不利益の可能性も十分に啓発したうえで検診，精密検査が可能な診療機関への受診が勧められる。年齢階層別PSAカットオフ値は50～64歳：3.0 ng/mL以下，65～69歳：3.5 ng/mL以下，70歳以上：4.0 ng/mL以下と記載されている。以上をふまえ，前立腺がん検診ガイドラインの改訂作業が行われている。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">直腸診</span>：PSA検査が普及した現在においても前立腺癌発見と局所診断に対して有用であり，施行が望まれる。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">画像診断</span>：原発巣の評価（T病期診断）にはMRIによる癌の局在診断は非常に有用である。<span class="本文_色文字太字">Prostate</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">Imaging</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">and</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">Reporting</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">and</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">Data</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">System</span><span class="本文_色文字太字">（</span><span class="本文_色文字太字">PIRADS</span><span class="本文_色文字太字">）</span>によるスコア化は，画像評価の標準化により癌検出感度，特異度を向上させることが報告されている。リンパ節の評価（N病期診断）には感度，特異度ともに十分ではないがCT，MRIが用いられる。遠隔転移の評価（M病期診断）には骨シンチグラフィーやCT，MRIが有用である。</p>
      <p class="本文_先頭丸数字">❹<span class="本文_太字">前立腺生検</span>：生検法は経直腸的と経会陰的アプローチがありいずれも同等の有用性があるが，初回生検では経直腸的超音波ガイド下に10～12ヵ所以上の生検が勧められる。</p>
      <p class="本文_先頭丸数字">❺<span class="本文_太字">その他</span>：前立腺癌臨床の変遷に伴い，従来の悪性度評価法であるGleason分類によるGleasonスコア（GS）の問題点が指摘され，International Society of Urological Pathology（ISUP）2014のコンセンサス会議で承認された<span class="本文_色文字太字">グレードグループ分類</span>が明示された。従来のGSに併記し，グレードグループ1～5の分類で悪性度を評価していくようになり，いずれ1本化されると考えられる。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶<span class="本文_太字">リスク分類</span>：2016年版前立腺癌診療アルゴリズムではまず病理学的診断，直腸診，画像診断による病期診断を行い，<span class="本文_色文字太字">限局性癌，局所進行性癌，転移性癌の</span><span class="本文_色文字太字">3</span><span class="本文_色文字太字">つに病期を分類し</span>，<span class="本文_色文字太字">リスク分類</span>により<span class="本文_色文字太字">限局性癌を低・中間・高リスクの</span><span class="本文_色文字太字">3</span><span class="本文_色文字太字">つに分類</span>している。D’Amicoのリスク分類が一般的によく用いられる。局所進行性癌はNCCNのリスク分類の超高リスクが当てはまる。</p>
      <p class="本文_先頭丸数字">①<span class="下線">D</span><span class="下線">’</span><span class="下線">Amico</span><span class="下線">のリスク分類</span></p>
      <p class="本文_先頭丸数字">　低リスク：PSA≦10 ng/mLかつGS≦6かつT1～T2a</p>
      <p class="本文_先頭丸数字">　中間リスク：PSA 10～20 ng/mLまたはGS7またはT2b</p>
      <p class="本文_先頭丸数字">　高リスク：PSA＞20 ng/mLまたはGS8～10またはT2c</p>
      <p class="本文_先頭丸数字">②<span class="下線">NCCN</span><span class="下線">のリスク分類</span></p>
      <p class="本文_先頭丸数字">　局所進行・超高リスク：T3b～T4</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">限局性癌</span>：期待余命を勘案し，治療によるベネフィットとQOLへの影響，合併症を考慮したうえで，さまざまな治療法が選択可能である。</p>
      <p class="本文_先頭丸数字">①低リスク群にはPSA監視療法も考慮したうえで，手術療法や放射線療法の根治療法が提示されている。</p>
      <p class="本文_先頭丸数字">②中間および高リスク群には手術療法や放射線療法の根治療法を第1選択として考慮する。手術療法は患者の合併症や既往手術歴により開腹手術，鏡視下手術，ロボット支援手術が選択される。放射線療法では強度変調放射線治療（IMRT）をはじめとした外照射と組織内照射の小線源療法が主体となるが，一般的に<span class="本文_色文字太字">中間リスクでは</span><span class="本文_色文字太字">6</span><span class="本文_色文字太字">ヵ月</span>，<span class="本文_色文字太字">高リスクでは</span><span class="本文_色文字太字">2</span><span class="本文_色文字太字">年以上のホルモン療法併用</span>が推奨される。また，年齢，合併症など患者背景を勘案し，focal therapyやホルモン療法単独が選択される場合もある。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">局所進行性癌</span>：放射線療法とホルモン療法の併用が標準治療であるが，症例によっては手術療法も選択可能である。また，患者背景を考慮してホルモン療法単独が選択される場合もある。</p>
      <p class="本文_先頭丸数字">❹<span class="本文_太字">転移性癌</span>：ホルモン療法が標準的な初期治療であるが，近年欧米の複数のランダム化比較試験の結果より，症例によっては初回よりドセタキセル6サイクルまでをホルモン療法に併用することにより予後延長効果が期待される。</p>
      <div class="見出し_小見出し">治療方法</div>
      <p class="本文_先頭丸数字">❶<span class="本文_太字">PSA</span><span class="本文_太字">監視療法</span>：前立腺生検，画像検査により診断された限局性癌で低リスクの症例に限って即時に治療介入せずに経過観察をする。PSA検査や直腸診は3～6ヵ月ごと，生検は1～3年ごとに行い，GSの上昇や生検陽性本数の増加などリスクのreclassification（再分類）があれば積極的治療を行うことが推奨される。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">手術療法</span>：開放手術，腹腔鏡下または，ロボット支援腹腔鏡下前立腺全摘除術が行われる。症例によっては神経温存術式にてQOLを維持することや，中，高リスク症例に対しては拡大リンパ節郭清を行うことが推奨される。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">放射線療法</span>：外照射（IMRT，陽子線治療，重粒子線治療など）または組織内照射（ヨウ素125永久挿入密封小線源療法，イリジウム192高線量率組織内照射）が行われる。</p>
      <p class="本文_先頭丸数字">❹<span class="本文_太字">ホルモン療法</span>：初期治療として外科的去勢術（両側精巣摘除術）またはLH-RH（luteinizing hormone-releasing hormone）アゴニストやLH-RHアンタゴニスト製剤を用いた内科的去勢術が行われる。本邦ではさらに抗アンドロゲン薬を併用したCAB（combined androgen blockade）療法が行われるのが一般的である。LH-RHアゴニスト投与初期はテストステロンの一過性上昇が起こり，尿路閉塞，骨転移巣の増悪に起因する骨痛や脊髄圧迫などの<span class="本文_色文字太字">フレアアップ現象</span>が起こることがあり，<span class="本文_色文字太字">抗アンドロゲン薬の併用療法を考慮すべき</span>である。</p>
      <p class="本文_先頭丸数字">❺<span class="本文_太字">CRPC</span><span class="本文_太字">の薬物療法</span></p>
      <p class="本文_先頭丸数字">①CRPCの定義は2016年版ではっきりと変更点が明記されている。欧州泌尿器科学会（EAU）のガイドラインに従い，外科的あるいは内科的去勢治療継続にて血清テストステロン値が50 ng/dL未満にもかかわらず，1週間以上の測定間隔でPSA値が3回連続で上昇し，最低値から50％以上の上昇が2回みられた場合，かつPSA値が2.0 ng/mL以上とされている。また一方PSA変化だけでは把握できない前立腺癌の増悪がしばしば認められることから，<span class="本文_色文字太字">画像上の増悪や新規病変の出現も</span><span class="本文_色文字太字">CRPC</span>と定義されている。また，<span class="本文_色文字太字">アンチアンドロゲン除去症候群（</span><span class="本文_色文字太字">AWS</span><span class="本文_色文字太字">）の確認は必須ではなくなった</span>。これは，アンドロゲン非依存性の前立腺癌の混在による癌の不均一性が臨床で考慮されるようになったためである。</p>
      <p class="本文_先頭丸数字">②新規アンドロゲン受容体標的薬が本邦でも保険適用となったことを受け，CRPCに対する治療としてドセタキセル後または前にどのような治療を選択するかについて<span class="本文_色文字太字">アビラテロン</span>，<span class="本文_色文字太字">エンザルタミド</span>に関するエビデンスが明記されている。<span class="本文_色文字太字">これら</span><span class="本文_色文字太字">2</span><span class="本文_色文字太字">剤には交叉耐性が出現する可能性があることを留意する</span>。また，ドセタキセル抵抗性となった症例に対してはカバジタキセル使用が推奨されている。</p>
      <p class="本文_級下げ">1）アビラテロンはCYP17阻害薬であり，内因性アンドロゲン合成阻害作用がある反面，ステロイド骨格がアルドステロンの合成にバイパスされることにより肝機能障害のほかに低K血症，高血圧，体液貯留・浮腫といった副作用がみられることがあり，<span class="本文_色文字太字">ステロイド（プレドニゾロン）の併用が必須である</span>。</p>
      <p class="本文_級下げ">2）エンザルタミドはアンドロゲン受容体シグナル伝達阻害薬であり，従来の抗アンドロゲン薬であるビカルタミドやフルタミドなどといったビンテージホルモン薬抵抗性となった症例でも有効である。近年では，特異的なアンドロゲン受容体の変異体には無効であることがわかってきている。有害事象としては痙攣発作や疲労，胃腸障害，高血圧，ほてりなどがある。対策として年齢や，有害事象の程度をみながら減量休薬を行うことも重要である。</p>
      <p class="本文_級下げ">3）カバジタキセルはドセタキセルの側鎖を修飾したタキサン系抗癌剤であり，ドセタキセル抵抗性の症例に使用可能である。重篤な肝機能障害を有する症例には禁忌であり，間質性肺炎併発にも十分な注意が必要であるが，欧米人と比較し日本人では発熱性好中球減少症の発症頻度が高いため，<span class="本文_色文字太字">予防的な</span><span class="本文_色文字太字">G</span><span class="本文_色文字太字">-</span><span class="本文_色文字太字">C</span><span class="本文_色文字太字">SF</span><span class="本文_色文字太字">製剤の使用</span>（持続型製剤のペグフィルグラスチム）が勧められる。投与量，投与間隔は個々の症例に応じて減量や延長が検討されるべきである。</p>
      <p class="本文_先頭丸数字">❻<img class="" src="./image/14-04-2.png"/></p>
      <p class="本文_級下げ">①塩化ラジウム223は2016年に日本でも使用可能になった製剤で，α線放出核種であるラジウム223が骨に特異的に集積することで骨転移巣の制御が可能である。放射線治療医の協力のもと4週に1回，6回まで投与可能で，骨転移を有するCRPC症例に対して比較的全身状態がよいうちに投与を考慮する。合併症として骨髄抑制や薬剤の消化管排泄による下痢に注意が必要である。</p>
      <p class="本文_級下げ">②骨転移に対する骨関連事象の発症抑制やホルモン療法による骨粗鬆症進展抑制に対して，積極的に骨修飾薬（BMA）の使用が推奨される。ビスホスホネート製剤であるゾレドロン酸やRANKL阻害薬であるデノスマブの使用を考慮する。使用に関しては口腔内のチェックと腎機能に留意し，低Ca血症，顎骨壊死などの有害事象に注意を払う必要がある。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①PSA 4.0 ng/mLを超えた患者</p>
      <p class="本文_専門医へのコンサルト">②年齢階層別PSA基準値を超えた患者</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">①PSA検診に関しては大規模コホート研究にて癌死亡率低下効果とともに費用対効果の知見が蓄積され，前立腺がん検診ガイドラインの改訂作業が行われている。</p>
      <p class="本文_最近の話題">②診断に関してはMRIによるPIRADSスコアを参考にし，MRI-US画像融合狙撃生検が可能な機種では，臨床的に有意な癌の検出に優れていることが報告されている。</p>
      <p class="本文_最近の話題">③欧米のガイドラインでは転移性前立腺癌に対する初回ホルモン療法にドセタキセル化学療法併用が高い推奨グレードとなり，特にhigh volume症例に対しては標準治療とされているが，日本では医療保険，人種差による合併症の増加，ホルモン療法感受性を勘案して併用を考慮する必要がある。</p>
      <p class="本文_最近の話題">④2017年米国臨床腫瘍学会（ASCO）において，CRPCより前倒しでの転移性前立腺癌の初回ホルモン療法としてアビラテロンの予後改善効果が報告され，今後新規アンドロゲン受容体標的薬の適応拡大が注目される。</p>
      <p class="本文_最近の話題">⑤CRPCの薬剤選択については次世代シーケンサーの活用により血中の循環腫瘍細胞（CTC）やセルフリーDNAの解析などで今後より的確なバイオマーカーの登場が期待されるが，現状では患者の状態，合併症，腫瘍volume，初期ホルモン療法への反応など個々の患者背景を勘案して，新規薬剤を十分使い切れるよう逐次療法を行っていくことが推奨される。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】限局性癌，局所進行性癌にはまず手術療法，放射線療法を考慮し，年齢，合併症など患者背景に応じてホルモン療法単独が選択される。リスク分類に応じて放射線療法にホルモン療法が併用される。転移性癌には初回ホルモン療法が選択され，CRPCとなった時点で新規ホルモン療法剤（アビラテロン，エンザルタミド）またはドセタキセルが症例に応じて選択される。ドセタキセル抵抗性となった症例でカバジタキセルを使用する。<span class="本文_イタリック">ゾーフィゴ</span>は放射線治療医との連携のもと，骨転移有意なCRPCに対して使用される。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">ホルモン療法の初期治療</span>
              </p>
            </td>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">　CAB療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①②（または③）のいずれか＋④または⑤</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ゾラデックス</span><span class="具体的処方_色文字">LA</span><span class="具体的処方_色文字">デポ</span></p>
              <p class="具体的処方_処方例1W下げ">10.8 mg×1回/12～13週，皮下注</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">リュープリン</span><span class="具体的処方_色文字">SR</span></p>
              <p class="具体的処方_処方例1W下げ">11.25 mg×1回/12週，皮下注</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">リュープリンプロ</span></p>
              <p class="具体的処方_処方例1W下げ">22.5 mg×1回/24週，皮下注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①②はLH-RHアゴニストであり，投与初期に一過性のテストステロン上昇に伴う症状増悪（フレアアップ現象）の可能性があるため，④～⑥を併用する。</p>
              <p class="具体的処方_ポイント">国内ではCAB療法として①②（または③）のいずれかに④または⑤を併用継続する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">ゴナックス</span></p>
              <p class="具体的処方_処方例1W下げ">初回：240 mg/回</p>
              <p class="具体的処方_処方例1W下げ">2回目以降：80 mg×1回/4週，皮下注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③LH-RHアンタゴニストであり，フレアアップ現象はみられないため，単独でも使用可能である。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">カソデックス</span></p>
              <p class="具体的処方_処方例1W下げ">80 mg×1回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">⑤<span class="具体的処方_色文字">オダイン</span></p>
              <p class="具体的処方_処方例1W下げ">125 mg×3回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">⑥<span class="具体的処方_色文字">プロスタール</span></p>
              <p class="具体的処方_処方例1W下げ">50 mg×2回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">⑥ステロイド性抗アンドロゲン薬であり，使用する機会が減少している。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">CRPC</span>
                <span class="具体的処方_色文字">の薬物療法</span>
              </p>
            </td>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅰ）ホルモン療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ザイティガ</span></p>
              <p class="具体的処方_処方例1W下げ">1000 mg×1回/日，空腹時</p>
              <p class="具体的処方_処方例">＋<span class="具体的処方_色文字">プレドニン</span></p>
              <p class="具体的処方_処方例1W下げ">10 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">初期治療で使用した抗アンドロゲン薬を中止し，患者背景に応じて①②のいずれかを使用する。AWS観察は必須ではない。</p>
              <p class="具体的処方_ポイント">①<span class="本文_イタリック">プレドニン</span>の併用が必須である。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">イクスタンジ</span></p>
              <p class="具体的処方_処方例1W下げ">160 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②患者背景により減量処方も検討する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">デカドロン</span></p>
              <p class="具体的処方_処方例1W下げ">0.5 mg×1回/日，朝</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）化学療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">タキソテール</span></p>
              <p class="具体的処方_処方例1W下げ">70～75 mg/m<span class="上付き">2</span>×1回/3～4週，点滴静注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①②適宜減量・投与間隔の延長を行う。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ジェブタナ</span></p>
              <p class="具体的処方_処方例1W下げ">20～25 mg/m<span class="上付き">2</span>×1回/3週，点滴静注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②G-CSF製剤（<span class="本文_イタリック">ジーラスタ</span>など）の併用を行う。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">エストラサイト</span>（156.7 mg）</p>
              <p class="具体的処方_処方例1W下げ">1～2カプセル×2回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶骨転移に対する治療</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ゾーフィゴ</span></p>
              <p class="具体的処方_処方例1W下げ">55 kBq/kg×1回/4週，最大6回まで，緩徐に静注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①骨髄抑制，炎症性腸疾患のある患者へは慎重投与。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ゾメタ</span></p>
              <p class="具体的処方_処方例1W下げ">4 mg×1回/4週，点滴静注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②③低Ca血症と顎骨壊死に注意。</p>
              <p class="具体的処方_ポイント">②ビスホスホネート製剤。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">ランマーク</span></p>
              <p class="具体的処方_処方例1W下げ">120 mg×1回/4週，皮下注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③ヒト型抗RANKLモノクローナル抗体。低Ca血症予防として<span class="本文_イタリック">デノタス</span>2錠×1回/日を併用する。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>山口大学大学院泌尿器科学<span class="著者名">　松本　洋明・松山　豪泰</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
