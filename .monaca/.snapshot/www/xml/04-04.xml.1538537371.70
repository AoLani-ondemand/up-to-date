<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>04</chapter>
    <chapter-title>消化管疾患</chapter-title>
    <section>04</section>
    <article-code>04-04</article-code>
    <article-title>H.pylori感染胃炎</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>4 <span class="本文_イタリック">H. </span><span class="本文_イタリック">pylori</span>感染胃炎</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト_NEW">
            <span class="ガイドライン_NEWマーク">NEW</span>
            <span>ヘリコバクター・ピロリ感染胃炎の診断と治療（</span>
            <span>2013</span>
            <span>）</span>
          </li>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  <span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染の診断と治療のガイドライン2016改訂版（2016）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">保険診療で認められる<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>の感染診断と除菌治療が，2013年2月に<span class="本文_色文字太字">ヘリコバクター・ピロリ感染胃炎</span>にまで拡大された。</li>
        <li class="whatsnew_リスト"><span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染症すべてが除菌治療の適応であるとの基本方針は変わりない。</li>
        <li class="whatsnew_リスト">治療については新たな除菌薬として認可されたカリウムイオン競合型アシッドブロッカー（P-CAB）について言及し，除菌レジメンの選択には薬剤感受性試験を行うことが推奨された。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">①<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染胃炎診断から除菌治療までの流れ</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/04-04-A01.svg"/>
          <p class="図表_引用">（筆者作成）</p>
          <p class="図表_注">・従来の<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>除菌治療の保険適用は，<span class="本文_色文字太字">胃・十二指腸潰瘍</span>，<span class="本文_色文字太字">胃</span><span class="本文_色文字太字">MALT</span><span class="本文_色文字太字">リンパ腫</span>，<span class="本文_色文字太字">特発性血小板減少性紫斑病（</span><span class="本文_色文字太字">ITP</span><span class="本文_色文字太字">）</span>，<span class="本文_色文字太字">早期胃癌に対する内視鏡的治療後胃</span>であったが，2013年2月に<span class="本文_色文字太字">ヘリコバクター・ピロリ感染胃炎</span>が追加された。</p>
          <p class="図表_注">・保険適用上は，<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染胃炎の診断には，胃内視鏡検査により慢性胃炎の所見があることを確認し，かつ，<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>の感染を確認する必要がある。</p>
          <p class="図表_注">・内視鏡検査にて胃癌などの悪性疾患の除外を行う必要がある。</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="2">②<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染診断と除菌治療のアルゴリズム</div>
      <div class="アルゴリズム" order="2">
        <div class="図表" type="A" order="2">
          <img class="width-800" src="./image/04-04-A02.svg"/>
          <p class="図表_引用">（井本一郎，他．<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染の診断法．日ヘリコバクター会誌．2013；Suppl：27．）</p>
          <p class="図表_注">・除菌治療後に除菌判定を行う必要がある。</p>
          <p class="図表_注">・除菌成功後も，胃癌リスクは<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>非感染者よりも高く，定期的な胃癌スクリーニング検査が必要である。</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染に伴い慢性胃炎から萎縮性胃炎に移行していく。この組織学的な胃粘膜の慢性炎症状態が<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染胃炎である。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>に感染すると，基本的に胃炎を生じるため，<span class="本文_色文字太字イタリック">H</span><span class="本文_色文字太字イタリック">. </span><span class="本文_色文字太字イタリック">pylori</span><span class="本文_色文字太字">感染胃炎</span>と<span class="本文_色文字太字イタリック">H</span><span class="本文_色文字太字イタリック">. </span><span class="本文_色文字太字イタリック">pylori</span><span class="本文_色文字太字">感染症</span>は本質的には同じである。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染胃炎を背景として胃癌，消化性潰瘍，胃MALTリンパ腫，胃過形成性ポリープなどの胃内疾患やITP，鉄欠乏性貧血，慢性蕁麻疹などの胃外疾患が発生する。</p>
      <p class="本文_先頭丸数字">❹<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>除菌治療により，胃・十二指腸潰瘍の潰瘍再発抑制，胃MALTリンパ腫の改善，ITPの血小板数増加，早期胃癌に対する内視鏡治療後胃の異時性癌再発予防が認められる。また，除菌治療により胃粘膜萎縮の改善，胃癌の予防，胃過形成性ポリープの消失もしくは縮小も期待できる。</p>
      <p class="本文_先頭丸数字">❺<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染胃炎に対する除菌治療の最大の目的は，胃炎の症状を改善することではなく，胃癌をはじめとする<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>関連疾患の予防，さらには感染経路を絶って新しい感染者を増やさないことである。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶<span class="本文_太字">内視鏡による</span><span class="本文_太字イタリック">H</span><span class="本文_太字イタリック">. </span><span class="本文_太字イタリック">pylori</span><span class="本文_太字">感染胃炎診断</span></p>
      <p>　上部消化管内視鏡検査では，胃炎の所見から<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染を疑うことができる。<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染者，未感染者，除菌後において，それぞれ観察されやすい京都分類の所見が示されている（<span class="本文_図表番号">表</span><span class="本文_図表番号">1</span>）。</p>
      <div class="図表" type="T" order="1">
        <p class="図表_タイトル" type="T" order="1" data-file="04-04-T01.svg">表1　<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染と内視鏡所見</p>
        <img class="width-800" src="./image/04-04-T01.svg"/>
        <p class="図表_注">○：観察されることが多い，×：観察されない，△：観察されることがある</p>
        <p class="図表_引用">（春間　賢（監）．胃炎の京都分類．日本メディカルセンター；2014．p.25-95．）</p>
      </div>
      <p class="本文_先頭丸数字">①<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染胃炎を診断するためには，<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>未感染胃粘膜の特徴を知っておくことが重要である。萎縮・好中球浸潤・腸上皮化生など組織学的胃炎のない胃粘膜の状態であり，内視鏡観察では粘膜上皮下に存在する集合細静脈が規則正しく配列する微小な発赤点，<span class="本文_色文字太字">RAC</span>が胃角～胃体下部小弯を中心に観察されることが多い（<span class="本文_図表番号">図</span><span class="本文_図表番号">1</span>）。また胃底腺ポリープ，塩酸ヘマチン付着，稜線状発赤，隆起型びらんなどの所見を認めることがある。</p>
      <div class="図表" type="F" order="1">
        <img class="width-600" src="./image/04-04-F01.svg"/>
        <p class="図表_タイトル" type="F" order="1" data-file="04-04-F01.svg">図1　RAC</p>
      </div>
      <p class="本文_先頭丸数字">②<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染胃炎で観察されることが多い内視鏡所見は，<span class="本文_色文字太字">萎縮</span>（血管透見像，褪色調粘膜）（<span class="本文_図表番号">図</span><span class="本文_図表番号">2</span><span class="本文_図表番号">-</span><span class="本文_図表番号">A</span>），びまん性発赤，腺窩上皮過形成性ポリープ，黄色腫，腸上皮化生，皺襞腫大・蛇行，白濁粘液，鳥肌（<span class="本文_図表番号">図</span><span class="本文_図表番号">2</span><span class="本文_図表番号">-</span><span class="本文_図表番号">B</span>）などである。</p>
      <div class="図表" type="F" order="2">
        <img class="width-800" src="./image/04-04-F02.svg"/>
        <p class="図表_タイトル" type="F" order="2" data-file="04-04-F02.svg">図2　内視鏡所見</p>
      </div>
      <p class="本文_先頭丸数字">❷<span class="本文_太字イタリック">H</span><span class="本文_太字イタリック">. </span><span class="本文_太字イタリック">pylori</span><span class="本文_太字">感染診断</span></p>
      <p class="本文_先頭丸数字">①内視鏡による生検組織を必要とする検査法（侵襲的検査法）として，1）<span class="本文_色文字太字">迅速ウレアーゼ試験</span>，2）<span class="本文_色文字太字">鏡検法</span>，3）<span class="本文_色文字太字">培養法</span>，内視鏡による生検組織を必要としない検査法（非侵襲的検査法）として，4）<span class="本文_色文字太字">尿素呼気試験</span>，5）<span class="本文_色文字太字">抗</span><span class="本文_色文字太字イタリック">H</span><span class="本文_色文字太字イタリック">. </span><span class="本文_色文字太字イタリック">pylori</span><span class="本文_色文字太字">抗体測定</span>，6）<span class="本文_色文字太字">便中</span><span class="本文_色文字太字イタリック">H</span><span class="本文_色文字太字イタリック">. </span><span class="本文_色文字太字イタリック">pylori</span><span class="本文_色文字太字">抗原測定</span>が挙げられる。</p>
      <p class="本文_先頭丸数字">②侵襲的な検査法は生検検体を用いるため「点」での診断となる。一方，非侵襲的な検査法は胃全体の<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染を反映する「面」での診断法であり，簡便で被験者の受容性が高い。</p>
      <p class="本文_先頭丸数字">③<span class="本文_色文字太字">感染診断</span>は<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>の存在診断であり，感度が重要であるが，それぞれの検査法の診断精度に大きな差はない。単独の方法で完全に診断可能なものはないので，その特徴を理解し，検査法を選択することが重要である。複数の診断法を組み合わせることで，診断精度は向上するが，保険上は制約がある。</p>
      <p class="本文_先頭丸数字">④血清ペプシノゲン（PG）測定は，非侵襲的な血液検査であり，胃粘膜の炎症状態を反映することから，<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染胃炎をはじめとした胃炎の補助診断に用いることができる。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">除菌判定</span></p>
      <p class="本文_先頭丸数字">①除菌治療後の<span class="本文_色文字太字">除菌判定</span>は特異度が重視される。偽陰性であったものを除菌成功と誤って判断しないためである。</p>
      <p class="本文_先頭丸数字">②除菌判定には，尿素呼気試験と糞便中抗原測定が推奨されている。除菌判定時に関しては，ガイドラインでは除菌薬服用終了後4週以降としており，判定までの間隔が長いほど判定精度はより高くなる。</p>
      <p class="本文_先頭丸数字">③尿素呼気試験はPPIにより偽陰性率が上昇するため，2週間以上の休薬後の検査が望ましいとされる。</p>
      <p class="本文_先頭丸数字">④抗<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>抗体は，除菌に成功し抗原がなくなってからも陰性化するまでには長期間を要す。そのため，除菌後6ヵ月以降に除菌前の抗体価に比べ50％以下の抗体価の低下が認められれば，除菌成功の指標としている。</p>
      <p class="本文_先頭丸数字">❹<span class="本文_太字">再感染</span>：日本においては，除菌後の再感染はまれとされているが，定期的な内視鏡検査などにて<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染が疑われる場合には，感染診断を行い再評価する必要がある。再陽性化の多くの症例は，除菌失敗に伴う再燃である。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶<span class="本文_色文字太字イタリック">H</span><span class="本文_色文字太字イタリック">. </span><span class="本文_色文字太字イタリック">pylori</span><span class="本文_色文字太字">除菌治療</span>は，酸分泌抑制薬と2種類の抗菌薬の計3種類を内服する3剤併用療法である。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">初めて除菌の場合（一次除菌）</span>：PPI（ランソプラゾール，オメプラゾール，ラベプラゾール，エソメプラゾールのいずれか）あるいはP-CAB〔ボノプラザン（VPZ）〕＋アモキシシリン（AMPC）750 mg＋クラリスロマイシン（CAM）200 mgまたは400 mgを同時に，1日2回，朝，夕食後に7日間投与する（各PPI，P-CABの用量については<span class="本文_太字">「具体的処方」</span>参照のこと）。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">一次除菌で除菌成功に至らなかった場合の除菌（二次除菌）</span>：PPIのいずれかあるいはP-CAB（VPZ）＋AMPC 750　mg＋メトロニダゾール（MNZ）250 mgを同時に，1日2回，朝，夕食後に7日間投与する。</p>
      <p class="本文_先頭丸数字">❹従来のPPIによる3剤併用療法の除菌率は，それぞれ一次除菌で70～80％前後，二次除菌で90％前後である。新しく保険適用となったP-CAB（VPZ）を使用した場合の除菌率は一次除菌で92.6％，二次除菌で98％という報告もあり，除菌率向上が期待されている。</p>
      <p class="本文_先頭丸数字">❺除菌治療が不成功となる最大の原因は薬剤耐性である。一次除菌で使用されるCAMは，呼吸器科領域などで使用する頻度が高く，CAM耐性の<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>が徐々に増加し，現在ではCAM耐性率は30％を超え，それに伴い除菌率も低下している。そのため，新しいガイドラインでは除菌治療における薬剤の選択は，薬剤感受性試験を行い，最も高い除菌率が期待される組み合わせをすることを推奨している。</p>
      <p class="本文_先頭丸数字">❻副作用は軟便・下痢が多く，味覚異常，肝機能障害を認めることもある。まれではあるが，抗菌薬による薬疹や出血性大腸炎を来すことがあり注意が必要である。また，薬物間相互作用により，吸収や代謝・排泄に影響を与える薬物が多数存在し，併用薬には注意が必要である。</p>
      <p class="本文_先頭丸数字">❼除菌成功後も，胃癌リスクは<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>非感染者よりも高いため，定期的な胃癌スクリーニング検査の必要性を患者に説明することが重要である。また，検査にて<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染診断が当初より陰性であっても，内視鏡的に胃炎があれば，自然除菌や高度萎縮による菌の自然消失を考慮し，やはり定期的な胃癌スクリーニング検査が必要である。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①一次除菌，二次除菌ともに不成功例</p>
      <p class="本文_専門医へのコンサルト">②ペニシリンアレルギー患者</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">　2014年，WHOの下部機関である国際がん研究機関（IARC）が，胃癌予防として<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>除菌による対策を推奨した。2016年には日本ヘリコバクター学会ガイドライン作成委員会より，未成年者から胃癌の発症リスクが高まる世代別に分けて，<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>除菌を中心とした胃癌予防策が提言された。いくつかの施設では中高生への<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>診断・治療が開始されており，今後これらの胃癌予防策が全国に普及することが期待されている。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>感染胃炎の治療は，下記のごとく酸分泌抑制薬と2種類の抗菌薬の3剤併用による除菌である。副作用や除菌不成功になる可能性を十分理解し，患者それぞれに応じた，除菌によるメリット・デメリットを検討し処方する必要がある。服薬指導，アルコール摂取，喫煙，有害事象時の対応に関しても説明しておく必要がある。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶一次除菌</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①次の3剤を朝，夕食後に1週間投与</p>
              <p class="具体的処方_処方例">1）<span class="具体的処方_色文字">タケプロン</span></p>
              <p class="具体的処方_処方例1W下げ">30 mg×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">オメプラール</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×2回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">1）酸分泌抑制薬併用により胃内pHが上がり，<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>の抗菌薬への感受性を高め，抗菌薬の抗菌作用を高める。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">パリエット</span></p>
              <p class="具体的処方_処方例1W下げ">10 mg×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ネキシウム</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">タケキャブ</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×2回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">＋2）<span class="具体的処方_色文字">サワシリン</span></p>
              <p class="具体的処方_処方例1W下げ">750 mg×2回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">＋3）<span class="具体的処方_色文字">クラリス</span></p>
              <p class="具体的処方_処方例1W下げ">200 mgまたは400 mg×2回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">3）<span class="本文_イタリック">クラリス</span>の1日用量400 mgと800 mgの間に除菌率の差はない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②朝，夕食後に1週間投与</p>
              <p class="具体的処方_処方例">1）<span class="具体的処方_色文字">ランサップ</span><span class="具体的処方_色文字">400</span></p>
              <p class="具体的処方_処方例1W下げ">半シート×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ランサップ</span><span class="具体的処方_色文字">800</span></p>
              <p class="具体的処方_処方例1W下げ">半シート×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ラベキュア</span><span class="具体的処方_色文字">400</span></p>
              <p class="具体的処方_処方例1W下げ">半シート×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ラベキュア</span><span class="具体的処方_色文字">800</span></p>
              <p class="具体的処方_処方例1W下げ">半シート×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ボノサップ</span><span class="具体的処方_色文字">400</span></p>
              <p class="具体的処方_処方例1W下げ">半シート×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ボノサップ</span><span class="具体的処方_色文字">800</span></p>
              <p class="具体的処方_処方例1W下げ">半シート×2回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">パック製剤。<span class="本文_イタリック">ランサップ</span>400（半シート）：<span class="本文_イタリック">タケプロン</span>30　mg，<span class="本文_イタリック">アモリン</span>750　mg，<span class="本文_イタリック">クラリス</span>200　mg。<span class="本文_イタリック">ランサップ</span>800（半シート）：<span class="本文_イタリック">ランサップ</span>400の<span class="本文_イタリック">クラリス</span>2倍量製剤。</p>
              <p class="具体的処方_ポイント"><span class="本文_イタリック">ラベキュア</span>400（半シート）：<span class="本文_イタリック">パリエット</span>10　mg，<span class="本文_イタリック">サワシリン</span>750　mg，<span class="本文_イタリック">クラリス</span>200　mg。<span class="本文_イタリック">ラベキュア</span>800（半シート）：<span class="本文_イタリック">ラベキュア</span>400の<span class="本文_イタリック">クラリス</span>2倍量製剤。</p>
              <p class="具体的処方_ポイント"><span class="本文_イタリック">ボノサップ</span>400（半シート）：<span class="本文_イタリック">タケキャブ</span>20　mg，<span class="本文_イタリック">アモリン</span>750　mg，<span class="本文_イタリック">クラリス</span>200　mg。<span class="本文_イタリック">ボノサップ</span>800（半シート）：<span class="本文_イタリック">ボノサップ</span>400の<span class="本文_イタリック">クラリス</span>2倍量製剤。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶二次除菌</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①次の3剤を朝，夕食後に1週間投与</p>
              <p class="具体的処方_処方例">1）<span class="具体的処方_色文字">タケプロン</span></p>
              <p class="具体的処方_処方例1W下げ">30 mg×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">オメプラール</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">パリエット</span></p>
              <p class="具体的処方_処方例1W下げ">10 mg×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ネキシウム</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">タケキャブ</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×2回/日</p>
              <p class="具体的処方_処方例">＋2）<span class="具体的処方_色文字">サワシリン</span></p>
              <p class="具体的処方_処方例1W下げ">750 mg×2回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">＋3）<span class="具体的処方_色文字">フラジール</span></p>
              <p class="具体的処方_処方例1W下げ">250 mg×2回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">3）アルコールの代謝を抑制するため，除菌中のアルコール摂取の禁止を厳しく服薬指導する必要あり。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②朝，夕食後に1週間投与</p>
              <p class="具体的処方_処方例">1）<span class="具体的処方_色文字">ランピオン</span></p>
              <p class="具体的処方_処方例1W下げ">半シート×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ラベファイン</span></p>
              <p class="具体的処方_処方例1W下げ">半シート×2回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ボノピオン</span></p>
              <p class="具体的処方_処方例1W下げ">半シート×2回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">パック製剤。<span class="本文_イタリック">ランピオン</span>（半シート）：<span class="本文_イタリック">タケプロン</span>30　mg，<span class="本文_イタリック">アモリン</span>750　mg，<span class="本文_イタリック">フラジール</span>250　mg。</p>
              <p class="具体的処方_ポイント"><span class="本文_イタリック">ラベファイン</span>（半シート）：<span class="本文_イタリック">パリエット</span>10　mg，<span class="本文_イタリック">サワシリン</span>750　mg，<span class="本文_イタリック">フラジール</span>250　mg。</p>
              <p class="具体的処方_ポイント"><span class="本文_イタリック">ボノピオン</span>（半シート）：<span class="本文_イタリック">タケキャブ</span>20　mg，<span class="本文_イタリック">アモリン</span>750　mg，<span class="本文_イタリック">フラジール</span>250　mg。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>大分大学消化器内科学<span class="著者名">　小川　　竜・村上　和成</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
