<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>13</chapter>
    <chapter-title>耳鼻咽喉科疾患</chapter-title>
    <section>07</section>
    <article-code>13-07</article-code>
    <article-title>メニエール病・良性発作性頭位めまい症</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>7 メニエール病・良性発作性頭位めまい症</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">メニエール病診療ガイドライン2011年版（2011）</li>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  メニエール病難治例の診療指針（2014）</li>
          <li class="ガイドライン_リスト">良性発作性頭位めまい症診療ガイドライン（医師用）（2009）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">メニエール病診断には「メニエール病診断の手引」（1974）が長く用いられてきた。その後の知見が整理され，診断基準の改訂と診療の標準化・普遍化を目指し「メニエール病診療ガイドライン2011年版」がまとめられた。さらに，難治例に対して「メニエール病難治例の診療指針」（2014）が追加された。</li>
        <li class="whatsnew_リスト">良性発作性頭位めまい症（BPPV）の病態は<span class="本文_色文字太字">半規管結石</span>（または<span class="本文_色文字太字">クプラ結石</span>）と考えられ，結石の移動を目的とした<span class="本文_色文字太字">頭位治療</span>（従来，理学療法と表現されていた）について解説され，質疑応答もまとめられた。一方，結石の移動を意図しない非特異的頭位治療についても有効性が述べられている。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">①メニエール病診断の過程</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/13-07-A01.svg"/>
          <p class="図表_引用">（厚生労働省難治性疾患克服研究事業前庭機能異常に関する調査研究班（2008～2010年度）（編）．メニエール病診療ガイドライン2011年版．金原出版；2011．より許諾を得て転載）</p>
          <p class="図表_注">・以前はメニエール病疑い例とされていた蝸牛症状のみ，あるいは前庭症状のみを繰り返す病型が，それぞれ<span class="本文_色文字太字">メニエール病非定型例（蝸牛型）</span>あるいは<span class="本文_色文字太字">非定型例（前庭型）</span>と名称が変更された。いずれも内リンパ水腫の存在が強く疑われ，特に蝸牛型ではメニエール病確実例に移行する例が少なくないので慎重な経過観察が必要である。</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="2">②良性発作性頭位めまい症（BPPV）診断のアルゴリズム</div>
      <div class="アルゴリズム" order="2">
        <div class="図表" type="A" order="2">
          <img class="width-800" src="./image/13-07-A02.svg"/>
          <p class="図表_引用">（筆者作成）</p>
          <p class="図表_注">・頭位誘発性めまい反復という病歴からすぐに診断をつけたくなるが，鑑別すべき重要疾患をきちんと除外することが大切。特に<span class="本文_色文字太字">頸椎異常の既往や可能性は，頭位・頭位変換眼振検査の前に確認が必要</span>である。</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <p>　内耳性めまいのなかで，頻度が高く，ガイドラインの整備も進んでいる2つの代表疾患，メニエール病とBPPVを取り上げ解説する。</p>
      <div class="見出し_中見出し">メニエール病</div>
      <div class="見出し_小見出し">定義</div>
      <p>　特発性（原因を特定できない）内リンパ水腫によって，難聴，耳鳴，耳閉感などの聴覚症状を伴うめまい発作を反復する疾患。</p>
      <div class="見出し_小見出し">診断</div>
      <p>　メニエール病確実例では，難聴，耳鳴，耳閉感などの聴覚症状を伴うめまい発作を反復する。その特徴は以下の診断基準に示すごとくである（<span class="本文_図表番号">表</span><span class="本文_図表番号">1</span>，<span class="本文_図表番号">2</span>）。</p>
      <p class="本文_先頭丸数字">
        <span class="本文_太字">（診断にあたっての注意事項）</span>
      </p>
      <p class="本文_先頭丸数字">❶初回発作時にはめまいを伴う突発性難聴と鑑別できないので発作の反復を確認。</p>
      <p class="本文_先頭丸数字">❷外リンパ瘻，内耳梅毒，聴神経腫瘍，神経血管圧迫症候群などの除外が必要。そのためには，十分な問診，神経学的検査，平衡機能検査，聴力検査，CTやMRIなどを行い，症例によっては経過観察が必要。</p>
      <p class="本文_先頭丸数字">❸感音難聴の確認，聴力変動の評価のために頻回の聴力検査が必要。</p>
      <p class="本文_先頭丸数字">❹グリセロール検査，蝸電図検査など内リンパ水腫推定検査が推奨される。</p>
      <p class="本文_先頭丸数字">❺めまいあるいは聴覚症状の一方しか認めない，前庭型，蝸牛型という非定型例に対してはより慎重な診断ならびに経過観察が必要である。</p>
      <div class="図表" type="T" order="1">
        <p class="図表_タイトル" type="T" order="1" data-file="13-07-T01.svg">表1　めまいの特徴</p>
        <img class="width-800" src="./image/13-07-T01.svg"/>
        <p class="図表_引用">（厚生労働省難治性疾患克服研究事業前庭機能異常に関する調査研究班（2008～2010年度）（編）．メニエール病診療ガイドライン2011年版．金原出版；2011．）</p>
      </div>
      <div class="図表" type="T" order="2">
        <p class="図表_タイトル" type="T" order="2" data-file="13-07-T02.svg">表2　聴覚症状の特徴</p>
        <img class="width-800" src="./image/13-07-T02.svg"/>
        <p class="図表_引用">（厚生労働省難治性疾患克服研究事業前庭機能異常に関する調査研究班（2008～2010年度）（編）．メニエール病診療ガイドライン2011年版．金原出版；2011．）</p>
      </div>
      <div class="見出し_小見出し">治療方針</div>
      <p>　急性期治療とその後の発作予防対策について以下に示す。また，<span class="本文_太字">「具体的処方」</span>も参照されたい。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/13-07-小見出し_矢印.png"/>急性期めまい治療</div>
      <p class="本文_先頭丸数字">❶一般的な急性期めまい治療に準ずる。まず，急性脳血管障害など中枢性めまいの可能性を否定し，その後，抗めまい，鎮吐，鎮静によって症状の緩和を目指す。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">7</span><span class="本文_太字">％炭酸水素ナトリウム点滴静注</span></p>
      <p class="本文_先頭丸数字">①急性期のめまい症状緩和に即効性が期待される。</p>
      <p class="本文_先頭丸数字">②混合しないこと（Caを含む点滴製剤と混合すると炭酸Caが沈殿。アルカリ性のため他の注射薬との配合変化を起こしやすい）。</p>
      <p class="本文_先頭丸数字">③血管痛予防のため緩徐に投与する。</p>
      <p class="本文_先頭丸数字">④多量のNa負荷による高血圧，心不全，腎障害，電解質異常に注意。</p>
      <p class="本文_先頭丸数字">⑤めまいの原因として脳出血が否定されていること（凝固時間延長作用が報告されている）。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">鎮吐薬</span>：ドパミンD<span class="下付き">2</span> 受容体拮抗薬は消化管運動改善と嘔吐中枢の抑制が期待される。第一世代（古典的）抗ヒスタミン薬は悪心・嘔吐抑制，鎮静効果を有する。</p>
      <p class="本文_先頭丸数字">❹<span class="本文_太字">抗不安薬</span></p>
      <p class="本文_先頭丸数字">①ベンゾジアゼピン系薬が使いやすい。不安に伴うめまい症状の増悪を防ぐ。</p>
      <p class="本文_先頭丸数字">②抗コリン作用から急性狭隅角緑内障や重症筋無力症では禁忌。他に中枢神経抑制薬使用例では併用注意。</p>
      <p class="本文_先頭丸数字">❺<span class="本文_太字">抗めまい薬・内耳循環改善薬</span></p>
      <p class="本文_先頭丸数字">①血流改善によって脳や内耳の血流を増やすことにより，めまいの改善が期待される。</p>
      <p class="本文_先頭丸数字">②ジフェニドールは抗コリン作用のため，緑内障，閉塞性呼吸器疾患，イレウス，前立腺肥大では使用を避ける。重篤な腎機能障害では禁忌。</p>
      <p class="本文_先頭丸数字">❻症状が軽減した時点で詳しい診察，検査を追加する。中枢障害の可能性がある場合には画像検査を行い，また，難聴自覚の有無によらず聴力検査を行う。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/13-07-小見出し_矢印.png"/>急性感音難聴治療</div>
      <p class="本文_先頭丸数字">❶突発性難聴などの急性感音難聴治療と原則は同様である。ただし，メニエール病では難聴の程度は軽いことが多い。その場合には，ステロイドは以下に示す一般的な急性感音難聴の投与量の半量で開始，あるいは使わずに経過をみることもある。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">副腎皮質ステロイド</span></p>
      <p class="本文_先頭丸数字">①難聴が高度で突発性難聴治療に準じる場合，リン酸デキサメタゾンナトリウム8 mgまたはプレドニゾロン60 mgで開始，1～2週で漸減し投与終了する。</p>
      <p class="本文_先頭丸数字">②投与前には，糖尿病，消化性潰瘍，高血圧，心不全，精神疾患，骨粗鬆症，血栓塞栓症，結核を含む感染症，緑内障，白内障の病歴聴取が必要。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">ビタミン</span><span class="本文_太字">B</span><span class="本文_太字">12</span><span class="本文_太字">：</span>メコバラミンは末梢神経障害における神経の修復・再生に有用。</p>
      <p class="本文_先頭丸数字">❹<span class="本文_太字">内耳循環改善薬：</span>アデノシン三リン酸は内耳障害に基づくめまい，あるいは耳鳴・難聴に適応。</p>
      <p class="本文_先頭丸数字">❺<span class="本文_太字">浸透圧利尿薬：</span>イソソルビドは内リンパ水腫を軽減。蝸牛症状がめまいに先行する場合，そのタイミングで使用することは有効。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/13-07-小見出し_矢印.png"/>発作予防の保存的治療</div>
      <p class="本文_先頭丸数字">❶メニエール病間歇期の発作予防は重要である。保存的治療法を併用して治療し，それでもコントロールできない難治例では専門医にコンサルトし，<span class="本文_図表番号">図</span><span class="本文_図表番号">1</span>に沿って治療を検討する。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">生活指導</span></p>
      <p class="本文_先頭丸数字">①心身のストレス，過労，睡眠不足を避け，規則正しい生活習慣を指導する。</p>
      <p class="本文_先頭丸数字">②性格的には，几帳面・神経質・勝ち気・完璧主義者・自己抑制型の行動特性が多い傾向があり，発想の転換を促す・悩みの相談をためらわない・娯楽や趣味をもつ・適度な有酸素運動を勧めるなど，生活指導や心理的アプローチも有効である。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_太字">薬物治療：</span>浸透圧利尿薬，内耳循環改善薬，抗不安薬，ビタミンB<span class="下付き">12</span>，漢方薬などが適宜使用される。</p>
      <div class="図表" type="F" order="1">
        <img class="width-800" src="./image/13-07-F01.svg"/>
        <p class="図表_タイトル" type="F" order="1" data-file="13-07-F01.svg">図1　難治例における治療のアウトライン</p>
        <p class="図表_注">GM：ゲンタマイシン</p>
        <p class="図表_引用">（厚生労働省難治性疾患等克服研究事業前庭機能異常に関する調査研究班（2011-2013年度）．メニエール病難治例の診療指針．Equilibrium Res. 2014；<span class="太字">73</span>：80-9.）</p>
      </div>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①聴力検査が施行できない時</p>
      <p class="本文_専門医へのコンサルト">②聴力障害が進行する例</p>
      <p class="本文_専門医へのコンサルト">③蝸牛型あるいは前庭型の非定型例</p>
      <p class="本文_専門医へのコンサルト">④保存的治療が奏効しない難治例</p>
      <p class="本文_専門医へのコンサルト">⑤両側性メニエール病症例</p>
      <div class="見出し_中見出し">良性発作性頭位めまい症（BPPV）</div>
      <div class="見出し_小見出し">定義</div>
      <p>　BPPVは特定頭位で誘発されるめまい（頭位誘発性めまい）を主徴とし，これに随伴する眼振を特徴とする内耳疾患。末梢性めまい疾患のなかで最も高率に認められる。</p>
      <div class="見出し_小見出し">診断</div>
      <p>　症状の特徴と典型的な眼振所見からBPPVの診断をつける（<span class="本文_図表番号">表</span><span class="本文_図表番号">3</span>）。さらに，どの半規管に病変があるかを推測する（<span class="本文_図表番号">表</span><span class="本文_図表番号">4</span>）。</p>
      <div class="図表" type="T" order="3">
        <p class="図表_タイトル" type="T" order="3" data-file="13-07-T03.svg">表3　BPPVの症状の特徴</p>
        <img class="width-700" src="./image/13-07-T03.svg"/>
        <p class="図表_引用">（厚生労働省難治性疾患克服研究事業前庭機能異常に関する調査研究班（2008～2010年度）（編）．メニエール病診療ガイドライン2011年版．金原出版；2011．より抜粋）</p>
      </div>
      <div class="図表" type="T" order="4">
        <p class="図表_タイトル" type="T" order="4" data-file="13-07-T04.svg">表4　BPPVの典型的な眼振所見</p>
        <img class="width-700" src="./image/13-07-T04.svg"/>
      </div>
      <div class="図表" type="F" order="2">
        <img class="width-800" src="./image/13-07-F02.svg"/>
        <p class="図表_タイトル" type="F" order="2" data-file="13-07-F02.svg">図2　BPPVの典型的な眼振所見</p>
      </div>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶一般にめまい治療の基本は薬物療法である。BPPVでは自然治癒例も少なくないので抗めまい薬で症状を抑えつつ自然軽快を待つことも可能である。しかし，結石症を来している半規管が正しく診断できれば，原因となる結石の移動を目的として頭位治療が可能であり，多くの症例で病悩期間の短縮に有用である。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_色文字太字">後半規管型</span><span class="本文_色文字太字">BPPV</span>に対して多用されている頭位治療はEpley法である。運動の手順は，①坐位→②患側懸垂頭位→③健側懸垂頭位→④懸垂頭位を維持したまま体全体を健側が下になるように回転→⑤坐位に戻し，頭部は前屈を保つ。それぞれの姿勢で眼振が消失するのを確認（あるいは2分程度維持）しつつ進める。運動速度が比較的低速なので患者負担が少なく，かつ治療効果も高い。</p>
      <p class="本文_先頭丸数字">❸非特異的頭位治療として，特に結石の移動にかかわらず頭部運動を反復することでBPPVが軽快，消失する場合がある。具体的な運動は規定されていないので，ベッド上でゆっくり頭位変換を行う程度の負荷の軽いものから症例に応じた平衡訓練を指導する。</p>
      <p class="本文_先頭丸数字">❹頭位治療が困難な症例（頸椎疾患など）あるいは奏効しない症例では，薬物療法を行い治癒までの期間の症状を軽減することが可能である。</p>
      <p class="本文_先頭丸数字">❺<span class="本文_太字">抗めまい薬・内耳循環改善薬：</span>血流改善によって脳や内耳の血流を増やすことにより，めまいの改善が期待される。</p>
      <p class="本文_先頭丸数字">❻<img class="" src="./image/13-07-4.png"/>BPPVは，本来，卵形囊にあるべき耳石が半規管に迷入して起こると考えられるものであるから，その迷入は重力によって起こり，坐位でも臥位でも最も低い位置にある後半気管に起こりやすい。実臨床の頻度でも後半規管型が多数を占める。ただし，水平半規管型もまれではなく存在し，頭位・頭位変換眼振検査で正確に眼振を確認しなければ診断できない。特に水平半規管型では患側の推定が容易ではない。頭位により生じるクプラの傾きが左右で対称であっても，刺激性の方向に動いた時により強い眼振がみられることから，半規管結石症ではより強い眼振が出る方向が患側であり，一方，クプラ結石症ではより強い眼振が出る方向の逆が患側となる。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①頸部に何らかの病変を有する症例</p>
      <p class="本文_専門医へのコンサルト">②水平半規管型BPPV</p>
      <p class="本文_専門医へのコンサルト">③眼振所見が典型的でない症例および難治症例</p>
      <p class="本文_専門医へのコンサルト">　<span class="本文_色文字太字">BPPV</span><span class="本文_色文字太字">に類似する頭位誘発性めまいを来す中枢性疾患もあることを念頭に置く</span>。</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題"/>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針見出し">メニエール病</p>
              <p class="具体的処方_治療方針">【治療方針】発作の急性期治療と発作予防を分けて考える。急性期は一般的な急性期めまい治療に準じる。発作予防では心身のストレスを回避する生活指導や心理的アプローチに加えて薬物療法が行われる。薬物療法が無効な時（難治例），より侵襲の強い治療法が考えられるので専門医に紹介する。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶メニエール病</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）急性期めまい</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">　</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">生理食塩水</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ソリ</span><span class="具体的処方_色文字">タ</span><span class="具体的処方_色文字">-</span><span class="具体的処方_色文字">T</span><span class="具体的処方_色文字">1</span><span class="具体的処方_色文字">号</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ソリ</span><span class="具体的処方_色文字">タ</span><span class="具体的処方_色文字">-</span><span class="具体的処方_色文字">T</span><span class="具体的処方_色文字">3</span><span class="具体的処方_色文字">号</span></p>
              <p class="具体的処方_処方例1W下げ">500 mL，点滴静注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">　</p>
              <p class="具体的処方_ポイント">①必要に応じて補液。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">メイロン</span><span class="具体的処方_色文字">7</span><span class="具体的処方_色文字">％</span></p>
              <p class="具体的処方_処方例1W下げ">40～250 mL，緩徐に点滴静注（一部側管注）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②アルカローシス，テタニーに注意。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">プリンペラン</span></p>
              <p class="具体的処方_処方例1W下げ">10 mg/回，静注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③ドパミンD<span class="下付き">2</span> 受容体拮抗薬。消化管出血，穿孔，イレウスは禁忌。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">トラベルミン</span></p>
              <p class="具体的処方_処方例1W下げ">1錠/回</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④第一世代（古典的）抗ヒスタミン薬。抗コリン作用が強く，緑内障，前立腺肥大などで禁忌。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">⑤<span class="具体的処方_色文字">セルシン</span></p>
              <p class="具体的処方_処方例1W下げ">5 mg/回，筋注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">⑤緑内障では避ける。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">⑥<span class="具体的処方_色文字">メリスロン</span></p>
              <p class="具体的処方_処方例1W下げ">6 mg×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">⑥ヒスタミン類似作用があるため，消化性潰瘍，喘息では避ける。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）急性感音難聴</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プレドニン</span></p>
              <p class="具体的処方_処方例1W下げ">30～60 mg/日から漸減投与</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①糖尿病では投与可否を専門医に相談。胃保護のためH<span class="下付き">2</span> ブロッカーなど併用。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">メチコバール</span></p>
              <p class="具体的処方_処方例1W下げ">0.5 mg×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②ビタミンB<span class="下付き">12</span>。大量のビタミンCと同時に摂取すると分解されることがある。ビタミンCのサプリメントとは1時間以上あけて摂取する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">アデホス顆粒</span></p>
              <p class="具体的処方_処方例1W下げ">1 g×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③脳出血直後は禁忌。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">イソバイド</span></p>
              <p class="具体的処方_処方例1W下げ">30 mL×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④急性頭蓋内血腫では禁忌。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅲ）めまい間歇期</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">イソバイド</span></p>
              <p class="具体的処方_処方例1W下げ">30 mL，頓用</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①長期連用は避ける。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">アデホス顆粒</span></p>
              <p class="具体的処方_処方例1W下げ">1 g×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②内耳循環改善効果。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">リーゼ</span></p>
              <p class="具体的処方_処方例1W下げ">5 mg×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③ベンゾジアゼピン系。低力価，中期作用型。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">メチコバール</span></p>
              <p class="具体的処方_処方例1W下げ">0.5 mg×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④ビタミンCと併用しない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">⑤<span class="具体的処方_色文字">半夏白朮天麻湯</span></p>
              <p class="具体的処方_処方例1W下げ">2.5 g×2～3回/日，食前または食間</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">⑤漢方薬。</p>
            </td>
          </tr>
        </tbody>
      </table>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針見出し">BPPV</p>
              <p class="具体的処方_治療方針">【治療方針】病悩期間短縮を目的に頭位治療が可能であれば施行し，必要であれば薬物療法も併用する。それぞれ，内耳循環改善効果などで治療効果が期待される。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">BPPV</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">アデホス顆粒</span></p>
              <p class="具体的処方_処方例1W下げ">1 g×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①脳出血直後は禁忌。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">メリスロン</span></p>
              <p class="具体的処方_処方例1W下げ">6 mg×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②ヒスタミン類似作用があるため，消化性潰瘍，喘息には避ける。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">セファドール</span></p>
              <p class="具体的処方_処方例1W下げ">25 mg×3回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③抗コリン作用のため，緑内障，閉塞性呼吸器疾患，イレウス，前立腺肥大では使用を避ける。重篤な腎機能障害では禁忌。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>筑波大学医学医療系耳鼻咽喉科・頭頸部外科<span class="著者名">　和田　哲郎</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
