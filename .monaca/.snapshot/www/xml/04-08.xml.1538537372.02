<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>04</chapter>
    <chapter-title>消化管疾患</chapter-title>
    <section>08</section>
    <article-code>04-08</article-code>
    <article-title>便秘</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>8 便秘</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  慢性便秘症診療ガイドライン2017（2017）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">わが国初の慢性便秘症診療ガイドラインが，日本消化器病学会関連研究会慢性便秘の診断・治療研究会より発刊された。</li>
        <li class="whatsnew_リスト">新薬としてリナクロチド（<span class="本文_イタリック">リンゼス</span>）（保険適用は便秘型過敏性腸症候群）とナルデメジントシル酸塩（<span class="本文_イタリック">スインプロイク</span>）（保険適用はオピオイド誘発性便秘症）が登場した。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">慢性便秘症の診療アルゴリズム</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/04-08-A01.svg"/>
          <p class="図表_注">＊：刺激性下剤，グリセリン浣腸，ビサコジル坐剤，レシカルボン坐剤など，＊＊：保険適用は便秘型過敏性腸症候群</p>
          <p class="図表_注">BSFS：Bristol便形状スケール</p>
          <p class="図表_引用">（筆者作成）</p>
          <p class="図表_注">・今回のガイドラインでは見送られたためあくまで私案を提示する。</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶本来体外に排出すべき糞便を，十分量かつ快適に排出できない状態。</p>
      <p class="本文_先頭丸数字">❷国際的には，改訂されたRome Ⅳの診断基準で<span class="本文_図表番号">表</span><span class="本文_図表番号">1</span>のように定義されている。</p>
      <div class="図表" type="T" order="1">
        <p class="図表_タイトル" type="T" order="1" data-file="04-08-T01.svg">表1　慢性便秘症の診断基準</p>
        <img class="width-400" src="./image/04-08-T01.svg"/>
        <p class="図表_引用">（Lacy BE, et al. Gastroenterology. 2016；<span class="太字">150</span>：1393-407.より改変）</p>
      </div>
      <p class="本文_先頭丸数字">❸<span class="下線">慢性便秘の分類</span>：慢性便秘には大腸癌などの器質性疾患，薬剤性や症候性など鑑別を要する2次性の便秘が多くあり（<span class="本文_図表番号">図</span><span class="本文_図表番号">1</span>），その鑑別には注意を要する。また近年，オピオイドによる便秘は<span class="本文_色文字太字">オピオイド誘発性便秘</span>（OIC）として別途分類されることが多い。</p>
      <div class="図表" type="F" order="1">
        <img class="width-800" src="./image/04-08-F01.svg"/>
        <p class="図表_タイトル" type="F" order="1" data-file="04-08-F01.svg">図1　慢性便秘の分類</p>
        <p class="図表_引用">（Lembo A, et al. N Engl J Med. 2003；<span class="太字">349</span>：1360-8.より改変）</p>
      </div>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶実際の日常臨床では，慢性便秘症と診断するのにRome Ⅳの診断基準を満たす必要はなく，<span class="本文_色文字太字">「本来体外に排出すべき糞便を十分量かつ快適に排出できない状態」が続くために日常生活に支障が出ていれば</span>，便秘症と診断して治療することが望ましい（<span class="本文_図表番号">図</span><span class="本文_図表番号">2</span>）。</p>
      <p class="本文_先頭丸数字">❷多くの患者は，排便回数よりも排便困難感を訴える。</p>
      <p class="本文_先頭丸数字">❸通常の診療では問診のみで診断をつけることで問題ないが，警告兆候（発熱，関節痛，血便，6ヵ月以内の予期せぬ3 kg以上の体重減少，腹部腫瘤などの異常な身体所見）があるときや50歳以上での発症では，大腸癌や炎症性腸疾患などの器質的疾患の検索を行う。特に最近急に発症した際，便秘症治療に抵抗性の場合は要注意である。</p>
      <p class="本文_先頭丸数字">❹慢性機能性便秘症の診断は過敏性腸症候群（IBS）の除外が前提になっており，実際腹部症状の前面に出る慢性機能性便秘症患者では同一患者でのIBSとの相互移行などがあり鑑別は難しいことがあるが，実地臨床上厳密に区別する必要性は低い。</p>
      <div class="図表" type="F" order="2">
        <img class="width-400" src="./image/04-08-F02.svg"/>
        <p class="図表_タイトル" type="F" order="2" data-file="04-08-F02.svg">図2　慢性便秘症の臨床診断</p>
      </div>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶慢性便秘症治療には保存的治療と外科的治療が用いられる。保存的治療には食習慣を含む生活習慣の改善や排便環境の整備，適切な排便姿勢の教育，摘便などの理学的治療，薬物治療が用いられ，薬物治療には数種類の異なった作用機序の薬剤，あるいは漢方薬が用いられている。</p>
      <p class="本文_先頭丸数字">❷保存的治療では病状の改善を認めず，便秘の病態評価により適応がある場合は外科的治療が行われる。</p>
      <p class="本文_先頭丸数字">❸治療のゴールは主観的には<span class="本文_色文字太字">排便困難症状の患者満足度</span>に加え客観的には<span class="本文_図表番号">図</span><span class="本文_図表番号">3</span>に示す<span class="本文_色文字太字">BSFS</span><span class="本文_色文字太字">でのタイプ</span><span class="本文_色文字太字">4</span>の達成である。</p>
      <div class="図表" type="F" order="3">
        <img class="width-400" src="./image/04-08-F03.svg"/>
        <p class="図表_タイトル" type="F" order="3" data-file="04-08-F03.svg">図3　Bristol便形状スケール（BSFS）</p>
      </div>
      <div class="見出し_小見出し">治療方法</div>
      <p class="本文_先頭丸数字">❶治療の基本は食事・運動・排便環境整備である。食事量を減らしたり，欠食することは極力避けること，また十分な水分摂取と食物繊維を1日約20 g程度摂取することが推奨される（20 gとは約キャベツ1個分）。</p>
      <p class="本文_先頭丸数字">❷薬物療法の成否は，<span class="本文_色文字太字">排便環境の整備</span>次第といっても過言ではない。</p>
      <p class="本文_先頭丸数字">❸薬物治療の基本は緩下剤（酸化マグネシウム，ルビプロストン，リナクロチドなど）はregular use，刺激性下剤はあくまでon demandに限って使用することが基本である。</p>
      <p class="本文_先頭丸数字">❹酸化マグネシウムを投与する場合は，定期的な血清マグネシウム濃度の測定が推奨される。腎機能異常者では注意を要するため，投与量の減量や中止をしなければならない。</p>
      <p class="本文_先頭丸数字">❺ルビプロストンは投与初期の悪心に注意する。</p>
      <p class="本文_先頭丸数字">❻浣腸や坐剤なども有効でる。</p>
      <p class="本文_先頭丸数字">❼治療のゴールは残便感のない排便（完全排便）で，BSFSでのタイプ4（<span class="本文_図表番号">図</span><span class="本文_図表番号">3</span>）を目指す。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①通常の緩下剤・刺激性下剤などで治療困難例</p>
      <p class="本文_専門医へのコンサルト">②緩下剤で軟便ないしは水様便になっていても，努責などの排便困難症状が強く残る場合</p>
      <p class="本文_専門医へのコンサルト">③刺激性下剤の濫用・依存性の高い患者</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">①新薬としてリナクロチド（<span class="本文_イタリック">リンゼス</span>）（保険適用は便秘型過敏性腸症候群）とナルデメジントシル酸塩（<span class="本文_イタリック">スインプロイク</span>）（適用はオピオイド誘発性便秘症）が登場した。</p>
      <p class="本文_最近の話題">②機能性消化管疾患の診断基準であるRome Ⅲが改訂されRome Ⅳが制定された。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】薬物治療の基本は緩下剤〔酸化マグネシウム，ルビプロストン（<span class="本文_イタリック">アミティーザ</span>），リナクロチド（<span class="本文_イタリック">リンゼス</span>）など〕はregular use，刺激性下剤は緩下剤に上乗せしてあくまでon demandに限って使用することが基本である。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶緩下剤</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">酸化マグネシウム</span></p>
              <p class="具体的処方_処方例1W下げ">2 g/日，3回に分服，毎食後</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">アミティーザ</span></p>
              <p class="具体的処方_処方例1W下げ">24 μg×2回/日，朝夕食直後</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①高マグネシウム血症が表れることがあるので，定期的に血清マグネシウム濃度を測定する。</p>
              <p class="具体的処方_ポイント">②投与初期の悪心に注意。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">リンゼス</span></p>
              <p class="具体的処方_処方例1W下げ">0.5 mg×1回/日，朝食前</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③保険適用は便秘型過敏性腸症候群。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶刺激性下剤</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プルゼニド</span></p>
              <p class="具体的処方_処方例1W下げ">12～24 mg×1回/日，就寝前または頓用</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">アローゼン</span></p>
              <p class="具体的処方_処方例1W下げ">0.5～1.0 g×1～2回/日，就寝前または頓用</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶漢方薬</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">大黄甘草湯</span></p>
              <p class="具体的処方_処方例1W下げ">7.5 g/日，3回に分服</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①作用は強力，甘草が含まれているため電解質異常に注意。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">麻子仁丸</span></p>
              <p class="具体的処方_処方例1W下げ">7.5 g/日，3回に分服</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②甘草が含まれておらず高齢者でも安心して使える。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">潤腸湯</span></p>
              <p class="具体的処方_処方例1W下げ">7.5 g/日，3回に分服</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③甘草が含まれているため電解質異常に注意。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">大建中湯</span></p>
              <p class="具体的処方_処方例1W下げ">15 g/日，3回に分服</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④腹部膨満や腹痛，腸閉塞の予防。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>横浜市立大学大学院肝胆膵消化器病学<span class="著者名">　中島　　淳</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
