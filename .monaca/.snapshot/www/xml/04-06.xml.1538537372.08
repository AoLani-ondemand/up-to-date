<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>04</chapter>
    <chapter-title>消化管疾患</chapter-title>
    <section>06</section>
    <article-code>04-06</article-code>
    <article-title>消化性潰瘍</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>6 消化性潰瘍</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">消化性潰瘍診療ガイドライン2015改訂第2版（2015）</li>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  消化性潰瘍診療ガイドライン2015改訂第2版（追補版）（2016，2017）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">除菌成功後における未治癒潰瘍の頻度と対策が加えられた。頻度は0～30％程度で，抗潰瘍薬投与およびNSAIDsの中止が推奨された。</li>
        <li class="whatsnew_リスト">NSAIDs潰瘍の発生時期は投与3ヵ月以内が多いことが示された。</li>
        <li class="whatsnew_リスト">非<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>・非NSAIDs潰瘍の頻度は，胃潰瘍全体の1～5％，十二指腸潰瘍全体の2％以下であることが示された。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">胃潰瘍の臨床診断のフローチャート</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/04-06-A01.svg"/>
          <p class="図表_注">＊1：禁忌である。中止不能のため，やむを得ず投与する場合。</p>
          <p class="図表_注">＊2：LDA潰瘍はPPIを選択。</p>
          <p class="図表_注">IVR：interventional radiology，NSAIDs：非ステロイド性抗炎症薬，PPI：プロトンポンプ阻害薬，PG：プロスタグランジン，H<span class="下付き">2</span>RA：H<span class="下付き">2</span> 受容体拮抗薬，LDA：低用量アスピリン</p>
          <p class="図表_引用">（「日本消化器病学会 編：消化性潰瘍診療ガイドライン2015改訂第2版（追補版），xvii，2015，南江堂」より許諾を得て転載）</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">疫学</div>
      <p class="本文_先頭丸数字">❶厚生労働省の傷病別年次推移のデータでは，<span class="本文_色文字太字">胃潰瘍，十二指腸潰瘍ともに急速に減少している</span>。</p>
      <p class="本文_先頭丸数字">❷胃潰瘍，十二指腸潰瘍の死亡者数は1950年からは大きく減少したが，1990年前後からは例年3000人程度で推移している<span>1</span><span>）</span>。</p>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶ヘリコバクター・ピロリ（<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>）とNSAIDsは，消化性潰瘍の2大原因とされている。</p>
      <p class="本文_先頭丸数字">①<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>は1983年にオーストラリアのWarrenとMarshallにより発見された細菌である。</p>
      <p class="本文_先頭丸数字">②<span class="本文_色文字太字">近年は特に若年者での</span><span class="本文_色文字太字イタリック">H</span><span class="本文_色文字太字イタリック">. </span><span class="本文_色文字太字イタリック">pylori</span><span class="本文_色文字太字">感染が急速に低下している</span>。</p>
      <p class="本文_先頭丸数字">❷高齢化の加速とともに，抗血小板薬として使用されている<span class="本文_色文字太字">LDA</span><span class="本文_色文字太字">による潰瘍が増加している</span>。</p>
      <p class="本文_先頭丸数字">❸NSAIDs以外の潰瘍発生リスクを高める薬物を<span class="本文_図表番号">表</span><span class="本文_図表番号">1</span>に示す。</p>
      <div class="図表" type="T" order="1">
        <p class="図表_タイトル" type="T" order="1" data-file="04-06-T01.svg">表1　潰瘍発生リスクを高める薬物（NSAIDsを除く）</p>
        <img class="width-400" src="./image/04-06-T01.svg"/>
        <p class="図表_引用">（日本消化器病学会（編）．消化性潰瘍診療ガイドライン2015改訂第2版．南江堂；2015．より作成）</p>
      </div>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶内視鏡検査が第一選択である。</p>
      <p class="本文_先頭丸数字">❷以下の症状に留意する。</p>
      <p class="本文_先頭丸数字">①胃潰瘍では食後の心窩部痛，十二指腸潰瘍では空腹時痛が多い。</p>
      <p class="本文_先頭丸数字">②出血性潰瘍では，吐血，下血（タール便）がみられる。</p>
      <p class="本文_先頭丸数字">③貧血により，ふらつきが主訴となることもある。</p>
      <p class="本文_先頭丸数字">④NSAIDs，LDA起因性潰瘍では腹部症状が乏しく，吐血，下血，貧血などで発症する例が多い。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶前述の<span class="本文_図表番号">アルゴリズム</span>が基本となる。</p>
      <p class="本文_先頭丸数字">①第一に穿孔，出血などの合併症の有無を確認し，穿孔があれば手術，出血があれば内視鏡的止血などの止血処置を行う。</p>
      <p class="本文_先頭丸数字">②次に，通常の潰瘍治療ではNSAIDsの使用の有無を評価する。NSAIDs使用中で継続が必要な場合はPPIを中心とした治療を行い，<span class="本文_色文字太字">非</span><span class="本文_色文字太字">NSAIDs</span><span class="本文_色文字太字">潰瘍で</span><span class="本文_色文字太字イタリック">H</span><span class="本文_色文字太字イタリック">. </span><span class="本文_色文字太字イタリック">pylori</span><span class="本文_色文字太字">陽性であれば除菌を行う</span>。</p>
      <p class="本文_先頭丸数字">③除菌を行わない，もしくは除菌適応でない潰瘍に対しては，PPI，H<span class="下付き">2</span>RAにて治療を行い，再発予防のために維持療法が必要である。</p>
      <p class="本文_先頭丸数字">❷本ガイドラインでは抗凝固薬・抗血小板薬服用中の出血性潰瘍への対応が追加され，休薬による血栓症リスク増加への注意喚起がなされた。</p>
      <p class="本文_先頭丸数字">①<span class="本文_色文字太字">抗血小板薬を中止することによって血栓症イベントの発生リスクが高い症例では，抗血小板薬を休薬しない</span>ことが推奨された。</p>
      <p class="本文_先頭丸数字">②抗凝固薬を服用中の患者は服薬中止によって血栓症イベントの発生リスクが高くなるので，ヘパリン置換や止血確認後早期に抗凝固薬服薬を再開することが提案された。</p>
      <div class="見出し_小見出し">治療方法</div>
      <p class="本文_先頭丸数字">❶<span class="本文_色文字太字">出血性潰瘍では，内視鏡的止血を行う</span>。</p>
      <p class="本文_先頭丸数字">①内視鏡的止血が奏効しなければ，血管塞栓術（IVR）や手術を考慮する。</p>
      <p class="本文_先頭丸数字">②内視鏡的止血後に酸分泌抑制薬（H<span class="下付き">2</span>RAやPPI）の静脈内投与を行う。</p>
      <p class="本文_先頭丸数字">③出血性潰瘍の急性期（一般的には48時間以内）は絶食とし，食事の再開は止血確認後とする。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_色文字太字イタリック">H</span><span class="本文_色文字太字イタリック">. </span><span class="本文_色文字太字イタリック">pylori</span><span class="本文_色文字太字">除菌は潰瘍再発を抑制するため，推奨される</span>。</p>
      <p class="本文_先頭丸数字">①除菌（一次除菌）のレジメンは3剤療法で，PPI，アモキシシリン（AMPC）およびクラリスロマイシン（CAM）の組み合わせで実施する。<span class="本文_色文字太字">除菌判定は除菌終了後</span><span class="本文_色文字太字">4</span><span class="本文_色文字太字">週以降に尿素呼気試験およびモノクローナル抗体を用いた便中抗原検査で行う</span>。</p>
      <p class="本文_先頭丸数字">②<span class="本文_色文字太字">PPI</span><span class="本文_色文字太字">投与中は除菌判定が偽陰性となるので，判定の</span><span class="本文_色文字太字">2</span><span class="本文_色文字太字">週前から中止する</span>。</p>
      <p class="本文_先頭丸数字">③一次除菌失敗例では二次除菌を行う。レジメンはPPI，AMPC，メトロニダゾール（MNZ）を用いる。</p>
      <p class="本文_先頭丸数字">④一次除菌失敗の主な原因はCAMの耐性であり，　近年増加している。</p>
      <p class="本文_先頭丸数字">❸NSAIDs，抗血栓薬内服中の潰瘍発症例で，これらの薬の継続投与が必要な場合，潰瘍再発予防にPPIの併用が有用である。</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">　2016年11月の追補版では，2015年2月から使用可能となったカリウムイオン競合型アシッドブロッカー（P-CAB）ボノプラザンが追記された。本剤は新規の酸分泌抑制薬であり，従来のPPIより即効性で持続時間が長く，酸性環境でも安定，さらに肝薬物代謝酵素CYP2C19遺伝子多型の影響を受けにくい。本剤により<span class="本文_イタリック">H</span><span class="本文_イタリック">. </span><span class="本文_イタリック">pylori</span>の一次除菌率は70％から90％へと上昇した。しかし，長期投与時には，酸分泌抑制のネガティブフィードバックによる高ガストリン血症に注意する必要がある。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶急性期出血性潰瘍</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">オメプラ</span><span class="具体的処方_色文字">－</span><span class="具体的処方_色文字">ル注用</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×2回/日</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">タケプロン静注用</span></p>
              <p class="具体的処方_処方例1W下げ">30 mg×2回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">絶食の期間中に用い，内服が可能となった時点で同系統の経口薬に切り替える。注射薬の投与期間は7日以内とする。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">ガスター注射液</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×2回/日</p>
              <p class="具体的処方_処方例">生理食塩水100 mLまたは5％ブドウ糖注射液に混合して点滴静注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③腎障害時に減量が必要である。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字太字イタリック">H</span>
                <span class="具体的処方_色文字太字イタリック">. </span>
                <span class="具体的処方_色文字太字イタリック">pylori</span>
                <span class="具体的処方_色文字">除菌治療</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）一次除菌</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ボノサップパック</span><span class="具体的処方_色文字">400</span><span class="具体的処方_色文字">／</span><span class="具体的処方_色文字">800</span></p>
              <p class="具体的処方_処方例1W下げ">1シート/日，朝夕食後に分服，7日間</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①～③除菌薬はアドヒアランス維持のため，PPI（P-CAB）＋AMPC＋CAM（二次除菌ではMNZ）のパック製剤が繁用されている。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）二次除菌（一次除菌不成功例）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ボノピオンパック</span></p>
              <p class="具体的処方_処方例1W下げ">1シート/日，朝夕食後に分服，7日間</p>
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">ラベファインパック</span></p>
              <p class="具体的処方_処方例1W下げ">1シート/日，朝夕食後に分服，7日間</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">一次除菌の成功率は70～90％，二次除菌の成功率は80～90％であり，P-CABでの除菌率が高い。</p>
              <p class="具体的処方_ポイント">腸内細菌叢の変化による下痢，軟便，味覚異常などの副作用がある。ペニシリンアレルギー症例では投与できない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶抗潰瘍療法</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①～③のいずれかを胃潰瘍は8週間，十二指腸潰瘍は6週間まで投与</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ネキシウム</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×1回/日，朝食後</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">パリエット</span></p>
              <p class="具体的処方_処方例1W下げ">10 mg×1回/日，朝食後</p>
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">タケキャブ</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×1回/日，朝食後</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">PPIは肝薬物代謝酵素CYP2C19の遺伝子多型により効果に個人差が生じる。さらに同様の代謝経路を有するワルファリン，クロピドグレル，テオフィリン，タクロリムスの血中濃度に影響することがあり，注意が必要である。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">NSAIDs</span>
                <span class="具体的処方_色文字">起因性潰瘍</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">NSAIDs中断後，前述の抗潰瘍療法を行う。</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>筑波大学附属病院光学医療診療部／筑波大学医学医療系消化器内科<span class="上付き">＊</span>　<span class="著者名">溝上　裕士・鈴木　英雄・奈良坂俊明・金子　　剛</span><span class="上付き">＊</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
