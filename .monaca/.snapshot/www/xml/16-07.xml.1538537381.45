<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>16</chapter>
    <chapter-title>小児科疾患</chapter-title>
    <section>07</section>
    <article-code>16-07</article-code>
    <article-title>小児ネフローゼ症候群</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>7 小児ネフローゼ症候群</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">小児特発性ネフローゼ症候群診療ガイドライン2013（2013）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">ネフローゼ症候群の診療経過，病型分類に対応した薬物療法フローチャートが提示されている。</li>
        <li class="whatsnew_リスト">小児特発性ネフローゼ症候群の初期治療は経口ステロイドであり，投与方法は国際法（8週間投与）が推奨される。</li>
        <li class="whatsnew_リスト">ネフローゼ症候群の一般療法（食事療法，運動制限，ステロイド副作用の対応策，予防接種など）についての最新のエビデンスも解説している。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">小児特発性ネフローゼ症候群の臨床経過と薬物療法</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/16-07-A01.svg"/>
          <p class="図表_引用">（日本小児腎臓病学会（編）．小児特発性ネフローゼ症候群診療ガイドライン2013．診断と治療社；2013．より作成）</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶ネフローゼ症候群は糸球体基底膜障害の結果，<span class="本文_色文字太字">高度蛋白尿</span>，<span class="本文_色文字太字">低蛋白血症</span>と全身性の浮腫が起こる病態である。</p>
      <p class="本文_先頭丸数字">❷小児ネフローゼ症候群の<span class="本文_色文字太字">約</span><span class="本文_色文字太字">90</span><span class="本文_色文字太字">％は原因</span><span class="本文_色文字太字">不明の特発性ネフローゼ症候群</span>である。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶小児ネフローゼ症候群の診断には以下の定義を用いる。</p>
      <p class="本文_先頭丸数字">①<span class="下線">ネフローゼ症候群</span>：高度蛋白尿（夜間蓄尿で40 mg/時/m<span class="上付き">2</span> 以上）または早朝尿で尿蛋白/Cr比2.0 g/gCr以上，かつ低アルブミン血症（血清アルブミン2.5 g/dL以下）。</p>
      <p class="本文_先頭丸数字">②<span class="下線">完全寛解</span>：試験紙法で早朝尿蛋白陰性を3日連続して示すもの，または早朝尿で尿蛋白/Cr比0.2 g/gCr未満を3日連続して示すもの。</p>
      <p class="本文_先頭丸数字">③<span class="下線">再発</span>：試験紙法で早朝尿蛋白3＋以上を3日連続して示すもの。</p>
      <p class="本文_先頭丸数字">④<span class="下線">ステロイド感受性</span>：プレドニゾロン（PSL）連日投与開始後4週間以内に完全寛解するもの。</p>
      <p class="本文_先頭丸数字">⑤<span class="下線">頻回再発</span>：初回寛解後6ヵ月以内に2回以上再発，または任意の12ヵ月以内に4回以上再発したもの。</p>
      <p class="本文_先頭丸数字">⑥<span class="下線">ステロイド依存性</span>：PSL治療中またはPSL中止後14日以内に2回連続して再発したもの。</p>
      <p class="本文_先頭丸数字">⑦<span class="下線">ステロイド抵抗性</span>：PSLを4週間以上連日投与しても，完全寛解しないもの。</p>
      <p class="本文_先頭丸数字">1）<span class="下線">Initial</span><span class="下線"> </span><span class="下線">nonresponder</span>：ネフローゼ症候群初発時にステロイド抵抗性になったもの。</p>
      <p class="本文_先頭丸数字">2）<span class="下線">Late</span><span class="下線"> </span><span class="下線">responder</span>：以前ステロイド感受性だったものがステロイド抵抗性になったもの。</p>
      <p class="本文_先頭丸数字">⑧<span class="下線">難治性ネフローゼ症候群</span>：ステロイド感受性のうち，標準的な免疫抑制薬治療では寛解を維持できず，頻回再発型やステロイド依存性のままで，ステロイド薬から離脱できないもの。ステロイド抵抗性のうち，標準的な免疫抑制薬治療では完全寛解しないもの。</p>
      <p class="本文_先頭丸数字">❷発症時に，①1歳未満，②持続的血尿，肉眼的血尿，③高血圧，腎機能低下，④低補体血症，⑤腎外症状（発疹，紫斑）を認める場合には，微小変化型以外の組織型の可能性が考えられるため，腎生検にて組織診断を行い治療方針を決定する。ステロイド抵抗性を示す場合にも，治療方針決定のために腎生検による組織診断が必要である。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶小児ネフローゼ症候群の多くは微小変化型である。そのため，初発，再発時ともに<span class="本文_色文字太字">経口ステロイド（</span><span class="本文_色文字太字">PSL</span><span class="本文_色文字太字">）を第一選択薬</span>として治療を開始する。</p>
      <p class="本文_先頭丸数字">❷頻回再発型やステロイド依存性例においては，繰り返す再発を抑制しステロイド治療からの離脱が目標である。</p>
      <p class="本文_先頭丸数字">❸難治性頻回再発型ネフローゼ症候群（FRNS）／ステロイド依存性ネフローゼ症候群（SDNS）では，長期にわたるステロイドや免疫抑制薬使用からの脱却を図る。</p>
      <p class="本文_先頭丸数字">❹ステロイド抵抗性ネフローゼ症候群（SRNS）では，どのように寛解導入を図るかが問題であり，難治性の場合は感染症予防や腎機能低下への対応も必要である。</p>
      <div class="見出し_小見出し">治療方法</div>
      <p class="本文_先頭丸数字">❶重症浮腫の場合を除いて安静・水分制限は不要である。</p>
      <p class="本文_先頭丸数字">❷初発時には，国際法（8週間投与）での短期PSL投与を推奨する。</p>
      <p class="本文_先頭丸数字">❸再発時には，国際法，国際法変法または長期漸減法でのPSL投与を行う。</p>
      <p class="本文_先頭丸数字">❹FRNSあるいはSDNSには，<span class="本文_色文字太字">シクロスポリン，シクロホスファミド，ミゾリビンなどの免疫抑制薬を導入</span>する。ステロイド治療により寛解してから新規免疫抑制薬を開始する。</p>
      <p class="本文_先頭丸数字">❺SRNSには，腎生検による組織学的診断を行い，シクロスポリンと低用量ステロイドの併用を推奨する。さらにステロイドパルス療法との併用も検討する。</p>
      <p class="本文_先頭丸数字">❻難治性FRNS／SDNSには，リツキシマブ，ミコフェノール酸モフェチル，タクロリムスのいずれかを治療の選択肢の1つとして検討してもよい（ただし，後者2つは現時点では適応外処方）。</p>
      <p class="本文_先頭丸数字">❼難治性SRNSには，LDL吸着療法および血漿交換療法の実施やリツキシマブ，タクロリムスなどの治療を考慮する（後者2つは適応外処方）。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には小児腎臓専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①初発時に，微小変化型以外の組織型の可能性が考えられる場合</p>
      <p class="本文_専門医へのコンサルト">②難治性FRNS／SDNSおよび難治性SRNS症例の場合</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">①わが国で行われた大規模RCTの結果，初発ネフローゼ症候群の治療において経口ステロイドの長期漸減法には国際法（8週間投与）に比し，頻回再発化や再発回数を抑制する効果が認められないことが明らかとなった。コクランレビュー（2015）でもこのエビデンスが取り上げられており，経口ステロイドの2～3ヵ月の投与を推奨している。本ガイドラインにおいてもステロイド使用による副作用や患者／家族の負担を軽減できる国際法（8週間投与）が初期治療として推奨されている。</p>
      <p class="本文_最近の話題">②難治性FRNS／SDNS患者ではステロイド長期使用による副作用（成長障害，高血圧，糖尿病，緑内障・白内障，肥満，骨粗鬆症）や長期入院を余儀なくされることが問題である。最近，難治性FRNS／SDNS患者に対してリツキシマブ（Bリンパ球表面抗原CD20に対するモノクローナル抗体）治療が有効であることが明らかとなり保険収載された。しかし，リツキシマブ使用においては進行性多巣性白質脳症，B型肝炎ウイルスのキャリアや既感染患者のウイルス再活性化に伴う劇症肝炎，肺線維症，潰瘍性大腸炎，無顆粒球症など重篤な有害事象が報告されており，使用する際には慎重な検討と経過観察が必要である。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】第一選択薬はステロイドで，80～90％が寛解に至る（ステロイド感受性ネフローゼ症候群）。<span class="本文_色文字太字">ステロイド感受性のうち約</span><span class="本文_色文字太字">80</span><span class="本文_色文字太字">％は再発</span>し，約半数がFRNS（あるいはSDNS）となり免疫抑制薬が用いられる。<span class="本文_色文字太字">ステロイド抵抗性（</span><span class="本文_色文字太字">SRNS</span><span class="本文_色文字太字">）は約</span><span class="本文_色文字太字">10</span><span class="本文_色文字太字">％</span>にみられるが，寛解導入のために免疫抑制薬が用いられる。寛解に至らない場合には腎不全に進行する可能性が高い。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶初発に対して</span>
              </p>
            </td>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅰ）国際法</p>
              <p class="具体的処方_病型分類">　 （8週間投与）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プレドニン</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">プレドニゾロン</span></p>
              <p class="具体的処方_処方例">1）60 mg/m<span class="上付き">2</span>/日または2　mg/kg/日，3回に分服，連日，4週間（最大60　mg/日）</p>
              <p class="具体的処方_処方例">2）40 mg/m<span class="上付き">2</span>/日または1.3　mg/kg/日，朝1回隔日，4週間（最大40　mg/日）</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶再発に対して</span>
              </p>
            </td>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅰ）国際法</p>
              <p class="具体的処方_病型分類">ⅱ）国際法変法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">初発に対してのⅰ）と同じ処方を行う</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プレドニン</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">プレドニゾロン</span></p>
              <p class="具体的処方_処方例">1）60 mg/m<span class="上付き">2</span>/日または2 mg/kg/日，3回に分服，連日，少なくとも尿蛋白消失確認後3日目まで投与，ただし4週間を超えない（最大60 mg/日）</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">2）60 mg/m<span class="上付き">2</span>/日または2　mg/kg/日，朝1回隔日，2週間（最大60　mg/日）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">2）以下の減量法に関しては，主治医の裁量に委ねられる部分が大きい。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">3）30 mg/m<span class="上付き">2</span>/日または1　mg/kg/日，朝1回隔日，2週間（最大30　mg/日）</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">4）15 mg/m<span class="上付き">2</span>/日または0.5　mg/kg/日，朝1回隔日，2週間（最大15　mg/日）</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅲ）長期漸減法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">適宜選択</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">FRN</span>
                <span class="具体的処方_色文字">S</span>
                <span class="具体的処方_色文字">/</span>
                <span class="具体的処方_色文字">S</span>
                <span class="具体的処方_色文字">DNS</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ネオーラル</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～5 mg/kg/日，2回に分服で開始（食前15～30分の内服が望ましい），トラフ値：80～100 ng/mLで6ヵ月間，以後60～80 ng/mLで管理</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①長期に投与する場合は，腎機能障害が認められない場合でも，投与開始2～3年後に腎生検を行い，慢性腎障害の有無を評価する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">エンドキサン</span></p>
              <p class="具体的処方_処方例1W下げ">2～2.5 mg/kg/日（最大100 mg/日），8～12週，朝1回投与が推奨される</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②投与は1クールとし，累積投与量は300 mg/kgを超えてはならない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">ブレディニン</span></p>
              <p class="具体的処方_処方例1W下げ">7～10 mg/kg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③ピーク濃度（C<span class="下付き">2</span> 値またはC<span class="下付き">3</span> 値）を3.0 μg/mL以上とすると，再発予防効果が高いとの報告あり，高用量投与を推奨する。通常用量（4 mg/kg/日，最大150　mg/日）では十分な効果が期待できない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶難治性</span>
              </p>
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">　</span>
                <span class="具体的処方_色文字">FRN</span>
                <span class="具体的処方_色文字">S</span>
                <span class="具体的処方_色文字">/</span>
                <span class="具体的処方_色文字">S</span>
                <span class="具体的処方_色文字">DNS</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">リツキサン</span></p>
              <p class="具体的処方_処方例1W下げ">1回量：375 mg/m<span class="上付き">2</span>（最大投与量500 mg）を1週間間隔で4回点滴静注する</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">セルセプト</span></p>
              <p class="具体的処方_処方例1W下げ">【適応外処方】</p>
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">プログラフ</span></p>
              <p class="具体的処方_処方例1W下げ">【適応外処方】</p>
              <p class="具体的処方_処方例">いずれの薬剤も小児腎臓専門医による治療が望ましい</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①重篤な有害事象も起こりうるので慎重に投与する。</p>
              <p class="具体的処方_ポイント">②③適応外使用薬剤は，患者の病状，リスク・ベネフィットを十分考慮したうえで使用する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">SRNS</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ネオーラル</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～5 mg/kg/日，2回に分服で開始，投与量は以下のトラフ値を目安に調節</p>
              <p class="具体的処方_処方例1W下げ">トラフ値100～150 ng/mL</p>
              <p class="具体的処方_処方例1W下げ">（3ヵ月）</p>
              <p class="具体的処方_処方例1W下げ">トラフ値80～100 ng/mL</p>
              <p class="具体的処方_処方例1W下げ">（3ヵ月～1年）</p>
              <p class="具体的処方_処方例1W下げ">トラフ値60～80 ng/mL</p>
              <p class="具体的処方_処方例1W下げ">（1年以降）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①投与後4～6ヵ月で不完全寛解以上の効果を認めない場合は治療方針を再検討する。不完全／完全寛解に至る場合は，1～2年間継続投与する。</p>
              <p class="具体的処方_ポイント">低用量ステロイドとの併用療法（PSL0.5～1.0 mg/kg，隔日投与）により寛解率が上昇するため併用を推奨する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①との併用で</p>
              <p class="具体的処方_処方例">②ステロイドパルス療法<span class="具体的処方_色文字">ソル・メドロール</span></p>
              <p class="具体的処方_処方例1W下げ">20～30 mg/kg×回/日（最大1 g/日），静脈内投与，1週間に3日間連続を1クールとして施行</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②単独でのステロイドパルス療法は寛解導入には用いない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶難治性</span>
                <span class="具体的処方_色文字">SRNS</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①LDL吸着療法（血漿交換療法）</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">リツキサン</span></p>
              <p class="具体的処方_処方例1W下げ">【適応外処方】</p>
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">プログラフ</span></p>
              <p class="具体的処方_処方例1W下げ">【適応外処方】</p>
              <p class="具体的処方_処方例">いずれの治療も小児腎臓専門医による施行が望ましい</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②③適応外使用薬剤は，患者の病状，リスク・ベネフィットを十分考慮したうえで使用する。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>徳島大学大学院小児科学<span class="著者名">　香美　祥二</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
