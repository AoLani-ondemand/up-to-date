<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>03</chapter>
    <chapter-title>循環器疾患</chapter-title>
    <section>02</section>
    <article-code>03-02</article-code>
    <article-title>高血圧</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>2 高血圧</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">高血圧治療ガイドライン2014（2014）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">診察室血圧よりも家庭血圧を優先する。家庭血圧は朝夕2回測定し，1機会2回の測定を平均する。</li>
        <li class="whatsnew_リスト">降圧目標は診察室血圧で140/90 mmHg未満（合併症あり・後期高齢者除く）とする。</li>
        <li class="whatsnew_リスト">第一選択薬はCa拮抗薬，ARB，ACE阻害薬，利尿薬。β 遮断薬は心疾患合併には積極的適応あり。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">初診時における高血圧の診断と治療方針</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/03-02-A01.svg"/>
          <p class="図表_引用">（日本高血圧学会高血圧治療ガイドライン作成委員会（編）．高血圧治療ガイドライン2014．ライフサイエンス出版；2014．より許諾を得て転載）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="2">診察室血圧に基づいた心血管病リスク層別化</div>
      <div class="アルゴリズム" order="2">
        <div class="図表" type="A" order="2">
          <img class="width-800" src="./image/03-02-A02.svg"/>
          <p class="図表_注">MetS：メタボリックシンドローム，CKD：慢性腎臓病</p>
          <p class="図表_引用">（日本高血圧学会高血圧治療ガイドライン作成委員会（編）．高血圧治療ガイドライン2014．ライフサイエンス出版；2014．より許諾を得て転載）</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶血圧は，心拍出量と末梢血管抵抗の積により規定され，非生理的な血圧の上昇を高血圧という。原因の特定できない本態性高血圧と特定の原因による二次性高血圧に分類される。</p>
      <p class="本文_先頭丸数字">❷至適血圧（120/80 mmHg）を超えて血圧が高くなるほど，脳血管疾患，心血管疾患，CKDなどの罹患率や死亡リスクが上昇する。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶高血圧の診断には，水銀血圧計，アネロイド血圧計を用いた聴診法，あるいは水銀血圧計を用いた聴診法と同程度の精度を有する自動血圧計を用いて測定する。安静座位で，カフ位置は心臓の高さに維持し，少なくとも2回以上測定し，安定した2回の測定を平均する。</p>
      <p class="本文_先頭丸数字">❷診察室血圧140/90 mmHg以上を高血圧と診断する。測定法により，その基準は異なる（<span class="本文_図表番号">表</span><span class="本文_図表番号">1</span>）。</p>
      <div class="図表" type="T" order="1">
        <p class="図表_タイトル" type="T" order="1" data-file="03-02-T01.svg">表1　異なる測定法における高血圧基準（mmHg）</p>
        <img class="width-500" src="./image/03-02-T01.svg"/>
        <p class="図表_引用">（日本高血圧学会高血圧治療ガイドライン作成委員会（編）．高血圧治療ガイドライン2014．ライフサイエンス出版；2014．より許諾を得て転載）</p>
      </div>
      <p class="本文_先頭丸数字">❸診察室血圧測定と家庭血圧測定により高血圧と診断する。複数回血圧を測定し，血圧高値を確認する。<span class="本文_色文字太字">家庭血圧測定</span>と<span class="本文_色文字太字">自由行動下血圧測定</span>（ABPM）により，高血圧および<span class="本文_色文字太字">白衣高血圧</span>と<span class="本文_色文字太字">仮面高血圧</span>の診断を行う（<span class="本文_図表番号">図</span><span class="本文_図表番号">1</span>）。</p>
      <p class="本文_先頭丸数字">①家庭血圧は降圧薬治療の効果判定に有用であり，長期の季節性の血圧変動も捉えることができる。ABPMは，24時間の限られた時間内の情報を得ることができ，家庭血圧で診断が困難な場合や，夜間血圧の測定が必要な時が主な適応である。</p>
      <p class="本文_先頭丸数字">②仮面高血圧は，目標値に降圧コントロールされていない高血圧と同等に臓器合併症のリスクが高いといわれている。</p>
      <div class="図表" type="F" order="1">
        <img class="width-800" src="./image/03-02-F01.svg"/>
        <p class="図表_タイトル" type="F" order="1" data-file="03-02-F01.svg">図1　仮面高血圧に含まれる病態とその因子</p>
        <p class="図表_引用">（日本高血圧学会高血圧治療ガイドライン作成委員会（編）．高血圧治療ガイドライン2014．ライフサイエンス出版；2014．より許諾を得て転載）</p>
      </div>
      <p class="本文_先頭丸数字">❹二次性高血圧の除外診断（ルールアウト）を行い，本態性高血圧と診断する（<span class="本文_図表番号">表</span><span class="本文_図表番号">2</span>）。</p>
      <div class="図表" type="T" order="2">
        <p class="図表_タイトル" type="T" order="2" data-file="03-02-T02.svg">表2　主な二次性高血圧を示唆する所見と鑑別に必要な検査</p>
        <img class="width-800" src="./image/03-02-T02.svg"/>
        <p class="図表_注">RA：レニン・アンジオテンシン，Cr：クレアチニン，CPK：クレアチンキナーゼ，LDH：乳酸脱水素酵素，PRA：血漿レニン活性，PAC：血漿アルドステロン濃度，ACTH：副腎皮質刺激ホルモン，TSH：甲状腺刺激ホルモン</p>
        <p class="図表_引用">（日本高血圧学会高血圧治療ガイドライン作成委員会（編）．高血圧治療ガイドライン2014．ライフサイエンス出版；2014．より許諾を得て転載）</p>
      </div>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶治療目的は，高血圧が持続することによる心血管病の発症・進展・再発を抑制し，死亡を減少させることである。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_色文字太字">治療対象は</span><span class="本文_色文字太字">14</span><span class="本文_色文字太字">0</span><span class="本文_色文字太字">/</span><span class="本文_色文字太字">9</span><span class="本文_色文字太字">0</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">mmHg</span><span class="本文_色文字太字">以上のすべての高血圧患者</span>である。<span class="本文_色文字太字">生活習慣の修正</span>（減塩，減量，禁煙など）（<span class="本文_色文字太字">表</span><span class="本文_色文字太字">3</span>）と降圧薬により，治療を開始する。降圧薬の開始時期については，<span class="本文_図表番号">アルゴリズム</span>に示したように，血圧以外の危険因子や臓器合併症の有無からリスクレベルを評価し決定する。</p>
      <div class="図表" type="T" order="3">
        <p class="図表_タイトル" type="T" order="3" data-file="03-02-T03.svg">表3　生活習慣の修正項目</p>
        <img class="width-800" src="./image/03-02-T03.svg"/>
        <p class="図表_注">生活習慣の複合的な修正はより効果的である</p>
        <p class="図表_引用">（日本高血圧学会高血圧治療ガイドライン作成委員会（編）．高血圧治療ガイドライン2014．ライフサイエンス出版；2014．より許諾を得て転載）</p>
      </div>
      <p class="本文_先頭丸数字">❸<span class="本文_色文字太字">降圧目標</span>は140/90 mmHg未満とする。糖尿病，蛋白尿陽性のCKD患者では130/80 mmHg未満である。後期高齢者は150/90 mmHg未満を目標とし，忍容性があれば140/90 mmHg未満を目指す。</p>
      <p class="本文_先頭丸数字">❹第一選択薬は<span class="本文_色文字太字">Ca</span><span class="本文_色文字太字">拮抗薬</span>，<span class="本文_色文字太字">ARB</span>，<span class="本文_色文字太字">ACE</span><span class="本文_色文字太字">阻害薬</span>，<span class="本文_色文字太字">利尿薬</span>であり，積極的適応または禁忌となる合併症を考慮して，適切な降圧薬を選択する。β遮断薬は第一選択薬から外れたが，心疾患合併患者には適応がある。</p>
      <p class="本文_先頭丸数字">❺ 1日1回投与を原則として開始し，24時間にわたる降圧のために，1日2回投与が好ましいこともある。多くの場合，2～3剤の併用が必要となり，異なるクラスの降圧薬の併用は降圧効果が大きい。160/100 mmHg以上では通常の単剤もしくは少量の2剤併用から開始してよい。2剤の併用としては，RA系阻害薬＋Ca拮抗薬，RA系阻害薬＋利尿薬，Ca拮抗薬＋利尿薬が用いられ，2剤でコントロールが不十分な場合は，3剤併用としてRA系阻害薬＋Ca拮抗薬＋利尿薬が推奨<br/>される。利尿薬は一般的にeGFR 30 mL/分/1.73 m<span class="上付き">2</span> 以上ではサイアザイド系利尿薬，それ未満はループ利尿薬が用いられる。</p>
      <p class="本文_先頭丸数字">❻<img class="" src="./image/03-02-2.png"/>高齢者では，腎におけるNa保持能の低下を契機に体液量が減少し，さらにレニン・アンジオテンシン・アルドステロン（RAA）系の作用減弱の結果，バソプレシンの分泌が亢進され，水分保持から低Na血症を呈することがあり，この状態を鉱質コルチコイド反応性低Na血症（MRHE）という。特に高齢者においては，RA系阻害薬使用により，MRHEを助長し低Na血症を発症することがある。その際にはRA系阻害薬の減量または中止が必要であり，それでも改善がない場合は，フルドロコルチゾンの投与を行う。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①二次性高血圧が疑われる症例</p>
      <p class="本文_専門医へのコンサルト">②利尿薬を含む3剤以上の降圧薬でもコントロールが不良な治療抵抗性高血圧</p>
      <p class="本文_専門医へのコンサルト">③妊娠高血圧</p>
      <p class="本文_専門医へのコンサルト">④高血圧緊急症・切迫症</p>
      <p class="本文_専門医へのコンサルト">⑤臓器障害が悪化した症例</p>
      <p class="本文_専門医へのコンサルト">⑥血圧変動の大きい症例</p>
      <p class="本文_専門医へのコンサルト">⑦白衣高血圧，仮面高血圧の診断に迷う症例</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">①糖尿病合併例において，欧米のガイドラインは目標血圧値を140/90 mmHg未満に緩和した。なぜなら，ACCORD-BP試験において厳格降圧群（目標収縮期血圧120 mmHg未満）は通常降圧群（目標収縮期血圧140 mmHg未満）に比較して，心血管疾患発症にメリットがみられなかったからである。しかしながら脳卒中については41％減少していた。心血管疾患が脳卒中に比較して多い欧米とは異なり，日本では脳卒中が多いことから，脳卒中予防に重きを置いて，JSH2014（本ガイドライン）における糖尿病患者やCKD患者の降圧目標値は130/80 mmHg未満に据え置かれた。</p>
      <p class="本文_最近の話題">②SPRINT試験では，脳卒中・糖尿病を除く中～高リスク高血圧患者に対して，収縮期圧140 mmHg未満の降圧目標に対して120 mmHg未満を目標とした積極的降圧により心血管イベント発生率および総死亡率が低下し，厳格降圧療法の妥当性が示された。しかしSPRINT試験では外来血圧を自動血圧計により複数回測定した血圧（AOBP）に準拠して測定しているため，通常の外来血圧や家庭血圧と直接比較ができないため，現時点で外来血圧をAOBPで示された120 mmHg未満へコントロールすべきとはいえない。SPRINT試験の結果を受けて，わが国でも日本版SPRINT試験としてAOBPの意義や，外来血圧，家庭血圧との違いについても検証される予定である。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】JSH2014に準拠した治療方針で加療する。高リスクでなければ，生活習慣の修正（減塩，減量，禁煙など）を施行後，効果が得られない場合に薬物治療を開始する。降圧薬で血圧を下降させることは，降圧度の大きさに比例して心血管病の発症を予防することができることが示されており，各患者の病態に適した降圧薬を選択し治療を行う。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶左室肥大</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">アジルバ</span></p>
              <p class="具体的処方_処方例1W下げ">20～40 mg×1回/日</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">アダラート</span><span class="具体的処方_色文字">CR</span></p>
              <p class="具体的処方_処方例1W下げ">10～80 mg/日，1～2回に分服</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①RA系阻害薬または②Ca拮抗薬のもつ長時間の強力な降圧効果が心負荷軽減による心肥大抑制作用を呈する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶心不全</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">アーチスト</span></p>
              <p class="具体的処方_処方例1W下げ">1.25～10 mg×2回/日</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">レニベース</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～10 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①～③収縮不全の心不全には①β遮断薬＋②RA系阻害薬＋③利尿薬が標準的治療。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">ダイアート</span></p>
              <p class="具体的処方_処方例1W下げ">30～60 mg×1回/日</p>
              <p class="具体的処方_処方例">低心機能の場合，④を追加</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">アルダクトン</span><span class="具体的処方_色文字">A</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">セララ</span></p>
              <p class="具体的処方_処方例1W下げ">25～50 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④アルドステロン拮抗薬は，重症心不全の予後を改善。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶頻脈</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">メインテート</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～5 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①β遮断薬もしくは②非ジヒドロピリジン系Ca拮抗薬を用いる。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ヘルベッサー</span><span class="具体的処方_色文字">R</span></p>
              <p class="具体的処方_処方例1W下げ">100～200 mg×1回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶冠動脈疾患</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）狭心症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">コニール</span></p>
              <p class="具体的処方_処方例1W下げ">2～8 mg/日，1～2回に分服</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①冠攣縮の関連が疑われる場合は，Ca拮抗薬が第一選択。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">メインテート</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～5 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②器質的狭窄がある場合は，内因性交感神経刺激作用のないβ遮断薬も第一選択。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）心筋梗塞後</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">タナトリル</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～10 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">心機能低下例ではRA系阻害薬による左室リモデリング抑制効果が期待される。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">CKD</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">イルベタン</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">アバプロ</span></p>
              <p class="具体的処方_処方例1W下げ">50～200 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①糖尿病合併または蛋白尿陽性例ではRA系阻害薬が第一選択。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">アテレック</span></p>
              <p class="具体的処方_処方例1W下げ">5～20 mg×1回/日</p>
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">ナトリックス</span></p>
              <p class="具体的処方_処方例1W下げ">0.5～2 mg×1回/日</p>
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">ラシックス</span></p>
              <p class="具体的処方_処方例1W下げ">10～20 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②N型Ca拮抗薬に，糸球体内圧減少による尿蛋白抑制作用あり。多くの場合多剤併用が必要になり，eGFR　30 mL/分/1.73 m<span class="上付き">2</span> 以上では③サイアザイド系利尿薬，未満は④ループ利尿薬の適応あり。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶脳血管障害（慢性期）</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">コバシル</span></p>
              <p class="具体的処方_処方例1W下げ">2～8 mg×1回/日</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ナトリックス</span></p>
              <p class="具体的処方_処方例1W下げ">0.5～2 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①ACE阻害薬や②利尿薬による十分な降圧は，脳血管障害再発率の抑制および認知症の発症予防効果あり。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">アムロジン</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ノルバスク</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～10 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③24時間の確実な降圧効果。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶糖尿病・肥満・メタボリックシンドローム</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ミカルディス</span></p>
              <p class="具体的処方_処方例1W下げ">20～80 mg×1回/日</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">オルメテック</span></p>
              <p class="具体的処方_処方例1W下げ">10～40 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">RA系阻害薬が第一選択。各薬剤により尿酸低下作用，インスリン抵抗性改善作用，脂質代謝改善作用の報告あり。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類"><span class="具体的処方_色文字">▶高齢者</span>（合併症のある場合）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">併用療法</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">レザルタス</span><span class="具体的処方_色文字">LD</span>，<span class="具体的処方_色文字">HD</span></p>
              <p class="具体的処方_処方例1W下げ">1錠×1回/日</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">アイミクス</span><span class="具体的処方_色文字">LD</span>，<span class="具体的処方_色文字">HD</span></p>
              <p class="具体的処方_処方例1W下げ">1錠×1回/日</p>
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">プレミネント</span><span class="具体的処方_色文字">LD</span>，<span class="具体的処方_色文字">HD</span></p>
              <p class="具体的処方_処方例1W下げ">1錠×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">単剤併用で安定した場合は，配合薬使用で，服薬回数減少による服薬アドヒアランスの向上を期待できる。</p>
              <p class="具体的処方_ポイント">①②RA系阻害薬＋Ca拮抗薬，③RA系阻害薬＋利尿薬，④RA系阻害薬＋Ca拮抗薬＋利尿薬。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">ミカトリオ</span></p>
              <p class="具体的処方_処方例1W下げ">1錠×1回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅰ）誤嚥性肺炎</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">エースコール</span></p>
              <p class="具体的処方_処方例1W下げ">2～4 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">ACE阻害薬に，咳反射亢進による誤嚥性肺炎の減少作用あり。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）骨粗鬆症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">フルイトラン</span></p>
              <p class="具体的処方_処方例1W下げ">0.5～1 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">サイアザイド系利尿薬。少量の使用により副作用が軽減される。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>鹿児島大学大学院心臓血管・高血圧内科学<span class="著者名">　赤</span><span class="著者名">﨑</span><span class="著者名">　雄一・大石　　充</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
