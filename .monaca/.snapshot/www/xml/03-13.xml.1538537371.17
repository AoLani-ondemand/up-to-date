<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>03</chapter>
    <chapter-title>循環器疾患</chapter-title>
    <section>13</section>
    <article-code>03-13</article-code>
    <article-title>肺高血圧症</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>13 肺高血圧症</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">肺高血圧症治療ガイドライン（2012年改訂版）（2012）</li>
          <li class="ガイドライン_リスト">2014年版慢性肺動脈血栓塞栓症に対するballoon pulmonary <br/>angioplastyの適応と実施法に関するステートメント（2015）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">2016年に欧州心臓病学会および呼吸器病学会から，肺高血圧症（PH）の診断と治療に関するガイドラインが発表された。</li>
        <li class="whatsnew_リスト">リオシグアト，イロプロスト，セレキシパグが肺動脈性肺高血圧症（PAH）の治療薬として使用可能となった。</li>
        <li class="whatsnew_リスト">血栓内膜摘除術の適応にならない慢性血栓塞栓性肺高血圧症（CTEPH）に対して，balloon pulmonary angioplastyの適応と実施法を示すステートメントが発表された。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">①肺高血圧症（PH）診断アルゴリズム</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/03-13-A01.svg"/>
          <p class="図表_引用">（「日本循環器学会．肺高血圧症治療ガイドライン（2012年改訂版）．http://www.j-circ.or.jp/guideline/pdf/JCS2012_<br/>nakanishi_h.pdf（2017年9月閲覧）」より許諾を得て転載）</p>
          <p class="図表_注">・まず一般的原因である左心疾患（第2群），肺疾患（第3群）の鑑別を行う。次に慢性血栓塞栓性肺高血圧症（第4群）の鑑別を行う。</p>
          <p class="図表_注">・第1，4，5群の可能性が考えられた時点で，専門医への紹介を考慮する。</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="2">②肺動脈性肺高血圧症（PAH）に対する治療手順</div>
      <div class="アルゴリズム" order="2">
        <div class="図表" type="A" order="2">
          <img class="width-800" src="./image/03-13-A02.svg"/>
          <p class="図表_引用">（「日本循環器学会．肺高血圧症治療ガイドライン（2012年改訂版）．http://www.j-circ.or.jp/guideline/pdf/JCS2012_<br/>nakanishi_h.pdf（2017年9月閲覧）」より許諾を得て転載）</p>
          <p class="図表_注">・一般的な処置，支持療法とは，抗凝固療法や酸素投与，右心不全合併時の利尿薬投与である。</p>
          <p class="図表_注">・急性肺血管反応性試験とは，エポプロステノール持続静注，アデノシン静注，NO吸入を行いその反応をみる試験である。陽性の条件は，肺動脈平均圧（mPAP）が10 mmHg以上低下し40 mmHg以下になり，かつCOが低下しないことである。</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶右心カテーテル検査が必須であり，安静時<span class="本文_色文字太字">mPAP</span><span class="本文_色文字太字">が</span><span class="本文_色文字太字">25</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">mmHg</span><span class="本文_色文字太字">以上</span>をPHと定義する。</p>
      <p class="本文_先頭丸数字">❷PHのなかで肺動脈楔入圧が15 mmHg以下であるものをPAHと定義する。</p>
      <p class="本文_先頭丸数字">❸分類：PHは原因によって5群に分類される（ニース分類，<span class="本文_図表番号">表</span><span class="本文_図表番号">1</span>）。</p>
      <div class="図表" type="T" order="1">
        <p class="図表_タイトル" type="T" order="1" data-file="03-13-T01.svg">表1　再改訂版肺高血圧症臨床分類</p>
        <img class="width-800" src="./image/03-13-T01.svg"/>
        <p class="図表_引用">（Simonneau G, et al. J Am Coll Cardiol. 2013；<span class="太字">62</span> Suppl 25：D34-41.）</p>
      </div>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶PH診断アルゴリズムに基づき診断する。</p>
      <p class="本文_先頭丸数字">❷労作時の息切れ，Ⅱ音肺動脈成分の亢進，BNPの上昇，心電図における<span class="本文_色文字太字">V</span><span class="本文_色文字太字">1</span><span class="本文_色文字太字">誘導の</span><span class="本文_色文字太字">R</span><span class="本文_色文字太字">波の増高</span>，胸部X線写真における<span class="本文_色文字太字">肺動脈本幹の拡大</span>などがみられたら，心エコー図検査を行う。</p>
      <p class="本文_先頭丸数字">❸膠原病，呼吸器疾患，先天性心疾患，肝疾患，肺塞栓の既往などPHを合併しうる疾患では，積極的に心エコー図検査を行う。</p>
      <p class="本文_先頭丸数字">❹心エコー図検査は有用だが，三尖弁逆流速度から求める推定肺動脈圧は過大・過小評価することがある。そのためPH診断のためには，右心カテーテル検査を行うべきである。</p>
      <p class="本文_先頭丸数字">❺アルゴリズムで示された原因疾患が除外されれば，特発性／遺伝性PAH，肺静脈閉塞性疾患（PVOD），肺毛細血管腫症（PCH）の診断となるが，第5群のPHの可能性も考慮する。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶第1群では，PAHに対する治療手順に基づき治療を行う。<span class="本文_色文字太字">3</span><span class="本文_色文字太字">系統の薬剤の積極的な併用と速やかな</span><span class="本文_色文字太字">dose</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">up</span><span class="本文_色文字太字">が有効</span>である。膠原病に関連したPAHでは，原疾患の活動性を評価したうえで，ステロイドや免疫抑制薬を検討する。先天性心疾患に関連したPAHでは心内修復術も考慮するが，PHのため適応がない場合も多い。その際にはPH治療を優先する<span class="本文_色文字太字">treat</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">and</span><span class="本文_色文字太字"> </span><span class="本文_色文字太字">repair</span>も検討する。WHO-FC Ⅲ度以上や経口薬で十分な効果が得られない場合は，エポプロステノール持続静注療法の導入を躊躇してはならない。</p>
      <p class="本文_先頭丸数字">❷第1’群であるPVOD／PCHは内科的治療が奏効しないため，診断がつき次第肺移植登録を考慮する。</p>
      <p class="本文_先頭丸数字">❸第2群，第3群に対しては原疾患の治療を優先する。</p>
      <p class="本文_先頭丸数字">❹第4群に対しては，まず肺動脈血栓内膜摘除術（PEA）を検討する（<span class="本文_図表番号">図</span><span class="本文_図表番号">1</span>）。PEAの適応とならない場合は，<span class="本文_色文字太字">バルーン肺動脈拡張術（</span><span class="本文_色文字太字">BPA</span><span class="本文_色文字太字">）</span>もしくは内服治療（リオシグアト）を選択する。BPAの適応基準を<span class="本文_図表番号">表</span><span class="本文_図表番号">2</span>に示す。BPAを正しく安全に施行するためには，PHの治療経験が十分にある，BPAの合併症に問題なく対処できる，集中治療室がある，外科医のバックアップがある，などの条件が望まれる。</p>
      <div class="図表" type="F" order="1">
        <img class="width-800" src="./image/03-13-F01.svg"/>
        <p class="図表_タイトル" type="F" order="1" data-file="03-13-F01.svg">図1　CTEPHの治療手順</p>
        <p class="図表_引用">（「日本循環器学会．肺高血圧症治療ガイドライン（2012年改訂版）．http://www.j-circ.or.jp/guideline/pdf/JCS2012_<br/>nakanishi_h.pdf（2017年9月閲覧）」より許諾を得て転載）</p>
      </div>
      <div class="図表" type="T" order="2">
        <p class="図表_タイトル" type="T" order="2" data-file="03-13-T02.svg">表2　BPAの適応</p>
        <img class="width-800" src="./image/03-13-T02.svg"/>
        <p class="図表_注">PEA：血栓内膜摘除術，BPA：バルーン肺動脈拡張術</p>
        <p class="図表_引用">（「日本循環器学会．循環器病ガイドシリーズ2014年度版：2014年版慢性肺動脈血栓塞栓症に対するballoon pulmonary angioplastyの適応と実施法に関するステートメント．http://www.j-circ.or.jp/guideline/pdf/JCS2014_ito_d.pdf（2017年9月閲覧）」より許諾を得て転載）</p>
      </div>
      <p class="本文_先頭丸数字">❺PAH，PVOD／PCHおよびCTEPHは指定難病となっている。PH治療薬は高価なものが多いため，薬剤投与前やPEA／BPA前にこれらの申請を行う。</p>
      <p class="本文_先頭丸数字">❻<img class="" src="./image/03-13-2.png"/>近年，エンドセリン受容体拮抗薬であるマシテンタン，可溶性グアニル酸シクラーゼ刺激薬であるリオシグアト，選択的IP受容体作動薬であるセレキシパグ，吸入薬のイロプロスト，持続静注もしくは皮下注投与するトレプロスチニルが登場し，症例に応じて使用されている。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①診断の確定が困難な症例</p>
      <p class="本文_専門医へのコンサルト">②高度の右心不全を伴うなどの重症例</p>
      <p class="本文_専門医へのコンサルト">③エポプロステノールの導入を考慮する例</p>
      <p class="本文_専門医へのコンサルト">④CTEPHに対してPEAもしくはBPAを考慮する例</p>
      <p class="本文_専門医へのコンサルト">⑤肺移植を考慮する例</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">　本ガイドライン改訂以降に，以下の薬剤が認可された。</p>
      <p class="本文_最近の話題">①リオシグアト：可溶性グアニル酸シクラーゼ刺激薬。NO非依存的に肺血管を拡張する。PDE5阻害薬との併用は禁忌。</p>
      <p class="本文_最近の話題">②トレプロスチニル：プロスタサイクリン誘導体。持続静注／皮下注投与可能なプロスタサイクリン製剤。持続皮下注では，注射部位の疼痛が必発。</p>
      <p class="本文_最近の話題">③マシテンタン：エンドセリンAおよびB受容体拮抗薬。</p>
      <p class="本文_最近の話題">④イロプロスト：プロスタサイクリン誘導体。吸入プロスタサイクリン製剤。1日6～9回の投与が必要。</p>
      <p class="本文_最近の話題">⑤セレキシパグ：選択的IP受容体作動薬。IP受容体への親和性が強い。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】①NYHA/WHO肺高血圧症機能分類Ⅱ度以下の症例に対しては，エンドセリン受容体拮抗薬，PDE5阻害薬，可溶性グアニル酸シクラーゼ刺激薬，IP受容体作動薬を単剤もしくは併用して使用する。②NYHA/WHO肺高血圧症機能分類Ⅲ度の症例に対しては，経口薬の併用に加え，イロプロスト吸入薬やエポプロステノール，トレプロスチニルの使用も考慮する。③NYHA/WHO肺高血圧症機能分類Ⅳ度の症例に対しては，エポプロステノール投与のうえ，経口薬を併用する。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">PAH</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）NYHA/WHO肺高血圧症機能分類Ⅱ度</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">トラクリア</span></p>
              <p class="具体的処方_処方例1W下げ">62.5 mg×2回/日で開始，125 mg×2回/日まで増量可</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①～③エンドセリン受容体拮抗薬。</p>
              <p class="具体的処方_ポイント">①肝障害に注意。多数の併用禁忌薬があり注意が必要。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ヴォリブリス</span></p>
              <p class="具体的処方_処方例1W下げ">5 mg×1回/日で開始，10 mg×1回/日まで増量可</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②体液貯留に注意。シクロスポリンと併用するときは5 mg。間質性肺炎症例には慎重投与。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">オプスミット</span></p>
              <p class="具体的処方_処方例1W下げ">10 mg×1回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">レバチオ</span></p>
              <p class="具体的処方_処方例1W下げ">20 mg×3回/日</p>
              <p class="具体的処方_処方例">⑤<span class="具体的処方_色文字">アドシルカ</span></p>
              <p class="具体的処方_処方例1W下げ">20～40 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④⑤PDE5阻害薬。アデムパスおよび硝酸薬との併用禁忌。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <span>
                <img class="" src="./image/03-13-2.png"/>
              </span>
              <p class="具体的処方_処方例">⑥<span class="具体的処方_色文字">アデムパス</span></p>
              <p class="具体的処方_処方例1W下げ">1 mg×3回/日で開始，2.5 mg×3回まで増量可</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">⑥可溶性グアニル酸シクラーゼ刺激薬。PDE5阻害薬および硝酸薬との併用禁忌。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">⑦<span class="具体的処方_色文字">ウプトラビ</span></p>
              <p class="具体的処方_処方例1W下げ">0.2 mg×2回/日で開始，1.6 mg×2回/日まで増量可</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">⑦選択的IP受容体作動薬。増量間隔は1週間毎。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）NYHA/WHO肺高血圧症機能分類Ⅲ度</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">ⅰ）①～⑦の併用療法に加え以下の薬剤の使用を検討</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">フローラン</span></p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">エポプロステノール</span></p>
              <span>
                <img class="" src="./image/03-13-2.png"/>
              </span>
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">トレプロスト</span></p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①～③ヒックマンカテーテルを留置し，持続ポンプを用いて投与。専門医での導入が望ましい。①②持続静注プロスタサイクリン製剤，③持続静注／皮下注プロスタサイクリン製剤。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">ベンテイビス</span></p>
              <p class="具体的処方_処方例1W下げ">2.5 μg×6～9回/日で開始，5 μg×6～9回/日まで増量可</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④吸入プロスタサイクリン製剤。I-neb AADネブライザを使用して吸入。1回の吸入では準備～吸入～洗浄まで約10～15分要する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅲ）NYHA/WHO肺高血圧症機能分類Ⅳ度</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">持続静注プロスタサイクリン製剤を導入のうえ，他薬剤の併用を行う</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶</span>
                <span class="具体的処方_色文字">CTEPH</span>
              </p>
              <p class="具体的処方_病型分類">PEA/BPA不適応症例</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">アデムパス</span></p>
              <p class="具体的処方_処方例1W下げ">1 mg×3回/日で開始，2.5 mg×3回まで増量可</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①可溶性グアニル酸シクラーゼ刺激薬。増量間隔は2週間毎。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>岡山大学大学院循環器内科学<span class="著者名">　赤木　　達・伊藤　　浩</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
