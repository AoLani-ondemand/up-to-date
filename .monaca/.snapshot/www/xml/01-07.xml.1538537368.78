<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>01</chapter>
    <chapter-title>感染症</chapter-title>
    <section>07</section>
    <article-code>01-07</article-code>
    <article-title>深在性真菌症</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>7 深在性真菌症</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト_NEW"><span class="ガイドライン_NEWマーク">NEW</span>  深在性真菌症の診断・治療ガイドライン2014小児領域改訂版（2016）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">小児領域で新たに保険適用を取得したボリコナゾールおよびカスポファンギンの推奨度が追加された。</li>
        <li class="whatsnew_リスト">カンジダの薬剤感受性試験の結果に基づく治療戦略を提唱した。</li>
        <li class="whatsnew_リスト">輸入真菌症，感染制御領域が追加され，総論の充実，チェックリストが追加された。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">①血液疾患領域の診断フローチャート</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-900" src="./image/01-07-A01.svg"/>
          <p class="図表_注">GM：ガラクトマンナン，BAL：気管支肺胞洗浄</p>
          <p class="図表_引用">（深在性真菌症のガイドライン作成委員会（編）．深在性真菌症の診断・治療ガイドライン2014 小児領域改訂版．協和企画；2016．より許諾を得て転載）</p>
          <p class="図表_注">・<span class="本文_図表番号">アルゴリズム①</span>は，発熱性好中球減少症（FN）の患者を対象としている。</p>
          <p class="図表_注">・上記以外の患者でも，本フローチャートは深在性真菌症の診断アルゴリズムとして参考になる。</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="2">②慢性進行性肺アスペルギルス症の診療フローチャート</div>
      <div class="アルゴリズム" order="2">
        <div class="図表" type="A" order="2">
          <img class="width-900" src="./image/01-07-A02.svg"/>
          <p class="図表_注">WBC：白血球数，ESR：赤血球沈降速度，BALF：気管支肺胞洗浄液，TBLB：経気管支肺生検，MCFG：ミカファンギン，VRCZ：ボリコナゾール，CPFG：カスポファンギン，ITCZ：イトラコナゾール，L-AMB：リポソームアムホテリシンB</p>
          <p class="図表_引用">（深在性真菌症のガイドライン作成委員会（編）．深在性真菌症の診断・治療ガイドライン2014 小児領域改訂版．協和企画；2016．より許諾を得て転載）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="3">③肺クリプトコックス症（非HIV患者）の診療フローチャート</div>
      <div class="アルゴリズム" order="3">
        <div class="図表" type="A" order="3">
          <img class="width-900" src="./image/01-07-A03.svg"/>
          <p class="図表_注">FLCZ：フルコナゾール，F-FLCZ：ホスフルコナゾール，5-FC：フルシトシン</p>
          <p class="図表_引用">（深在性真菌症のガイドライン作成委員会（編）．深在性真菌症の診断・治療ガイドライン2014 小児領域改訂版．協和企画；2016．より許諾を得て転載）</p>
          <p class="図表_注">・健康成人に発症しうる本邦で唯一の深在性真菌症である。</p>
          <p class="図表_注">・血清クリプトコックス抗原検査が有用である。</p>
          <p class="図表_注">・肺クリプトコックス症と診断した場合，クリプトコックス脳髄膜炎の精査を行う。</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶真菌感染症のうち，皮膚以外の深部を感染巣とするものを，深在性真菌症と呼ぶ。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">原因真菌</span>：<span class="本文_色文字太字">カンジダ</span>，<span class="本文_色文字太字">アスペルギルス</span>，<span class="本文_色文字太字">クリプトコックス</span>，<span class="本文_色文字太字">ムーコル</span>，トリコスポロン，ニューモシスチスなど。</p>
      <p class="本文_先頭丸数字">❸コクシジオイデス症，ヒストプラスマ症，パラコクシジオイデス症，マルネッフェイ型ペニシリウム症，ブラストミセス症といった輸入真菌症も増加傾向にある。</p>
      <div class="見出し_小見出し">診断<span class="本文_太字">（</span><span class="本文_図表番号">表</span><span class="本文_図表番号">1</span><span class="本文_太字">）</span></div>
      <p class="本文_先頭丸数字">❶診断の基本は培養検査である。しかし感度が低いため，病理学的検査が有用なときもある。</p>
      <p class="本文_先頭丸数字">❷血清学的検査法に，β-Dグルカン，カンジダマンナン抗原，GM抗原（アスペルギルス抗原），グルクロノキシロマンナン抗原（クリプトコックス抗原）が実用化されている。</p>
      <p class="本文_先頭丸数字">❸ 1ヵ月以上の経過を示す肺アスペルギルス症を慢性肺アスペルギルス症（CPA）と呼び，そのなかでも活動性の炎症を伴わないものを単純性肺アスペルギローマ（SPA），活動性の炎症を伴うものを慢性進行性肺アスペルギルス症（CPPA）に分類する。</p>
      <p class="本文_先頭丸数字">❹CPAの診断には，抗アスペルギルス沈降抗体が用いられる。</p>
      <p class="本文_先頭丸数字">❺肺クリプトコックス症と診断した場合は，髄液検査などの脳髄膜炎の精査も行う。</p>
      <p class="本文_先頭丸数字">❻ニューモシスチス肺炎の診断には，胸部CTにおける淡いスリガラス影のほか，血清β-Dグルカン，呼吸器検体の塗抹検査，PCR検査が有用である。</p>
      <div class="図表" type="T" order="1">
        <p class="図表_タイトル" type="T" order="1" data-file="01-07-T01.svg">表1　深在性真菌症の診断法</p>
        <img class="width-800" src="./image/01-07-T01.svg"/>
        <p class="図表_注">◎：非常に有用。可能であれば試みるべき，○：有用。試みる価値あり，△：病態により有用な場合がある，×：通常あまり有用でない，－：推奨できる検査法は知られていない</p>
        <p class="図表_注">＊1：実際の有用性は病態・基礎疾患などに左右される，＊2：菌種の同定は通常困難である，＊3：海外ではカンジダマンナン抗体・抗原同時測定の有用性が報告されている，＊4：保険適用外，＊5：ムーコル症，クリプトコックス症などを除く多くの真菌症で上昇する。原因真菌の鑑別は不能，＊6：クリプトコックスはムーコルと異なり細胞壁に β-Dグルカンを有するので，クリプトコックス症では β-Dグルカンを検出する場合がある。しかし，その頻度は低いので，本ガイドラインでは診断的価値を認めない</p>
        <p class="図表_引用">（深在性真菌症のガイドライン作成委員会（編）．深在性真菌症の診断・治療ガイドライン2014 小児領域改訂版．協和企画；2016．より許諾を得て転載，改変）</p>
      </div>
      <div class="見出し_小見出し">治療方針</div>
      <div class="見出し_孫見出し"><img class="" src="./image/01-07-小見出し_矢印.png"/>カンジダ症</div>
      <p class="本文_先頭丸数字">❶菌種判明時に菌種ごとに抗真菌薬を考慮する（<span class="本文_図表番号">表</span><span class="本文_図表番号">2</span>）。カンジダ属は抗真菌薬感受性試験を実施する。</p>
      <p class="本文_先頭丸数字">❷カンジダ血症の場合，血管内カテーテルの抜去が最も重要である。カンジダ血症に対する抗真菌薬投与期間の目安は，血液培養陰性化後14日間である。</p>
      <div class="図表" type="T" order="2">
        <p class="図表_タイトル" type="T" order="2" data-file="01-07-T02.svg">表2　カンジダ菌種によって選択される抗真菌薬（参考）</p>
        <img class="width-800" src="./image/01-07-T02.svg"/>
        <p class="図表_引用">（深在性真菌症のガイドライン作成委員会（編）．深在性真菌症の診断・治療ガイドライン2014 小児領域改訂版．協和企画；2016．より許諾を得て転載）</p>
      </div>
      <div class="見出し_孫見出し"><img class="" src="./image/01-07-小見出し_矢印.png"/>アスペルギルス症</div>
      <p class="本文_先頭丸数字">❶抗アスペルギルス活性をもつ抗真菌薬は，アムホテリシンB（AMPH-B），ITCZ，VRCZ，MCFG，CPFGに限られる。</p>
      <p class="本文_先頭丸数字">❷侵襲性肺アスペルギルス症に対する第一選択薬はVRCZ，AMPH-Bである。</p>
      <p class="本文_先頭丸数字">❸活動性の炎症を伴わないSPAに対する第一選択は肺切除である。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/01-07-小見出し_矢印.png"/>クリプトコックス症</div>
      <p class="本文_先頭丸数字">❶クリプトコックス症に対し，MCFGやCPFGは無効である。</p>
      <p class="本文_先頭丸数字">❷肺クリプトコックス症の治療にはFLCZないしはITCZを用いるが，クリプトコックス脳髄膜炎など重症例にはAMPH-Bと5-FCの併用療法が必要となる。</p>
      <div class="見出し_孫見出し"><img class="" src="./image/01-07-小見出し_矢印.png"/>ムーコル症・トリコスポロン症・ニューモシスチス肺炎</div>
      <p class="本文_先頭丸数字">❶ムーコル症にはAMPH-Bのみが有効であり，積極的な切除やデブリドマンを考慮する。</p>
      <p class="本文_先頭丸数字">❷トリコスポロン症の第一選択薬はVRCZである。</p>
      <p class="本文_先頭丸数字">❸ニューモシスチス肺炎の治療にはST合剤，ペンタミジン，アトバコンが用いられる。治療期間は21日間が推奨されている。</p>
      <p class="本文_先頭丸数字">❹呼吸障害の強いニューモシスチス肺炎の場合は，ステロイドの併用を考慮する。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①輸入真菌症の疑い</p>
      <p class="本文_専門医へのコンサルト">②真菌性眼内炎，副鼻腔真菌症</p>
      <p class="本文_専門医へのコンサルト">③原因真菌の培養陰性化が得られない</p>
      <p class="本文_専門医へのコンサルト">④一部の抗真菌薬と相互作用のある薬剤を使用している</p>
      <p class="本文_専門医へのコンサルト">⑤経験のない真菌が培養で得られた</p>
      <p class="本文_専門医へのコンサルト">⑥キャンディン耐性カンジダ疑い，アゾール耐性アスペルギルス疑い</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">　深在性真菌症の世界でも<span class="本文_色文字太字">薬剤耐性</span>の問題が拡大しており，近年はカンジダのみならず，アスペルギルスにおいても薬剤耐性株の増加が危惧されている。アスペルギルスは環境真菌であり，土壌を中心として幅広い環境に存在するが，特に農薬として使用されているアゾール系抗真菌薬の影響により，環境中でアゾール耐性アスペルギルスが増加し，臨床の現場に影響を与えていることが明らかとなってきた。さらに，これらの環境由来耐性株は世界中への拡大傾向を示しており，2016年には日本においても環境由来株と思われる耐性株が臨床検体から分離されるに至っている。今後も耐性株の増加に注意が必要だが，アスペルギルスの薬剤感受性試験が実施できる施設が限られている点が大きな問題点である。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】いずれも抗真菌薬投与が基本となるが，ムーコル症や副鼻腔真菌症，活動性の炎症を伴わないSPAは積極的な外科的対応を考慮する。カンジダ症は抗真菌薬感受性試験の結果により，抗真菌薬の変更を検討する。カンジダ血症の場合，血管内カテーテルの抜去が最も重要である。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶カンジダ症</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ファンガード</span></p>
              <p class="具体的処方_処方例1W下げ">100～150 mg×1回/日，点滴静注</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">カンサイダス</span></p>
              <p class="具体的処方_処方例1W下げ">50 mg×1回/日，点滴静注</p>
              <p class="具体的処方_処方例1W下げ">（初期投与：初日のみ70 mg×1回/日）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①②キャンディン系抗真菌薬。<span class="本文_イタリック">Candida</span><span class="本文_イタリック"> </span><span class="本文_イタリック">parapsilosis</span>の感受性は低いが，安全性が高く，菌種判明前の経験的治療にも用いられる。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">アムビゾーム</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～5 mg/kg×1回/日，点滴静注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③L-AMB製剤。腎機能障害や低K血症に注意。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">プロジフ</span></p>
              <p class="具体的処方_処方例1W下げ">400 mg×1回/日，静注</p>
              <p class="具体的処方_処方例1W下げ">（初期投与：800 mg×1回/日，2日間）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④F-FLCZ。体内でFLCZに変化する。静注可能。併用注意薬が多い。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶アスペルギルス症</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）侵襲性肺アスペルギルス症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ブイフェンド</span></p>
              <p class="具体的処方_処方例1W下げ">4 mg/kg×2回/日，点滴静注</p>
              <p class="具体的処方_処方例1W下げ">（初期投与：初日のみ6 mg/kg/回）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①VRCZ。開始当初に一過性の視覚異常を伴うことがある。薬物血中濃度測定が必要。免疫抑制薬など，薬物相互作用に注意。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">アムビゾーム</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～5 mg/kg×1回/日，点滴静注</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">イトリゾール</span></p>
              <p class="具体的処方_処方例1W下げ">200 mg×1回/日，点滴静注</p>
              <p class="具体的処方_処方例1W下げ">（初期投与：200 mg×2回/日，2日間）</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③ITCZ。併用禁忌薬剤が多いため注意。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">ファンガード</span></p>
              <p class="具体的処方_処方例1W下げ">150～300 mg×1回/日，点滴静注</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">④キャンディン系抗真菌薬の抗アスペルギルス活性は限定的のため，第一選択薬とはしない。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）慢性肺アスペルギルス症の維持療法</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ブイフェンド</span></p>
              <p class="具体的処方_処方例1W下げ">200 mg×2回/日，内服</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①バイオアベイラビリティは良好。定期的な薬物血中濃度測定が必要。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">イトリゾール</span></p>
              <p class="具体的処方_処方例1W下げ">200 mg×1～2回/日，内服</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶クリプトコックス症</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）肺クリプトコックス症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">プロジフ</span></p>
              <p class="具体的処方_処方例1W下げ">400 mg×1回/日，静注</p>
              <p class="具体的処方_処方例1W下げ">（初期投与：800 mg×1回/日，2日間）</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ジフルカン</span></p>
              <p class="具体的処方_処方例1W下げ">200～400 mg×1回/日，内服</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②FLCZ。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">イトリゾール</span></p>
              <p class="具体的処方_処方例1W下げ">200 mg×1回/日，点滴静注</p>
              <p class="具体的処方_処方例1W下げ">（初期投与：200 mg×2回/日，2日間）</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">イトリゾール</span></p>
              <p class="具体的処方_処方例1W下げ">200 mg×1回/日，内服</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）クリプトコックス脳髄膜炎</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">アムビゾーム</span></p>
              <p class="具体的処方_処方例1W下げ">2.5～6 mg/kg×1回/日，点滴静注，4週間</p>
              <p class="具体的処方_処方例">＋<span class="具体的処方_色文字">アンコチル</span></p>
              <p class="具体的処方_処方例1W下げ">25 mg/kg×4回/日，2週間，内服</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①<span class="本文_イタリック">アムビゾーム</span>は高用量を用いる。<span class="本文_イタリック">アンコチル</span>（5-FC）は骨髄抑制に注意。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶ムーコル症</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">アムビゾーム</span></p>
              <p class="具体的処方_処方例1W下げ">5 mg/kg×1回/日，点滴静注</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶トリコスポロン症</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">ブイフェンド</span></p>
              <p class="具体的処方_処方例1W下げ">4 mg/kg×2回/日，点滴静注</p>
              <p class="具体的処方_処方例1W下げ">（初期投与：初日のみ6 mg/kg/回）</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶ニューモシスチス肺炎</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">バクタ</span></p>
              <p class="具体的処方_処方例1W下げ">3 g×3回/日，3週間</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">①ST合剤。発疹，発熱などの副作用出現率が高い。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ベナンバックス</span></p>
              <p class="具体的処方_処方例1W下げ">3～4 mg/kg×1回/日，点滴静注，3週間</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">②①が使用できない際に用いるが，副作用の出現率は同様に高い。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">サムチレール</span></p>
              <p class="具体的処方_処方例1W下げ">750 mg×2回/日，3週間</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">③比較的安全性は高いが，治療効果は他に劣る。</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>長崎大学大学院臨床感染症学<span class="著者名">　田代　将人・泉川　公一</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
