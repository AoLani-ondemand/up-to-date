<?xml version="1.0" encoding="UTF-8"?>
<doc>
  <head>
    <chapter>13</chapter>
    <chapter-title>耳鼻咽喉科疾患</chapter-title>
    <section>05</section>
    <article-code>13-05</article-code>
    <article-title>鼻副鼻腔炎</article-title>
    <book-title>診療ガイドライン UP-TO-DATE 2018-2019</book-title>
  </head>
  <body>
    <!--ヘッドライン add start-->
    <div class="ブロック-ヘッドライン">
      <div class="記事タイトル">
        <p>5 鼻副鼻腔炎</p>
      </div>
      <div class="ガイドライン">
        <ul>
          <li class="ガイドライン_リスト">副鼻腔炎診療の手引き（2007）</li>
          <li class="ガイドライン_リスト">急性鼻副鼻腔炎診療ガイドライン2010年版（2010）</li>
          <li class="ガイドライン_リスト">急性鼻副鼻腔炎診療ガイドライン2010年版（追補版）（2013）</li>
        </ul>
      </div>
    </div>
    <!--ヘッドライン add end-->
    <!--whatsnew add start-->
    <div class="ブロック-whatsnew">
      <div class="whatsnew_タイトル">What's new</div>
      <ul>
        <li class="whatsnew_リスト">臨床症状と鼻腔所見の各項目をスコア化しその点数により重症度分類。</li>
        <li class="whatsnew_リスト">重症度ごとの治療アルゴリズムに従って治療を行うことを推奨。</li>
        <li class="whatsnew_リスト">薬剤耐性（AMR）対策アクションプランを意識し，投与する抗菌薬を選択し，投与する場合には高用量短期間投与を心がける。</li>
      </ul>
    </div>
    <!--whatsnew add end-->
    <!--アルゴリズム add start-->
    <div class="ブロック-アルゴリズム">
      <div class="アルゴリズム_タイトル">
        <p>アルゴリズム</p>
      </div>
      <div class="アルゴリズム_見出し" order="1">①急性鼻副鼻腔炎のスコアリングと重症度分類</div>
      <div class="アルゴリズム" order="1">
        <div class="図表" type="A" order="1">
          <img class="width-800" src="./image/13-05-A01.svg"/>
          <p class="図表_引用">（日本鼻科学会急性鼻副鼻腔炎診療ガイドライン作成委員会．急性鼻副鼻腔炎診療ガイドライン．日鼻誌．2010； <span class="太字">49</span>：143-98．より作成）</p>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="2">②急性鼻副鼻腔炎の治療アルゴリズム<br/>1）成人・軽症</div>
      <div class="アルゴリズム" order="2">
        <div class="図表" type="A" order="2">
          <img class="width-800" src="./image/13-05-A02-1.svg"/>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="3">2）成人・中等症</div>
      <div class="アルゴリズム" order="3">
        <div class="図表" type="A" order="3">
          <img class="width-800" src="./image/13-05-A02-2.svg"/>
        </div>
      </div>
      <div class="アルゴリズム_見出し" order="4">3）成人・重症</div>
      <div class="アルゴリズム" order="4">
        <div class="図表" type="A" order="4">
          <img class="width-800" src="./image/13-05-A02-3.svg"/>
          <p class="図表_注">・＊で経過が思わしくない場合には肺炎球菌迅速診断なども参考のうえ，抗菌薬の変更を考慮する。</p>
          <p class="図表_注">AMPC：アモキシシリン，CDTR-PI：セフジトレンピボキシル，CFPN-PI：セフカペンピボキシル，CFTM-PI：セフテラムピボキシル，AZM：アジスロマイシン，CTRX：セフトリアキソン</p>
          <p class="図表_引用">（日本鼻科学会急性鼻副鼻腔炎診療ガイドライン作成委員会．急性鼻副鼻腔炎診療ガイドライン．日鼻誌．2010；<span class="太字">49</span>：143-98．）</p>
        </div>
      </div>
    </div>
    <!--アルゴリズム add end-->
    <!--総説 add start-->
    <div class="ブロック-総説">
      <div class="総説_タイトル">
        <p>総説</p>
      </div>
      <div class="見出し_小見出し">定義</div>
      <p class="本文_先頭丸数字">❶<span class="本文_太字">急性鼻副鼻腔炎</span>：急性に発症し，発症から4週間以内の鼻副鼻腔の感染症で，鼻閉，鼻漏，後鼻漏，咳嗽といった呼吸器症状を呈し，頭痛，頬部痛，顔面圧迫感などを伴う疾患。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">慢性副鼻腔炎</span>：副鼻腔の炎症により，鼻閉，鼻漏，後鼻漏，咳嗽といった呼吸器症状を呈する疾患で，頭痛，頬部痛や嗅覚障害などを伴う疾患。</p>
      <div class="見出し_小見出し">診断</div>
      <p class="本文_先頭丸数字">❶<span class="本文_太字">急性鼻副鼻腔炎</span>：<span class="本文_図表番号">アルゴリズム①</span>のように臨床症状と鼻腔所見を合わせた臨床所見から<span class="本文_色文字太字">診断</span>および<span class="本文_色文字太字">重症度分類</span>がなされる。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_太字">慢性副鼻腔炎</span>：定義に示したような症状がある症例において，鼻内所見で，膿性，粘膿性または粘性の鼻汁や鼻粘膜腫脹，また鼻茸を認めるなどの所見がある場合に診断される。また，画像検査により副鼻腔に異常陰影を認める。</p>
      <div class="見出し_小見出し">治療方針</div>
      <p class="本文_先頭丸数字">❶急性鼻副鼻腔炎の場合には初診時に重症度分類を行い，ガイドラインに示してある治療アルゴリズムに従い治療を開始する。</p>
      <p class="本文_先頭丸数字">❷<span class="本文_色文字太字">軽症例の初期はウイルス感染</span>が主体として発症していることが疑われるために，抗菌薬の非投与で経過観察をする。<span class="本文_色文字太字">改善のない症例や，中等症および重症例においては細菌感染</span>が主体となるために抗菌薬の投与を行う。</p>
      <p class="本文_先頭丸数字">❸<span class="本文_色文字太字">感染している細菌</span>は，2大起炎菌として<span class="本文_色文字太字">肺炎球菌</span>と<span class="本文_色文字太字">インフルエンザ菌</span>があり，投与する抗菌薬はこれらに薬剤感受性および抗菌活性をもつものを選択。</p>
      <p class="本文_先頭丸数字">❹抗菌薬投与の必要性の有無を重症度分類により判定し，必要な場合にはペニシリン系抗菌薬（ペニシリンアレルギーがない場合）の投与から始める。選択する抗菌薬の第一選択はAMPCであり，効果不十分な場合には，セフェム系抗菌薬，ニューキノロン系抗菌薬，一部のマクロライド系抗菌薬を選択して投与する。セフェム系抗菌薬を投与する場合には，最初から高用量（倍量）の投与を行う。</p>
      <p class="本文_先頭丸数字">❺小児に対しては投与できる抗菌薬の種類に限りがあるため，使用できる抗菌薬のなかから最も薬剤感受性の良いものを高用量で投与する。小児の重症例では，ニューキノロン系抗菌薬に適応のあるものがなく，カルバペネム系抗菌薬を増量して投与する。</p>
      <p class="本文_先頭丸数字">❻効果が認められない場合には，細菌検査および薬剤感受性の結果に従い抗菌薬の増量および変更を検討して投与する。抗菌薬の投与期間は7～10日間。</p>
      <p class="本文_先頭丸数字">❼すべての治療期間において鼻処置を優先し，抗菌薬投与前に中鼻道の鼻漏から細菌検査を行う。また，中鼻道の開大処置は副鼻腔からの排膿に有効なため積極的に行うことが勧められる。</p>
      <p class="本文_先頭丸数字">❽眼窩内合併症および頭蓋内合併症が疑われる場合には，CTによる確認ののち入院での抗菌薬の点滴治療が勧められる。</p>
      <p class="本文_先頭丸数字">❾慢性副鼻腔炎において，保存的治療の中心は<span class="本文_色文字太字">マクロライド系抗菌薬の少量長期投与</span>であり，3ヵ月間で改善効果が認められる場合には最長6ヵ月間の投与を行う。鼻茸合併例の場合には効果が少ないため，鼻茸切除などの手術を行ってからマクロライド系抗菌薬の少量長期投与を行う。</p>
      <p class="本文_先頭丸数字">❿ 3 ～ 6 ヵ月のマクロライド系抗菌薬の少量長期投与を行っても十分な効果が認められない場合には，内視鏡下副鼻腔手術（ESS）を行うことを推奨する。</p>
      <p class="本文_先頭丸数字">⓫<img class="" src="./image/13-05-2.png"/>ガイドラインが作成された時点ではAMPCおよびクラブラン酸カリウム（CVA）/AMPCの副鼻腔炎の適応がなかったが，AMPCは2012年3月に，CVA/AMPCは2015年5月に適応が取得され，治療に用いることができるようになった。</p>
      <p class="本文_先頭丸数字">⓬<img class="" src="./image/13-05-3.png"/>海外では，鼻噴霧ステロイドの急性鼻副鼻腔炎に対する有効性を示す報告が多く認められるが，わが国では保険適用がないため，急性鼻副鼻腔炎，慢性副鼻腔炎の病名では投与できない。</p>
      <div class="見出し_専門医へのコンサルト">専門医へのコンサルト</div>
      <p class="本文_専門医へのコンサルト">以下の場合には専門医への紹介が望ましい。</p>
      <p class="本文_専門医へのコンサルト">①頭痛，顔面痛が強い場合</p>
      <p class="本文_専門医へのコンサルト">②眼窩内合併症や頭蓋内合併症が認められる場合</p>
      <p class="本文_専門医へのコンサルト">③治療を行うも症状が遷延する場合</p>
    </div>
    <!--総説 add end-->
    <!--最近の話題 add start-->
    <div class="ブロック-最近の話題">
      <div class="見出し_最近の話題">
        <p>最近の話題</p>
      </div>
      <p class="本文_最近の話題">　日本に限らず全世界的に薬剤耐性菌が問題となっている。世界的にはWHOにおいて2015年5月に「薬剤耐性（AMR）に関するグローバル・アクション・プラン」が採択され，わが国でも2016年4月に「<span class="本文_色文字太字">薬剤耐性（</span><span class="本文_色文字太字">AMR</span><span class="本文_色文字太字">）対策アクションプラン</span>」が発表された。それによると2020年までに2013年比で抗菌薬の使用量を全体で33％減少，鼻副鼻腔炎に関与する抗菌薬では経口セファロスポリン，フルオロキノロン，マクロライド系抗菌薬を50％減少させることを目標とすることが盛り込まれている。したがって，鼻副鼻腔炎，特に急性鼻副鼻腔炎の抗菌薬治療においてもこのことを考慮に入れ，まず抗菌薬投与が必要かどうかを重症度分類および治療アルゴリズムに基づいて検討し，必要であれば十分量の抗菌薬を短期間，すなわち高用量短期間投与することが推奨される。また，抗菌薬を選択する場合には，検出菌の同定および薬剤感受性結果に基づいて行うことが重要である。またその抗菌薬選択においても，上記の経口セファロスポリン，フルオロキノロン，マクロライド系抗菌薬以外の抗菌薬，すなわち<span class="本文_色文字太字">経口ペニシリン系抗菌薬</span>を中心に選択することが望ましいとされている。</p>
    </div>
    <!--最近の話題 add end-->
    <!--具体的処方 add start-->
    <div class="ブロック-具体的処方">
      <h3 class="見出し_具体的処方">具体的処方</h3>
      <table class="具体的処方_表">
        <colgroup>
          <col width="33.33%"/>
          <col width="33.33%"/>
          <col width="33.34%"/>
        </colgroup>
        <thead>
          <tr>
            <th>病型分類</th>
            <th>処方例</th>
            <th>ポイント</th>
          </tr>
          <tr>
            <th colspan="3">
              <p class="具体的処方_治療方針">【治療方針】臨床症状，鼻腔所見から重症度分類を行い，重症度に従った治療アルゴリズムを用いて治療を行う。抗菌薬投与前に中鼻道の鼻漏から細菌検査および薬剤感受性検査を行い，その結果を抗菌薬の選択に反映させ治療を行う。</p>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶小児急性鼻副鼻腔炎</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）軽症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">　</p>
              <p class="具体的処方_処方例">抗菌薬非投与で5日間経過観察。</p>
              <p class="具体的処方_処方例">効果がない場合</p>
            </td>
            <td colspan="" rowspan="2">
              <p class="具体的処方_ポイント">小児に用いられる抗菌薬はいずれもβ-ラクタム系抗菌薬である。このガイドライン作成時には<span class="本文_イタリック">クラバモックス</span>の急性副鼻腔炎に対する適応がなかったが，適応ができた現在，AMPC高用量の代わりに<span class="本文_イタリック">クラバモックス</span>の投与を考慮してもよい。ただ，他の抗菌薬と比較して下痢の頻度が高いため，投与時には整腸薬の併用を考慮する。</p>
              <p class="具体的処方_ポイント">投与量は成人投与量を超えない範囲で選択する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）中等症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">サワシリン</span></p>
              <p class="具体的処方_処方例1W下げ">20～40 mg/kg/日，3～4回に分服，5日間</p>
              <p class="具体的処方_処方例">ⅰ）で効果がない場合，①～③のいずれかを5日間</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">オラペネム小児用細粒</span></p>
              <p class="具体的処方_処方例1W下げ">4～6 mg/kg×2回/日</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">サワシリン</span></p>
              <p class="具体的処方_処方例1W下げ">60～90 mg/kg/日，3～4回に分服</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">メイアクト</span><span class="具体的処方_色文字">MS</span><span class="具体的処方_色文字">小児用細粒</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">フロモックス小児用細粒</span></p>
              <p class="具体的処方_処方例1W下げ">18 mg/kg/日，3回に分服</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅲ）重症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">ⅱ）で効果がない場合</p>
              <p class="具体的処方_処方例">①上記の薬剤を変更</p>
              <p class="具体的処方_処方例">②上顎洞穿刺洗浄を考慮</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶成人急性鼻副鼻腔炎</span>
              </p>
              <p class="具体的処方_病型分類">ⅰ）軽症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">　</p>
              <p class="具体的処方_処方例">抗菌薬非投与で5日間経過観察。</p>
              <p class="具体的処方_処方例">効果がない場合，①②のいずれかを5日間</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">サワシリン</span></p>
              <p class="具体的処方_処方例1W下げ">250 mg×3～4回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">　</p>
              <p class="具体的処方_ポイント">ニューキノロン系抗菌薬を投与する場合にはレスピラトリーキノロンを選択。</p>
              <p class="具体的処方_ポイント">また投与回数としてはPK/PD理論から1日1回投与の抗菌薬を選択する。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">メイアクト</span><span class="具体的処方_色文字">MS</span></p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">フロモックス</span></p>
              <p class="具体的処方_処方例1W下げ">100 mg×3回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅱ）中等症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">ⅰ）で効果がない場合，①～④のいずれかを5日間</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">サワシリン</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×3回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">メイアクト</span><span class="具体的処方_色文字">MS</span></p>
              <p class="具体的処方_処方例1W下げ">200 mg×3回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">フロモックス</span></p>
              <p class="具体的処方_処方例1W下げ">150 mg×3回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">クラビット</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×1回/日</p>
              <p class="具体的処方_処方例1W下げ">または<span class="具体的処方_色文字">ジェニナック</span></p>
              <p class="具体的処方_処方例1W下げ">400 mg×1回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">ジスロマック</span><span class="具体的処方_色文字">SR</span></p>
              <p class="具体的処方_処方例1W下げ">2 g×1回</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">ⅲ）重症</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">ⅱ）で効果がない場合</p>
              <p class="具体的処方_処方例">①上記の薬剤を変更する</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ロセフィン静注用</span></p>
              <p class="具体的処方_処方例1W下げ">1～2 g×1回/日，3日間，点滴静注</p>
              <p class="具体的処方_処方例">＋上顎洞穿刺洗浄を考慮</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
          <tr>
            <td colspan="" rowspan="">
              <p class="具体的処方_病型分類">
                <span class="具体的処方_色文字">▶成人慢性副鼻腔炎</span>
              </p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">①～③のいずれかに④を併用し，3～6ヵ月間投与</p>
              <p class="具体的処方_処方例">①<span class="具体的処方_色文字">クラリス</span></p>
              <p class="具体的処方_処方例1W下げ">200 mg×1回/日</p>
              <p class="具体的処方_処方例">②<span class="具体的処方_色文字">ルリッド</span></p>
              <p class="具体的処方_処方例1W下げ">150 mg×1回/日</p>
            </td>
            <td colspan="" rowspan="">
              <p class="具体的処方_ポイント">従来からのマクロライド系抗菌薬である③よりもニューマクロライド系抗菌薬に分類される①②のほうが効果があるとされている。</p>
            </td>
          </tr>
          <tr>
            <td colspan="" rowspan=""/>
            <td colspan="" rowspan="">
              <p class="具体的処方_処方例">③<span class="具体的処方_色文字">エリスロマイシン</span></p>
              <p class="具体的処方_処方例1W下げ">400～600 mg/日，4～6回に分服</p>
              <p class="具体的処方_処方例">④<span class="具体的処方_色文字">ムコダイン</span></p>
              <p class="具体的処方_処方例1W下げ">500 mg×3回/日</p>
            </td>
            <td colspan="" rowspan=""/>
          </tr>
        </tbody>
      </table>
    </div>
    <!--具体的処方 add end-->
    <!--最後の図表 add start-->
    <div class="ブロック-最後の図表"/>
    <!--最後の図表 add end-->
    <!--謝辞 add start-->
    <div class="ブロック-謝辞">
      <div class="謝辞"/>
    </div>
    <!--謝辞 add end-->
    <!--著者 add start-->
    <div class="ブロック-著者">
      <p class="著者"><span class="著者名">【</span>宇野耳鼻咽喉科クリニック<span class="著者名">　宇野　芳史</span><span class="著者名">】</span></p>
    </div>
    <!--著者 add end-->
  </body>
</doc>
